package com.yourcompany.zoo;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import com.perfspy.aspect.AbstractPerfSpyAspect;

@Aspect
public class PerfSpyDemoAsepct extends AbstractPerfSpyAspect {

	//capture call(* com.anothercompany..*.*(..) ), but do not capture the details of another company's functions
	//hence use call not execution
	@Pointcut("execution(* com.yourcompany.zoo..*.*(..) ) || call(* com.anothercompany..*.*(..) )")
	public void withinCflowOps() {
	}

	@Pointcut("cflow(execution(*  com.yourcompany.zoo.animal.Animal.playTricks(..)  ) )")
	public void cflowOps() {
	}

}
