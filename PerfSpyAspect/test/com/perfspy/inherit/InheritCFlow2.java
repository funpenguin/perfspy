package com.perfspy.inherit;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import com.perfspy.aspect.AbstractPerfSpyAspect;

@Aspect
public class InheritCFlow2 extends AbstractPerfSpyAspect {

	/**
	 * when you test this in eclipse, remember to clean PerfSpyAspect whenever
	 * you change InheritCFlow
	 * 
	 * AInterface.h is invoked in caller.inherit.InheritTest, if caller.inherit
	 * is not weaved (excluded in aop.xml), then
	 * "cflow(call(* *..AInterface.h(..) ) )" won't capture AInterface.h.
	 * "cflow(execution(* *..AInterface.h(..) ) )" can. if caller.inherit is
	 * weaved, then if caller.inherit is weaved, then
	 * "cflow(call(* *..AInterface.h(..) ) )" will be captured
	 * 
	 * cflow(execution(* *..AInterface.h(..) ) ), with or without weaving caller.inherit, will capture: 
	 * AInheritance.h(), A2.h(), A3.h()
	 * 
	 * cflow(call(* *..AInterface.h(..) ) ), with weaving caller.inherit, will 
	 * capture:AInheritance.h(), A2.h(), A3.h()
	 * 
	 * cflow(call(* *..AInterface.h(..) ) ), without weaving caller.inherit, will not capture:
	 * AInheritance.h(), A2.h(), A3.h()
	 * 
	 * @Pointcut("execution(* inherit..*.*(..) )") doesn't capture constructors
	 * @Pointcut("execution(* inherit.A3.new(..) )") can't even compile, while @Pointcut("execution( inherit.A3.new(..) )") can,
	 * suspect this is because constructors do not return any value, so even "*" is not allowed
	 * @Pointcut("execution( inherit.*.new(..) )") captures all constructors
	 */
	@Pointcut("cflow(execution(* *..A3.method9(..) ) )")
	// + "|| cflow(execution( * *..A3.h() ) ) ")
	//	@Pointcut("cflow(execution(* *..A3.*(..) ) )")
	public void cflowOps() {
	}

	@Pointcut("execution(* inherit..*.*(..) )")
	//	@Pointcut("execution(* inherit.A3.new(..) )")
	//	@Pointcut("execution( inherit.*.new(..) )")
	public void withinCflowOps() {
	}

	// @Pointcut("!within(inherit.InheritCFlow)")
	// public void otherNotMonitoredOps() {
	// }

	@Override
	protected boolean isTargetToBeCaptured(String target) {
		if (target.contains("A3")) {
			return true;
		}
		return false;
	}

}