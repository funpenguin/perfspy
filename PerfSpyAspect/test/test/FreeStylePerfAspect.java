package test;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import com.perfspy.aspect.AbstractPerfSpyAspect;

@Aspect
public class FreeStylePerfAspect extends AbstractPerfSpyAspect {
	//capture call(* com.anothercompany..*.*(..) ), but do not capture the details of another company's functions
	//hence use call not execution
	@Pointcut("execution(* test.run.inner..*(..) )")
	public void withinCflowOps() {
	}

	@Pointcut("cflow(execution(*  test.run.inner..*(..)  ) )")
	public void cflowOps() {
	}
}
