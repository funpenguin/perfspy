package hibernate;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import com.perfspy.aspect.AbstractPerfSpyAspect;

@Aspect
public class HibernateSessionImplPerfSpy extends AbstractPerfSpyAspect {

	@Pointcut("execution(* org.hibernate..*.*(..))")
	public void hibernateOps() {
	}

	@Pointcut("execution(* ppm..*.*(..))")
	public void ppmOps() {
	}

	@Pointcut("hibernateOps() || ppmOps() ")
	public void withinCflowOps() {
	}

	@Pointcut("cflow (execution(* org.hibernate.impl.SessionImpl.flush(..))) ")
	public void cflowOps() {
	}

	@Override
	protected String getPersistTo(String target) {
		return "DB";
	}

}
