package com.perfspy.aspect;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.MDC;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import com.perfspy.monitor.config.PerfSpyConfig;
import com.perfspy.monitor.dao.DAOExecutor;
import com.perfspy.monitor.dao.OperationFlowDAO;
import com.perfspy.monitor.exception.FatalException;
import com.perfspy.monitor.model.OperationFlow;
import com.perfspy.monitor.util.EnteredCFlowFlag;
import com.perfspy.monitor.util.HibernateLazyInitializeUtil;

@Aspect
// ("perthis(monitoredOp())")
public abstract class AbstractPerfSpyAspect {
	private static final String CONFIG_FILE = "perfspy-config.xml";

	private static final String FILE_PERSIST = "FILE";

	private static final String DB_PERSIST = "DB";

	private static final Log logger = LogFactory.getLog(AbstractPerfSpyAspect.class);// Logger.getLogger(AbstractPerfSpyAspect.class);

	public AbstractPerfSpyAspect() {
		//		System.out.println("creating " + this + " by " + this.getClass().getClassLoader());
		//		Thread.currentThread().dumpStack();
	}

	@Pointcut
	public abstract void withinCflowOps();

	@Pointcut
	public abstract void cflowOps();

	@Pointcut("!within(com.perfspy.aspect.AbstractPerfSpyAspect+) && !within(com.perfspy..*)")
	public void notMonitoredOps1() {
	}

	@Pointcut("!execution(* toString()) && !within(org.apache.log4j..*)")
	public void notMonitoredOps2() {
	}

	@Pointcut("notMonitoredOps1() && notMonitoredOps2()")
	public void notMonitoredOps() {
	}

	@Pointcut(" withinCflowOps() && cflowOps() &&  notMonitoredOps()")
	public void monitoredOp() {
	}

	@Around("monitoredOp()")
	public Object monitor(ProceedingJoinPoint pjp) throws Throwable {

		logger.debug("entering " + pjp);

		String pjpShortSig = pjp.getSignature().toShortString();
		String pjpLongSig = pjp.getSignature().toLongString();

		if (HibernateLazyInitializeUtil.isHibernateLazyEnhanceByCGLib(pjpShortSig)) {
			logger.debug(pjp + " is hibernate lazy initialized object, skip it");
			return pjp.proceed();
		}

		if (EnteredCFlowFlag.isFatallyWounded()) {
			logger.info("Cheer up, it is not the end of the world! Just not doing my job of capturing " + pjp
					+ " because of a fatal error.");
			return pjp.proceed();
		}

		if (!EnteredCFlowFlag.isEntered()) {
			String capturedTarget = captureTarget(pjp);
			if (StringUtils.isEmpty(capturedTarget)) {
				return pjp.proceed();
			} else {
				try {
					EnteredCFlowFlag.enterCFlow(capturedTarget, this);
					logger.info("Capturing " + capturedTarget);
					PerfSpyConfig perfSpyConfig = EnteredCFlowFlag.getPerfSpyConfig();
					if (getPersistToWrapper(EnteredCFlowFlag.getCflowTarget()).equals(DB_PERSIST)) {
						Connection con = perfSpyConfig.getConnection(EnteredCFlowFlag.getCflowTarget());
						EnteredCFlowFlag.setConnection(con);

						OperationFlowDAO dao = new OperationFlowDAO(con);
						dao.setSimpleBatchSize(perfSpyConfig.getSimpleBatchSize());
						dao.setDetailBatchSize(perfSpyConfig.getDetailBatchSize());

						DAOExecutor executor = new DAOExecutor(perfSpyConfig.getQueueSize(capturedTarget));
						EnteredCFlowFlag.setDAOExecutor(executor);

						dao.setExecutor(executor);
						EnteredCFlowFlag.setOperationFlowDao(dao);

						dao.startPersisting(EnteredCFlowFlag.getSpyEvent());

						EnteredCFlowFlag.getAnalyzer().prepareForAnalyze();
					}

				} catch (Throwable e) {
					logger.error("error in entering " + pjp, e);
					EnteredCFlowFlag.encounterFatalError();
					return pjp.proceed();
				}
			}
		}

		// skip an operation and its child operations
		if (EnteredCFlowFlag.isSkip()) {
			return pjp.proceed();
		}
		if (EnteredCFlowFlag.getPerfSpyConfig().isToSkip(EnteredCFlowFlag.getCflowTarget(), pjpLongSig)) {
			logger.debug("skip " + pjpLongSig);
			if (!EnteredCFlowFlag.isSkip()) {
				EnteredCFlowFlag.enterSkip();
			}
			try {
				return pjp.proceed();
			} finally {
				if (EnteredCFlowFlag.isSkip()) {
					EnteredCFlowFlag.exitSkip();
				}
			}
		}

		if (this != EnteredCFlowFlag.getAspectInstance()) {
			if (logger.isDebugEnabled()) {
				logger.debug(this + " is not " + EnteredCFlowFlag.getAspectInstance() + ", since " + pjp
						+ " is already captured, i will turn away.");
			}
			return pjp.proceed();
		}

		OperationFlow firstLevelOps = null;
		OperationFlow currentOps = null;
		try {
			firstLevelOps = (OperationFlow) MDC.get("firstLevel-" + Thread.currentThread().getId());
			if (firstLevelOps == null) {
				firstLevelOps = OperationFlow.DUMMY();
				/*
				 * unfortunately LTW doesn't work with perthis() aspect, so this
				 * aspect is a singleton, have to differentiate different
				 * invocations, so use MDC
				 */
				MDC.put("firstLevel-" + Thread.currentThread().getId(), firstLevelOps);
				MDC.put("current-" + Thread.currentThread().getId(), firstLevelOps);
			}

			logger.debug("the thing inside MDC is:" + MDC.get("current-" + Thread.currentThread().getId()));
			OperationFlow previousCurrentOps = (OperationFlow) MDC.get("current-" + Thread.currentThread().getId());
			currentOps = new OperationFlow();
			currentOps.setSignature(pjpShortSig);
			currentOps.setLongSignature(pjpLongSig);

			MethodSignature methodSig = (MethodSignature) pjp.getStaticPart().getSignature();
			currentOps.setParameterName(methodSig.getParameterNames());
			currentOps.setParameters(pjp.getArgs());

			currentOps.setId(EnteredCFlowFlag.getId());
			previousCurrentOps.addOutOp(currentOps);

			MDC.put("current-" + Thread.currentThread().getId(), currentOps);
		} catch (Throwable e) {
			logger.error("error in the before part of the advice for " + pjp, e);
			EnteredCFlowFlag.encounterFatalError();
			return pjp.proceed();
		}

		long start = System.currentTimeMillis();
		if (logger.isDebugEnabled()) {
			logger.debug(this + " start handling " + currentOps + " at: " + start);
		}

		Object ret = null;
		try {
			ret = pjp.proceed();
			return ret;
		} finally {
			long processStartTime = System.currentTimeMillis();
			try {

				long complete = System.currentTimeMillis();
				long runTimeInMillionSeconds = complete - start;

				runTimeInMillionSeconds -= currentOps.getExecutionTime();
				if (logger.isDebugEnabled()) {
					logger.debug(this + " finish advising " + currentOps + " at: " + complete + " ,total time:"
							+ (complete - start) + ", after substracting processing time "
							+ currentOps.getExecutionTime() + ", the execution time is:" + runTimeInMillionSeconds
							+ " million seconds");
				}
				currentOps.setExecutionTime(runTimeInMillionSeconds / 1000);
				currentOps.setReturnValue(ret);

				MDC.put("current-" + Thread.currentThread().getId(), currentOps.callingOp);

				EnteredCFlowFlag.getAnalyzer().analyzeThis(currentOps);
				if (getPersistToWrapper(EnteredCFlowFlag.getCflowTarget()).equals(DB_PERSIST)) {
					EnteredCFlowFlag.getOperationFlowDao().persistThis(currentOps,
							shouldLogDetailsWrapper(EnteredCFlowFlag.getCflowTarget(), pjpLongSig));
				}

				long processEndTime = System.currentTimeMillis();
				long processTime = processEndTime - processStartTime;
				OperationFlow parentOp = currentOps.callingOp;
				while (parentOp != firstLevelOps) {
					parentOp.addExecutionTime(processTime);
					parentOp = parentOp.callingOp;
				}
				// if debugging, keep the whole tree to compare with the tree persisted to the database
				if (!logger.isDebugEnabled()) {
					// now that the operation has been analyzed and persisted, null it to help GC
					currentOps.runAwayFromParent();
					currentOps = null;
				}
				if (logger.isDebugEnabled()) {
					logger.debug(this + " processing time for " + currentOps + ":" + processTime);
				}
				// output if it has returned to the first level
				OperationFlow currentOps2 = (OperationFlow) MDC.get("current-" + Thread.currentThread().getId());

				if (currentOps2 == firstLevelOps) {
					try {
						//						String spyEvent = EnteredCFlowFlag.getSpyEvent();
						//						String cflowName = EnteredCFlowFlag.getCflowTarget();

						// if
						// (getPersistToWrapper(cflowName).equals(FILE_PERSIST))
						// {
						// EnteredCFlowFlag.getAnalyzer().doneAnalyze();
						// OperationFlowImpExp.dumpToFile(outputFilePath + "-"
						// + escapeForFileName(spyEvent),
						// firstLevelOps);
						// // TODO: what to do with analyzeResults?
						// } else {
						OperationFlowDAO dao = EnteredCFlowFlag.getOperationFlowDao();
						dao.persistThis(firstLevelOps,
								shouldLogDetailsWrapper(EnteredCFlowFlag.getCflowTarget(), pjpLongSig));
						dao.lastOpPersisted();

						// some analyzers are performed on the database, so they need to wait 
						//after all operations are persisted into the database, 
						//this can take considerable time 
						List<OperationFlow> analyzeResults = EnteredCFlowFlag.getAnalyzer().doneAnalyze();

						dao.finishedPersisting(EnteredCFlowFlag.getStartTime(), analyzeResults);

						// just for testing
						if (logger.isDebugEnabled()) {
							// before extracting the tree, wait till the tree is completely written
							EnteredCFlowFlag.getDAOExecutor().waitToFinish();
							Connection con1 = EnteredCFlowFlag.getPerfSpyConfig()
									.getConnection(EnteredCFlowFlag.getCflowTarget());
							OperationFlowDAO dao1 = new OperationFlowDAO(con1);
							OperationFlow opFromDb = dao1.getOperationTreeByEventName(EnteredCFlowFlag.getSpyEvent());
							con1.close();
							if (!opFromDb.equals(firstLevelOps)) {
								logger.debug(
										"The operation tree persisted to the db is not equal to the one captured, perhaps Writting to DB is wrong!");
							}
						}

						firstLevelOps.suicideBomb();
						//majority of memory is held up in DAOExecutor 
						//which is run in a seperate thread, if gc() here, gc log will show everytime gc runs,
						//it reclaims little memory, and eventually it throws out GC Overhead limit error.
						//System.gc();
					} catch (Throwable e) {
						EnteredCFlowFlag.encounterFatalError();
						logger.error("error in dealing with the entry method for " + pjp, e);
					} finally {
						MDC.clear();
						EnteredCFlowFlag.exitCFlow();
						logger.info("Exited " + pjp);

					}
				}
			} catch (FatalException e) {
				EnteredCFlowFlag.encounterFatalError();
				logger.error("error in the after part of the advice for " + pjp, e);
			} catch (Throwable e) {
				logger.error("error in the after part of the advice for " + pjp, e);
			}

		}

	}

	private String escapeForFileName(String str) {
		str = str.replace("/", "-");
		return str.replace(":", ".");
	}

	protected String captureTarget(ProceedingJoinPoint pjp) {
		try {
			Object target = pjp.getTarget();
			Class targetClz;
			if (target == null) {
				// with static methods, target && this are both null
				targetClz = pjp.getSignature().getDeclaringType();
			} else {
				targetClz = target.getClass();
			}

			logger.debug("capturing " + targetClz + "?");

			// allow runtime configuration changes
			try {
				PerfSpyConfig config = PerfSpyConfig.config(CONFIG_FILE);
				EnteredCFlowFlag.setPerfSpyConfig(config);
			} catch (Throwable e) {
				logger.error("can't read configuration file " + CONFIG_FILE, e);
			}

			String targetClzName = targetClz.getName();

			if (isTargetToBeCaptured(targetClzName)) {
				logger.debug("capturing " + targetClz + "? YES!");
				return targetClzName;
			}

			logger.debug("capturing " + targetClz + "? NO!");
			return "";

		} catch (Exception e) {
			logger.error("error in checking if should capture " + pjp, e);
		}
		return "";

	}

	protected boolean isTargetToBeCaptured(String targetClzName) {
		logger.trace("checking " + targetClzName + " against the configuration file");
		return EnteredCFlowFlag.getPerfSpyConfig().isTargetEnabled(targetClzName);

	}

	/*
	 * these wrapper methods are here to ensure the law of inheritance is
	 * observed by aspectj. It seems if methods are invoked inside an advice,
	 * then the methods defined the abstract advice are called, no matter if
	 * these methods are overridden by sub-advices.
	 * 
	 * this seems to be a bug of aspectj compiler, and has been fixed in 1.7.2.
	 * but if compile aspectj advices with 1.6.2, this issue manifests.
	 */

	private String getPersistToWrapper(String target) {
		return getPersistTo(target);
	}

	private String[] shouldLogDetailsWrapper(String target, String pjpLongSig) {
		return shouldLogDetails(target, pjpLongSig);
	}

	protected String[] shouldLogDetails(String target, String pjpLongSig) {
		return EnteredCFlowFlag.getPerfSpyConfig().logDetails(EnteredCFlowFlag.getCflowTarget(), pjpLongSig);
	}

	protected String getPersistTo(String target) {
		return EnteredCFlowFlag.getPerfSpyConfig().getPersistTo(target);
	}

}