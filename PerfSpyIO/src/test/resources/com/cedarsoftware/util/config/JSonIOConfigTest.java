package com.cedarsoftware.util.config;

import junit.framework.TestCase;

public class JSonIOConfigTest extends TestCase {

	public void testConfig3() throws Exception {

		JSonIOConfig ioConfig = JSonIOConfig.config(JSonIOConfigTest.class, "JSonIOConfigTest3.xml");
		assertTrue(ioConfig.acceptWrittingType("org.apache.lucene.search.BooleanQuery"));
	}

	public void testConfig2() throws Exception {

		JSonIOConfig ioConfig = JSonIOConfig.config(JSonIOConfigTest.class, "JSonIOConfigTest2.xml");
		assertTrue(ioConfig.acceptWrittingType("com.kintana.core.packet.LogonPacket"));
	}

	public void testConfig() throws Exception {

		JSonIOConfig ioConfig = JSonIOConfig.config(JSonIOConfigTest.class, "JSonIOConfigTest.xml");

		assertTrue(ioConfig.acceptWrittingType(java.lang.String.class.getName()));

		Object[] o = new Object[2];
		String name = o.getClass().getName();
		System.out.println(name);
		assertTrue(ioConfig.acceptWrittingType(name));

		String[] s = new String[2];
		String name2 = s.getClass().getName();
		System.out.println(name2);
		assertTrue(ioConfig.acceptWrittingType(name2));

		assertFalse(ioConfig.acceptWrittingType("a.b.c.d"));

		assertTrue(ioConfig.acceptWrittingType(new String[] { "abcd", "1234" }.getClass().getName()));

		int colTh = ioConfig.getCollectionThreshhold("a.b.c.d");
		assertTrue(colTh == 2);

		int opTh = ioConfig.getCollectionThreshhold("com.perfspy.monitor.model.OperationFlow");
		assertTrue(opTh == Integer.MAX_VALUE);

		int goNoFurtherLevel = ioConfig.getGoNoFurtherLevel();
		assertTrue(goNoFurtherLevel == 5);

		/**
		 *   <include within="com.mercury.itg"/>	
		<include within="com.kintana"/>	
		<include within="com.sea.dolphin"/>
		
		<exclude within="com.kintana.ServerConnection"/>
		<exclude within="com.sea"/>
		
		<include within="com.kintana.core.cache.MLUCacheKey"/>			
		<exclude within="com.kintana.core.cache"/> 
		 */
		assertTrue(ioConfig.acceptWrittingType("com.sea.dolphin"));
		assertFalse(ioConfig.acceptWrittingType("com.kintana.ServerConnection"));

		assertTrue(ioConfig.acceptWrittingType("com.kintana.core.cache.MLUCacheKey"));
		assertFalse(ioConfig.acceptWrittingType("com.kintana.core.cache"));

	}

	@Override
	protected void setUp() throws Exception {

	}

}
