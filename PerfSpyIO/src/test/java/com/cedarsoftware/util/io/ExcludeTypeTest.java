package com.cedarsoftware.util.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import com.cedarsoftware.util.config.JSonIOConfig;

public class ExcludeTypeTest extends TestCase {
	public void testExcludeCollection() throws Exception {
		JSonIOConfig ioConfig = new JSonIOConfig();
		ioConfig.excludeType(B1.class);

		List<B1> b1s = new ArrayList<ExcludeTypeTest.B1>();
		for (int i = 0; i < 20; i++) {
			B1 b1 = new B1();
			b1s.add(b1);
		}
		String json = JsonWriter.objectToJson(b1s, ioConfig);
		// since B1 is excluded to be written, json should be empty
		System.out.println(json);

		List<B1> newB1s = (List<B1>) JsonReader.jsonToJava(json);
		assertTrue(newB1s.size() == 20);
		for (B1 b1 : newB1s) {
			assertTrue(b1 == null);
		}
	}

	public void testExcludeArray() throws Exception {
		JSonIOConfig ioConfig = new JSonIOConfig();

		String[] ss = new String[20];
		for (int i = 0; i < ss.length; i++) {
			ss[i] = "abc-" + i;
		}

		System.out.println("****************plain String[20]");
		String json = JsonWriter.objectToJson(ss);
		System.out.println(json);

		String[] newSS = (String[]) JsonReader.jsonToJava(json);
		assertTrue(Arrays.equals(ss, newSS));

		// B1.class is a red herring, but we want to see the String array is written, despite
		// that [Ljava.lang.String; is not explicitly included
		System.out.println("****************plain String[20], B1.class included to be written");
		ioConfig.includeType(B1.class);
		json = JsonWriter.objectToJson(ss);
		System.out.println(json);

		newSS = (String[]) JsonReader.jsonToJava(json);
		assertTrue(Arrays.equals(ss, newSS));

		System.out.println("****************B1[20], B1.class excluded to be written");
		ioConfig = new JSonIOConfig();
		ioConfig.excludeType(B1.class);

		B1[] b1s = new B1[20];
		for (int i = 0; i < b1s.length; i++) {
			b1s[i] = new B1();
			b1s[i].setB1s("b1sString" + i);
		}

		json = JsonWriter.objectToJson(b1s);
		// since B1 is excluded to be written, json should be empty
		System.out.println("write B1[20], with B1.class excluded to be written" + json);

		System.out.println("****************B2[20], B1.class excluded to be written");

		ioConfig = new JSonIOConfig();
		ioConfig.excludeType(B1.class);

		b1s = new B2[20];
		for (int i = 0; i < b1s.length; i++) {
			b1s[i] = new B2();
			((B2) (b1s[i])).setB2s("b2sString" + i);
		}
		json = JsonWriter.objectToJson(b1s);
		System.out.println(json);
		assertTrue(Arrays.equals(b1s, (B1[]) JsonReader.toJava(json)));

		System.out.println("****************B2[20], B2.class excluded to be written");

		ioConfig = new JSonIOConfig();
		ioConfig.excludeType(B2.class);

		b1s = new B2[20];
		for (int i = 0; i < b1s.length; i++) {
			b1s[i] = new B2();
			((B2) (b1s[i])).setB2s("b2sString" + i);
		}
		json = JsonWriter.objectToJson(b1s);
		System.out.println(json);

		System.out.println("****************Object[20], B2.class excluded to be written");
		ioConfig = new JSonIOConfig();
		ioConfig.excludeType(B2.class);

		Object[] objs = new Object[20];
		for (int i = 0; i < b1s.length; i++) {
			objs[i] = new B2();
			((B2) (b1s[i])).setB2s("b2sString" + i);
		}
		json = JsonWriter.objectToJson(objs, ioConfig);
		System.out.println(json);
		Object[] objs2 = (Object[]) JsonReader.jsonToJava(json);
		assertTrue(objs2.length == 20);
		for (Object obj : objs2) {
			assertTrue(obj == null);
		}
	}

	public void testExclude() throws Exception {
		JSonIOConfig ioConfig = new JSonIOConfig();

		A a = new A();
		a.setB1(new B1());
		a.setB2(new B2());
		String json = JsonWriter.objectToJson(a);
		System.out.println(json);

		A newA = (A) JsonReader.jsonToJava(json);
		assertTrue(a.s.equals(newA.s));
		assertTrue(a.path.equals(newA.path));
		assertTrue(a.b1.equals(newA.b1));
		assertTrue(a.b2.equals(newA.b2));

		ioConfig.excludeType(B1.class);

		json = JsonWriter.objectToJson(a, ioConfig);
		System.out.println(json);

		newA = (A) JsonReader.jsonToJava(json);
		assertTrue(a.s.equals(newA.s));
		assertTrue(a.path.equals(newA.path));
		assertTrue(newA.b1 == null);
		assertTrue(a.b2.equals(newA.b2));

		ioConfig = new JSonIOConfig();
		ioConfig.includeType(A.class);
		ioConfig.includeType(B2.class);
		json = JsonWriter.objectToJson(a, ioConfig);
		System.out.println(json);
		newA = (A) JsonReader.jsonToJava(json);
		assertTrue(a.s.equals(newA.s));
		assertTrue(a.path.equals(newA.path));
		assertTrue(newA.b1 == null);
		assertTrue(a.b2.equals(newA.b2));
	}

	class A {
		String path = "c:\\a\\b\\c\\";
		String s = "";
		B1 b1;
		B2 b2;

		public B1 getB1() {
			return b1;
		}

		public void setB1(B1 b1) {
			this.b1 = b1;
		}

		public B2 getB2() {
			return b2;
		}

		public void setB2(B2 b2) {
			this.b2 = b2;
		}

		@Override
		public boolean equals(Object obj) {
			A that = (A) obj;
			return this.path.equals(that.path);
		}
	}

	class B1 {
		protected String b1s = "b1String";

		public String getB1s() {
			return b1s;
		}

		public void setB1s(String b1s) {
			this.b1s = b1s;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}

			if (!(obj instanceof B1)) {
				return false;
			}

			B1 that = (B1) obj;
			return this.b1s.equals(that.b1s);
		}

		@Override
		public int hashCode() {
			return b1s.hashCode();
		}
	}

	class B2 extends B1 {
		String b2s = "b2String";

		public String getB2s() {
			return b2s;
		}

		public void setB2s(String b2s) {
			this.b2s = b2s;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}

			if (!(obj instanceof B2)) {
				return false;
			}

			B2 that = (B2) obj;
			return this.b2s.equals(that.b2s);
		}

		@Override
		public int hashCode() {
			return b2s.hashCode();
		}
	}

	class C {
		String cs;

		public String getCs() {
			return cs;
		}

		public void setCs(String cs) {
			this.cs = cs;
		}

	}
}
