package com.cedarsoftware.util.io;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.cedarsoftware.util.config.JSonIOConfig;

/***
 * make sure the referenced objects are written and read successfully
 * 
 */
public class RefIDTest extends TestCase {

	public void testRefIds() throws Exception {
		JSonIOConfig ioConfig = new JSonIOConfig();

		C c = new C("c");

		for (int i = 0; i < 5; i++) {
			C c1 = new C("c1-" + i);
			c.addChild(c1);
		}

		C c2 = new C("c2");
		c.addChild(c2);

		for (int i = 0; i < 5; i++) {
			C c21 = new C("c21-" + i);
			c2.addChild(c21);
		}

		String json = JsonWriter.objectToJson(c);
		System.out.println(json);
		C newC = (C) JsonReader.toJava(json);
		assertTrue(c.equals(newC));
	}

	class C {
		private List<C> children = new ArrayList<C>();
		private String name;
		private C parent;

		public C(String name) {
			super();
			this.name = name;
		}

		public void addChild(C a) {
			children.add(a);
			a.parent = this;
		}

		@Override
		public String toString() {
			return this.name;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}

			if (!(obj instanceof C)) {
				return false;
			}

			C that = (C) obj;
			if (!this.name.equals(that.name)) {
				return false;
			}

			if (this.parent != null) {
				if (!this.parent.name.equals(that.parent.name)) {
					return false;
				}
			}

			if (!(this.children.equals(that.children))) {
				return false;
			}

			return true;
		}
	}
}
