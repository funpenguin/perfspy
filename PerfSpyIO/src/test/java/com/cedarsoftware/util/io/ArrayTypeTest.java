package com.cedarsoftware.util.io;

import java.lang.reflect.Field;

import junit.framework.TestCase;

/**
 * @author Apple array types need to be treated specially, the class name is
 *         prefixed with "[L" and appendixed with ";", such as
 *         "[Lcom.cedarsoftware.util.io.B;".
 * 
 */
public class ArrayTypeTest extends TestCase {

	public void testWriteArray() throws Exception {

		B[] bb = new B[2];
		for (int i = 0; i < bb.length; i++) {
			bb[i] = new B();
		}

		A a = new A(bb);

		String json = JsonWriter.objectToJson(a);
		System.out.println(json);

		A a2 = (A) JsonReader.toJava(json);

		assertEquals(a, a2);

		assertTrue(a2.oarray == a2.oarray2);

	}

	public void testArrayTypes() {
		A a = new A(null);
		Object fieldValue = getFieldValue(a, "bs");
		System.out.println(fieldValue);
		System.out.println(fieldValue.getClass());
		assertTrue(fieldValue.getClass().getName().equals("[Lcom.cedarsoftware.util.io.ArrayTypeTest$B;"));
		String fieldClz = fieldValue.getClass().getName();
		String arrayClz = fieldClz.substring(2, fieldValue.getClass().getName().length() - 1);
		System.out.println(arrayClz);
		assertTrue(arrayClz.equals("com.cedarsoftware.util.io.ArrayTypeTest$B"));

	}

	private Object getFieldValue(Object obj, String fieldName) {
		try {
			Field field = obj.getClass().getDeclaredField(fieldName);
			if (!field.isAccessible()) {
				field.setAccessible(true);
			}

			return field.get(obj);
		} catch (Exception e) {
		}

		return null;
	}

	class A {

		public A(B[] b) {
			super();

			if (b == null) {
				b = new B[50];
			}

			this.bs = b;

			oarray = new Object[] { "foo", true, 55, 16L, 3.14 };
			oarray2 = oarray;

		}

		private B[] bs = new B[50];
		private Object oarray;
		private Object oarray2;

		private char[] _chars_a = new char[] { 'a', '\t', '\u0005' };

		private char[] _chars_a_copy = _chars_a;

		private int[] ll_a = new int[] { 1, 2, 3 };
		private int[] ll_a_copy = ll_a;

		private char[][] _chars_g = new char[][] { { 'a', '\t', '\u0004' }, null, {} };

		public B[] getB() {
			return bs;
		}

		public void setB(B[] b) {
			this.bs = b;
		}

		@Override
		public boolean equals(Object obj) {
			A that = (A) obj;
			for (int i = 0; i < bs.length; i++) {
				if (!bs[i].equals(that.bs[i])) {
					return false;
				}
			}

			for (int i = 0; i < ((Object[]) oarray).length; i++) {
				if (!((Object[]) oarray)[i].equals(((Object[]) that.oarray)[i])) {
					return false;
				}

			}

			for (int i = 0; i < _chars_a.length; i++) {
				if (_chars_a[i] != that._chars_a[i]) {
					return false;
				}
			}

			for (int i = 0; i < _chars_a_copy.length; i++) {
				if (_chars_a_copy[i] != that._chars_a[i]) {
					return false;
				}
			}

			for (int i = 0; i < ll_a.length; i++) {
				if (ll_a[i] != that.ll_a[i]) {
					return false;
				}
			}

			for (int i = 0; i < ll_a_copy.length; i++) {
				if (ll_a_copy[i] != that.ll_a[i]) {
					return false;
				}
			}

			for (int i = 0; i < _chars_g.length; i++) {
				if (_chars_g[i] == null) {
					if (that._chars_g[i] != null) {
						return false;
					}
				} else {
					for (int j = 0; j < _chars_g[i].length; j++)
						if (_chars_g[i][j] != that._chars_g[i][j]) {
							return false;
						}
				}
			}

			return true;
		}
	}

	class B {
		private String bstring = "bs";

		@Override
		public boolean equals(Object obj) {
			B that = (B) obj;
			return this.bstring.equals(that.bstring);
		}
	}
}
