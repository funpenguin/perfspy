package com.cedarsoftware.util.io;

import junit.framework.TestCase;

public class SubClassTest extends TestCase {
	interface CInterface {
		public String getCs();
	}

	public void testWriteSubClass() throws Exception {

		class C implements CInterface {
			String cs;

			public String getCs() {
				return cs;
			}

			public void setCs(String cs) {
				this.cs = cs;
			}
		}

		class D {
			CInterface c;

			public CInterface getC() {
				return c;
			}

			public void setC(CInterface c) {
				this.c = c;
			}

		}

		class B1 {
			protected String b1s = "b1String";
			protected CInterface c;

			public CInterface getC() {
				return c;
			}

			public void setC(CInterface c) {
				this.c = c;
			}

			public String getB1s() {
				return b1s;
			}

			public void setB1s(String b1s) {
				this.b1s = b1s;
			}

		}

		class B2 extends B1 {
			String b2s = "b2String";
			int b2i = 15;
			Class clz = C.class;
			D d;

			public String getB2s() {
				return b2s;
			}

			public void setB2s(String b2s) {
				this.b2s = b2s;
			}

			public D getD() {
				return d;
			}

			public void setD(D d) {
				this.d = d;
			}

		}

		B2 b2 = new B2();
		b2.setB2s("This is a b2String");

		C c = new C();
		c.setCs("This is a cString");

		D d = new D();
		d.setC(c);

		b2.setC(c);
		b2.setD(d);

		String json = JsonWriter.objectToJson(b2);
		System.out.println(json);

		B2 newB2 = (B2) JsonReader.jsonToJava(json);
		System.out.println(newB2);

		assertEquals(b2.getB1s(), newB2.getB1s());
		assertEquals(b2.getB2s(), newB2.getB2s());
		assertEquals(b2.getC().getCs(), newB2.getC().getCs());

	}
}
