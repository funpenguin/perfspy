package com.cedarsoftware.util.io;

import org.apache.commons.lang3.builder.ToStringStyle;

public class Child {

	private Long id;
	private String name;
	private Long version;
	private Parent parent;

	public Parent getParent() {
		return parent;
	}

	public void setParent(Parent parent) {
		this.parent = parent;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}

	@Override
	public int hashCode() {
		if (this.id != null) {
			return new org.apache.commons.lang3.builder.HashCodeBuilder().append(this.id).toHashCode();
		} else {
			return System.identityHashCode(this);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Child)) {
			return false;
		}

		Child that = (Child) obj;
		return this.getId().equals(that.getId());
	}

}
