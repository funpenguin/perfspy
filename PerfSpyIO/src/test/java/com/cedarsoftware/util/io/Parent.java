package com.cedarsoftware.util.io;

import java.util.ArrayList;
import java.util.List;

public class Parent {

	private Long id;
	private String name;
	private Long version;
	private List<Child> children = new ArrayList<Child>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Child> getChildren() {
		return children;
	}

	private void setChildren(List<Child> child) {
		this.children = child;
	}

	public void addChild(Child child) {
		this.children.add(child);
	}

	public void addChildren(List<Child> children) {
		this.children.addAll(children);
	}

	public void removeChild(Child child) {
		this.children.remove(child);
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return new org.apache.commons.lang3.builder.ToStringBuilder(this).append("id", this.id)
				.append("name", this.name).toString();
	}

	@Override
	public int hashCode() {
		if (this.id != null) {
			return new org.apache.commons.lang3.builder.HashCodeBuilder().append(this.id).toHashCode();
		} else {
			return System.identityHashCode(this);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Parent)) {
			return false;
		}

		Parent that = (Parent) obj;
		return this.getId().equals(that.getId());
	}

}
