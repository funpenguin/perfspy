package com.cedarsoftware.util.io;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class JSonTest extends TestCase {

	public void testNull() throws Exception {
		System.out.println("------test null -----");

		List<String> ls = null;
		String json = JsonWriter.objectToJson(ls);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(ls, s2);
	}

	public void testNullInAClass() throws Exception {
		System.out.println("------test nullInAClass -----");
		class A {
			List<String> ls = null;

			@Override
			public boolean equals(Object obj) {
				A that = (A) obj;
				return this.ls == that.ls;
			}
		}

		A a = new A();
		String json = JsonWriter.objectToJson(a);
		System.out.println(json);

		A a2 = (A) JsonReader.toJava(json);
		System.out.println(a2);

		assertEquals(a, a2);
	}

	public void testString() throws Exception {
		System.out.println("------test String -----");
		String s = "abcd";
		String json = JsonWriter.objectToJson(s);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(s, s2);

	}

	public void testEmptyString() throws Exception {
		System.out.println("------test String -----");
		String s = "";
		String json = JsonWriter.objectToJson(s);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(s, s2);

	}

	public void testStringInAClass() throws Exception {
		System.out.println("------test StringInAClass -----");

		class B {
			String bs = "b";

			public boolean equals(Object obj) {
				B that = (B) obj;
				return this.bs.equals(that.bs);
			};
		}

		class A {
			String as = "a";
			B b = new B();

			@Override
			public boolean equals(Object obj) {
				A that = (A) obj;
				if (!as.equals(that.as)) {
					return false;
				}

				return this.b.equals(that.b);
			}
		}

		A a = new A();
		String json = JsonWriter.objectToJson(a);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(a, s2);

	}

	public void testInt() throws Exception {
		System.out.println("------test Int -----");
		int i = 56;

		String json = JsonWriter.objectToJson(i);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(i, s2);
	}

	public void testLong() throws Exception {
		System.out.println("------test Long -----");
		long l = 56L;

		String json = JsonWriter.objectToJson(l);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(l, s2);
	}

	public void testFloat() throws Exception {
		System.out.println("------test Float -----");
		Float f = Float.NEGATIVE_INFINITY;

		String json = JsonWriter.objectToJson(f);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(f, s2);

		f = Float.POSITIVE_INFINITY;
		json = JsonWriter.objectToJson(f);
		System.out.println(json);

		s2 = JsonReader.toJava(json);
		System.out.println(s2);
		assertEquals(f, s2);

		Float[] fs = new Float[] { 1.5f, -1.5f, Float.POSITIVE_INFINITY };
		json = JsonWriter.objectToJson(fs);
		System.out.println(json);

		s2 = JsonReader.toJava(json);
		System.out.println(s2);
	}

	public void testLongInClass() throws Exception {
		class LongClass {
			long l = 1370836497636l;

			@Override
			public boolean equals(Object obj) {
				LongClass that = (LongClass) obj;
				return this.l == that.l;
			}
		}

		LongClass lc = new LongClass();
		String json = JsonWriter.objectToJson(lc);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(lc, s2);
	}

	public void testByte() throws Exception {
		System.out.println("------test Byte -----");
		byte b = 56;

		String json = JsonWriter.objectToJson(b);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(b, s2);
	}

	public void testChar() throws Exception {
		System.out.println("------test char -----");
		char c = 'c';

		String json = JsonWriter.objectToJson(c);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(c, s2);
	}

	public void testTimStamp() throws Exception {
		java.sql.Timestamp ts = new Timestamp(System.currentTimeMillis());
		System.out.println(ts);

		String json = JsonWriter.objectToJson(ts);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(ts, s2);
	}

	public void testDate() throws Exception {
		System.out.println("------test Date -----");
		java.util.Date date = new java.util.Date();

		String json = JsonWriter.objectToJson(date);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(date, s2);

	}

	public void testClass() throws Exception {
		System.out.println("------test Class -----");
		Class c = String.class;

		String json = JsonWriter.objectToJson(c);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals(c, s2);
	}

	public void testReadStringJson() throws Exception {
		String json = "{\"@t\":\"string\",\"@v\":\"abcd\"}";
		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertEquals("abcd", s2);
	}

	public void testEmptyArrayList() throws Exception {
		class A {
			ArrayList<String> list = new ArrayList<String>();
		}

		String json = JsonWriter.objectToJson(new A());
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertTrue(s2 instanceof A);
		assertTrue(((A) s2).list.isEmpty());
	}

	public void testWriteEnum() throws Exception {
		String json = JsonWriter.objectToJson(MonthEnum.January);
		System.out.println(json);

		Object s2 = JsonReader.toJava(json);
		System.out.println(s2);

		assertTrue(s2.equals(MonthEnum.January));
	}

	public void testReadJsonObject() throws Exception {
		class A5 {
			String f1 = "abc";
			A5 self = this;
		}

		A5 a5 = new A5();
		String json = JsonWriter.objectToJson(a5);
		System.out.println(json);

	}

}

enum MonthEnum {

	January(1), Febuary(2);

	int index;

	MonthEnum(int index0) {
		index = index0;
	}

}