package com.cedarsoftware.util.io;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import com.cedarsoftware.util.config.JSonIOConfig;

public class CollectionThresholdTest extends TestCase {

	public void testWriteArray() throws Exception {

		D[] d = new D[20];
		for (int i = 0; i < d.length; i++) {
			d[i] = new D("d-" + i);
		}

		JSonIOConfig ioConfig = new JSonIOConfig();
		System.out.println("-----------------limit array threshold to 5");
		ioConfig.addCollectionLimit(D.class, 5);

		String json = JsonWriter.objectToJson(d, ioConfig);
		System.out.println(json);

		D[] d2 = (D[]) JsonReader.toJava(json);
		assertTrue(d2.length == 5);
		for (int i = 0; i < d2.length; i++) {
			assertEquals(d[i], d2[i]);
		}

		System.out.println("-----------------limit array threshold to 20");
		ioConfig.addCollectionLimit(D.class, 20);
		json = JsonWriter.objectToJson(d);
		System.out.println(json);

		d2 = (D[]) JsonReader.toJava(json);
		assertTrue(d2.length == 20);
		for (int i = 0; i < d2.length; i++) {
			assertEquals(d[i], d2[i]);
		}

		ioConfig = new JSonIOConfig();

		System.out.println("-----------------set default array threshold to 5");
		ioConfig.setDefaultLimit(5);
		json = JsonWriter.objectToJson(d, ioConfig);
		System.out.println(json);

		d2 = (D[]) JsonReader.toJava(json);
		assertTrue(d2.length == 5);
		for (int i = 0; i < d2.length; i++) {
			assertEquals(d[i], d2[i]);
		}
	}

	public void testWriteCollections() throws Exception {
		JSonIOConfig ioConfig = new JSonIOConfig();

		ioConfig.setDefaultLimit(5);
		ioConfig.addCollectionLimit(B.class, -1);
		ioConfig.addCollectionLimit(C.class, 2);

		C c = new C();

		for (int i = 0; i < 5000; i++) {
			C c1 = new C();
			c.addChild(c1);
		}

		C c2 = new C();
		c.addChild(c2);

		B[] bs = new B[50];
		for (int i = 0; i < 50; i++) {
			B b = new B();
			b.setC(c);
			bs[i] = b;
		}

		A a = new A();
		a.setB(bs);

		String json = JsonWriter.objectToJson(a, ioConfig);
		System.out.println(json);
		A newA = (A) JsonReader.toJava(json);
		int bThreshold = ioConfig.getCollectionThreshhold(B.class.getName());
		if (bs.length > bThreshold) {
			assertTrue(newA.getB().length == bThreshold);
		} else {
			assertTrue(newA.getB().length == bs.length);
		}
		System.out.println(newA.getB().length);

		for (B b : newA.getB()) {
			int cThreshold = ioConfig.getCollectionThreshhold(C.class.getName());
			if (b.getC().children.size() > cThreshold) {
				assertTrue(b.getC().children.size() == cThreshold);
			} else {
				assertTrue(b.getC().children.size() == b.getC().children.size());
			}
			System.out.println(b.getC().children.size());
		}
	}

	public void testWriteMap() throws Exception {
		JSonIOConfig ioConfig = new JSonIOConfig();

		ioConfig.setDefaultLimit(5);

		Map<Integer, C> cs = new LinkedHashMap<Integer, CollectionThresholdTest.C>();

		for (int i = 0; i < 5000; i++) {
			C c1 = new C();
			cs.put(i, c1);
		}

		String json = JsonWriter.objectToJson(cs, ioConfig);
		System.out.println(json);

		Map<Integer, C> cs2 = (Map) JsonReader.toJava(json);

		assertTrue(cs2.size() == ioConfig.getCollectionThreshhold(cs.getClass().getName()));

		System.out.println(cs2);

		Iterator<Integer> it = cs2.keySet().iterator();
		int index = 0;
		while (it.hasNext()) {
			assertTrue(it.next() == index);
			index++;
		}

	}

	class A {
		private B[] b = new B[50];

		public B[] getB() {
			return b;
		}

		public void setB(B[] b) {
			this.b = b;
		}
	}

	class B {
		private C c;

		public C getC() {
			return c;
		}

		public void setC(C c) {
			this.c = c;
		}
	}

	class C {
		private List<C> children = new ArrayList<C>();
		private C parent;

		public void addChild(C a) {
			children.add(a);
			a.parent = this;
		}
	}

	class D {
		private String ds;

		public D(String ds) {
			this.ds = ds;
		}

		@Override
		public String toString() {
			return ds;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}

			if (!(obj instanceof D)) {
				return false;
			}

			D that = (D) obj;
			return this.ds.equals(that.ds);
		}

		@Override
		public int hashCode() {
			return this.ds.hashCode();
		}

	}
}
