package com.cedarsoftware.util.io;

import java.lang.reflect.Constructor;

import junit.framework.TestCase;

public class NestedClassTest extends TestCase {
	private static class A {
		String as = "as";

		A() {
			// TODO Auto-generated constructor stub
		}
	}

	public void testContructors() {
		A a = new A();

		Constructor<?>[] declaredConstructors = A.class.getConstructors();
		System.out.println("this is publicly accessible contructors");
		for (Constructor<?> constructor : declaredConstructors) {
			System.out.println(constructor);
		}

		declaredConstructors = A.class.getDeclaredConstructors();
		System.out.println("this is contructors declared by this class");
		for (Constructor<?> constructor : declaredConstructors) {
			System.out.println(constructor);
		}

		declaredConstructors = TestJsonReaderWriter.TestClass.class.getConstructors();
		System.out.println("this is publicly accessible contructors");
		for (Constructor<?> constructor : declaredConstructors) {
			System.out.println(constructor);
		}

		declaredConstructors = TestJsonReaderWriter.TestClass.class.getDeclaredConstructors();
		System.out.println("this is contructors declared by this class");
		for (Constructor<?> constructor : declaredConstructors) {
			System.out.println(constructor);
		}
	}
}
