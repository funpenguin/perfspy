package com.cedarsoftware.util.io;

import junit.framework.TestCase;

public class LevelTest extends TestCase {

	public void testLevel() throws Exception {

		C parent = new C();

		C parent0 = parent;
		for (int i = 0; i < 200; i++) {
			C child = new C();
			parent.setChild(child);

			parent = child;
		}

		String json = JsonWriter.objectToJson(parent0);
		System.out.println(json);
	}

	static class C {
		static int level = 0;

		String cs;
		C child;
		C parent;

		public C() {
			cs = "this is String at " + level;// NDC.getDepth();
			level++;
			// NDC.push("");

		}

		public C getChild() {
			return child;
		}

		public void setChild(C child) {
			this.child = child;
		}

		public C getParent() {
			return parent;
		}

		public void setParent(C parent) {
			this.parent = parent;
		}
	}

}
