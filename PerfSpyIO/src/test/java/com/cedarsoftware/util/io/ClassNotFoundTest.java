package com.cedarsoftware.util.io;

import junit.framework.TestCase;

/**
 * 
 * Test JSonReader when it can't instantiate an object. If JSonReader can't
 * initiate an object, it is expected to gracefully set that object to null and
 * read as much a object graph as possible, instead of throwing an exception and
 * return null for the whole object graph
 * 
 * First run testWrite, then run testRead.
 * 
 * @author Apple
 * 
 */
public class ClassNotFoundTest extends TestCase {

	private String fileName = "ClassNotFoundTest.txt";

	public void testWrite() {
		class B {
			String bs;

			public B(String bs) {
				if (bs == null || bs.length() == 0) {
					throw new RuntimeException("bs can't be empty");
				}
				this.bs = bs;
			}

		}

		class A {
			String as = "AString";
			B b;

			public A() {
				System.out.println("A contructor with empty parameters");
			}

			public A(B b) {
				this.b = b;
			}

			public String getAs() {
				return as;
			}

			public void setAs(String as) {
				this.as = as;
			}

		}

		A a = new A(new B("BString"));
		a.setAs("this is an string in a");

		JsonWriter.toJsonFile(a, fileName);
	}

	public void testRead() {
		Object obj = JsonReader.jsonFileToJava(ClassNotFoundTest.class, fileName);
		System.out.println(obj);
		// B will not be initialized, because B() contructor will fail, however,
		// A should still be created
		assertTrue(obj.getClass().getName()
				.equals("com.cedarsoftware.util.io.ClassNotFoundTest$1A"));

		Object asValue = ReflectionUtil.getFieldValue(obj, "as");
		assertEquals("this is an string in a", asValue);
	}
}
