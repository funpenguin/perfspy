package com.cedarsoftware.util.io;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ReflectionUtil {
	private static final Log logger = LogFactory.getLog(ReflectionUtil.class);

	public static Object getFieldValue(Object obj, String fieldName) {
		Field field = getField(obj, fieldName);
		return getFieldValue(obj, field);
	}

	public static Field[] getFields(Object obj) {
		Set<Field> fields = new HashSet<Field>();

		Class<? extends Object> clz = obj.getClass();
		// get all fields declared by this class
		Field[] declaredFields = clz.getDeclaredFields();
		for (Field field0 : declaredFields) {
			// do not include statid fields
			if ((field0.getModifiers() & Modifier.STATIC) != 0) {
				continue;
			}
			fields.add(field0);
		}

		// get all public fields declared by this class or its
		// superclass/interfact
		declaredFields = clz.getFields();
		for (Field field0 : declaredFields) {
			// do not include statid fields
			if ((field0.getModifiers() & Modifier.STATIC) != 0) {
				continue;
			}
			fields.add(field0);
		}

		// get all protected fields declared by its superclass
		clz = obj.getClass().getSuperclass();
		while (!Object.class.equals(clz) && clz != null) {
			declaredFields = clz.getDeclaredFields();
			for (Field field0 : declaredFields) {

				if ((field0.getModifiers() & Modifier.PROTECTED) != 0) {
					if ((field0.getModifiers() & Modifier.STATIC) == 0) {
						fields.add(field0);
					}

				}

			}
			clz = clz.getSuperclass();
		}
		return fields.toArray(new Field[0]);
	}

	private static Field getField(Object obj, String fieldName) {
		Class clz = obj.getClass();
		while (!Object.class.equals(clz) && clz != null) {
			Field field = null;
			try {
				field = clz.getDeclaredField(fieldName);
			} catch (Exception e) {
				// ignore
			}
			if (field != null) {
				return field;
			}
			clz = clz.getSuperclass();
		}
		return null;

	}

	public static Object getFieldValue(Object obj, Field field) {
		try {
			if (field == null) {
				return null;
			}

			if (!field.isAccessible()) {
				field.setAccessible(true);
			}
			return field.get(obj);
		} catch (Exception e) {
			throw new IllegalStateException("failed to get field " + field + " from " + obj, e);
		}
	}

	public static Object invokeMethod(Object target, String methodName,
			Map<Class, Object>... params) {

		Collection<Class> paramTypes = new ArrayList<Class>();
		if (params.length > 0) {
			paramTypes = params[0].keySet();
		}
		Method method = findMethod(target.getClass(), methodName,
				paramTypes.toArray(new Class[] {}));

		if (method == null) {
			throw new IllegalStateException("method " + methodName + " with params ( " + paramTypes
					+ " ) doesn't exist");
		}
		Collection<Object> paramValues = new ArrayList<Object>();
		if (params.length > 0) {
			for (Object param : paramTypes) {
				paramValues.add(params[0].get(param));
			}
		}

		return invokeMethod(method, target, paramValues.toArray(new Object[] {}));
	}

	private static Object invokeMethod(Method method, Object target, Object... args) {
		try {
			method.setAccessible(true);
			return method.invoke(target, args);
		}

		catch (Exception ex) {
			throw new IllegalStateException("failed to invoke method " + method + " on target "
					+ target, ex);

		}
	}

	private static Method findMethod(Class<?> clazz, String name, Class<?>... paramTypes) {

		Class<?> searchType = clazz;

		while (searchType != null) {
			Method[] methods = (searchType.isInterface() ? searchType.getMethods() : searchType
					.getDeclaredMethods());

			for (Method method : methods) {

				if (name.equals(method.getName())

				&& (paramTypes == null || Arrays.equals(paramTypes, method.getParameterTypes()))) {

					return method;
				}
			}
			searchType = searchType.getSuperclass();
		}

		return null;
	}

}
