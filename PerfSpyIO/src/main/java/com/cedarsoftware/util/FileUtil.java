package com.cedarsoftware.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import com.cedarsoftware.util.config.JSonIOConfig;

public class FileUtil {

	public static InputStream getResourceAsStream(Class callingFrom, String fileName)
			throws Exception {
		if (callingFrom == null) {
			callingFrom = JSonIOConfig.class;
		}
		InputStream stream = callingFrom.getResourceAsStream(fileName);
		if (stream == null) {
			stream = callingFrom.getClassLoader().getResourceAsStream(fileName);
		}
		if (stream == null) {
			stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
		}
	
		if (stream == null) {
			stream = new FileInputStream(new File(fileName));
		}
	
		return stream;
	}

}
