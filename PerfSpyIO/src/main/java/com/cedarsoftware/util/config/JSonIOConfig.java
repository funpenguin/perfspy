package com.cedarsoftware.util.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.InputSource;

import com.cedarsoftware.util.FileUtil;
import com.cedarsoftware.util.TypeUtil;

public class JSonIOConfig {

	private static final String JDK_TYPE = "java.";

	class CollectionLimit {
		private int defaultLimit = -1;
		private Map<String, Integer> type2Limit = new HashMap<String, Integer>();

		private void addType(String type, int limit) {
			this.type2Limit.put(type, limit);
		}

		private int getCollectionLimit(String type) {
			if (type2Limit.containsKey(type)) {
				Integer t = type2Limit.get(type);
				return t == -1 ? Integer.MAX_VALUE : t;
			}

			return defaultLimit == -1 ? Integer.MAX_VALUE : defaultLimit;

		}

		private void clear() {
			this.defaultLimit = -1;
			this.type2Limit.clear();
		}
	}

	private List<String> includeTypes = new ArrayList<String>();
	private List<String> excludeTypes = new ArrayList<String>();
	private CollectionLimit collectionLimit = new CollectionLimit();
	private int goNoFurtherLevel = Integer.MAX_VALUE;
	private Set<String> acceptWritings = new HashSet<String>();
	private Set<String> skipWritings = new HashSet<String>();

	//	private static JSonIOConfig configuration = new JSonIOConfig();

	/**
	 * these methods are only here to faciliate testing
	 */
	public void addCollectionLimit(Class cls, int limit) {
		collectionLimit.addType(cls.getName(), limit);
	}

	/**
	 * these methods are only here to faciliate testing
	 */
	public void setDefaultLimit(int limit) {
		collectionLimit.defaultLimit = limit;
	}

	/**
	 * these methods are only here to faciliate testing
	 */
	public void excludeType(Class cls) {
		excludeTypes.add(cls.getName());
	}

	/**
	 * these methods are only here to faciliate testing
	 */
	public void includeType(Class cls) {
		includeTypes.add(cls.getName());
	}

	/**
	 * these methods are only here to faciliate testing
	 */
	public void setGoNoFurtherLevel(int goNoFurtherLevel) {
		this.goNoFurtherLevel = goNoFurtherLevel;
	}

	public int getGoNoFurtherLevel() {
		return goNoFurtherLevel;
	}

	public static JSonIOConfig config(String fileName) throws Exception {
		JSonIOConfig config = new JSonIOConfig();
		org.dom4j.Document doc = config.readConfigFile(null, fileName);
		Element jsonioEle = doc.getRootElement();
		config.parseConfigFile(jsonioEle);
		return config;
	}

	public static JSonIOConfig config(Class callingFrom, String fileName) throws Exception {
		JSonIOConfig config = new JSonIOConfig();
		org.dom4j.Document doc = config.readConfigFile(callingFrom, fileName);
		Element jsonioEle = doc.getRootElement();
		config.parseConfigFile(jsonioEle);
		return config;
	}

	public static JSonIOConfig configFromJSonIOElement(Element jsonEle) throws Exception {
		JSonIOConfig config = new JSonIOConfig();
		config.parseConfigFile(jsonEle);
		return config;
	}

	private org.dom4j.Document readXmlFragments(String xml) throws Exception {
		SAXReader reader = new SAXReader();
		return reader.read(new StringReader(xml));
	}

	private org.dom4j.Document readConfigFile(Class callingFrom, String fileName) throws Exception {
		InputStream stream = FileUtil.getResourceAsStream(callingFrom, fileName);
		if (stream == null) {
			throw new IOException("can't find file " + fileName);
		}
		org.dom4j.Document doc;
		try {
			SAXReader saxReader = new SAXReader();
			doc = saxReader.read(new InputSource(stream));
		} catch (DocumentException e) {
			throw new Exception("Could not parse configuration: " + fileName, e);
		} finally {
			stream.close();
		}
		return doc;
	}

	private void parseConfigFile(Element jsonio) {
		if (jsonio == null) {
			return;
		}
		Element goNoFurtherLevelEle = jsonio.element("GoNoFurtherLevel");
		if (goNoFurtherLevelEle != null) {

			String text = goNoFurtherLevelEle.getText();
			if (text != null && text.trim().length() > 0) {
				goNoFurtherLevel = Integer.parseInt(text.trim());
			}

		}

		Element types = jsonio.element("Types");
		if (types == null) {
			return;
		}

		List<Element> includeTypeEles = types.elements("include");
		for (Element ele : includeTypeEles) {
			includeTypes.add(ele.attributeValue("within"));
		}
		//sort so the longest will get matched first
		Collections.sort(includeTypes, new Comparator<String>() {
			public int compare(String t1, String t2) {
				return t2.length() - t1.length();
			};
		});

		List<Element> excludeTypeEles = types.elements("exclude");
		for (Element ele : excludeTypeEles) {
			excludeTypes.add(ele.attributeValue("within"));
		}
		Collections.sort(excludeTypes, new Comparator<String>() {
			public int compare(String t1, String t2) {
				return t2.length() - t1.length();
			};

		});

		Element colEle = jsonio.element("collection");
		if (colEle != null) {
			String strTemp = colEle.attributeValue("limit");
			Integer intTemp = Integer.parseInt(strTemp);
			if (intTemp != null) {
				collectionLimit.defaultLimit = intTemp;
			}

			List<Element> colTypeEles = colEle.elements("type");
			for (Element colType : colTypeEles) {
				String type = colType.getStringValue();
				String t = colType.attributeValue("limit");
				collectionLimit.addType(type, Integer.parseInt(t));
			}
		}
	}

	private String isTypeIncludedForWriting(String type) {
		for (String include : includeTypes) {
			if (type.startsWith(include)) {

				return include;
			}
		}
		return null;
	}

	private String isTypeExcludedForWritting(String type) {
		for (String exclude : excludeTypes) {
			if (type.startsWith(exclude)) {
				return exclude;
			}
		}
		return null;
	}

	public boolean acceptWrittingType(String type0) {
		if (TypeUtil.isPrimitiveOrWrapper(type0)) {
			return true;
		}

		if (includeTypes.size() == 0 && excludeTypes.size() == 0) {
			return true;
		}

		if (includeTypes.size() == 0) {
			return isTypeExcludedForWritting(type0) == null;
		}

		if (acceptWritings.contains(type0)) {
			return true;
		}

		if (skipWritings.contains(type0)) {
			return false;
		}

		String type = type0;
		//array types need to be treated specially, the class name is
		//prefixed with "[L" and appendixed with ";", such as
		//"[Lcom.cedarsoftware.util.io.B;"
		if (type0.startsWith("[L")) {
			type = type0.substring(2);
		}

		if (type.startsWith(JDK_TYPE)) {
			acceptWritings.add(type0);
			return true;
		}

		if (excludeTypes.size() == 0) {
			boolean accept = isTypeIncludedForWriting(type) != null;
			if (accept) {
				acceptWritings.add(type0);
			} else {
				skipWritings.add(type0);
			}
			return accept;
		}

		String includedBy = isTypeIncludedForWriting(type);
		if (includedBy != null) {
			String excludedBy = isTypeExcludedForWritting(type);
			if (excludedBy == null) {
				acceptWritings.add(type0);
				return true;
			} else {
				if (includedBy.length() > excludedBy.length()) {
					acceptWritings.add(type0);
					return true;
				} else {
					skipWritings.add(type0);
					return false;
				}
			}
		}
		skipWritings.add(type0);
		return false;

	}

	public int getCollectionThreshhold(String type) {
		return collectionLimit.getCollectionLimit(type);
	}

	// public static boolean isTypeNotIncludedForWriting(String type) {
	// return isTypeExcludedForWritting(type);
	// }
}
