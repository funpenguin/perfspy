package com.cedarsoftware.util.hibernate;

import java.lang.reflect.Field;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HibernateUtil {
	private static final Log logger = LogFactory.getLog(HibernateUtil.class);

	public static String getHibernateCollectionType(Object col) {
		// AbstractPersistentCollection col2 = (AbstractPersistentCollection)
		// col;
		/**
		 * Apple:if uninitialized, do not strip to the true collection type
		 * thus when the json is read back into an object, accessing collection
		 * will throw uninitialized exception, just like what you will get if
		 * you try to access the collection before it was converted to json
		 */
		boolean notInitialized = isHibernateUnintializedCollection(col);
		if (notInitialized) {
			return col.getClass().getName();
		}

		Object trueCol;

		Field field = getField(col, "list");
		if (field != null) {
			trueCol = getFieldValue(col, field);
			return trueCol.getClass().getName();
		}

		field = getField(col, "map");
		if (field != null) {
			trueCol = getFieldValue(col, field);
			return trueCol.getClass().getName();
		}

		field = getField(col, "set");
		if (field != null) {
			trueCol = getFieldValue(col, field);
			return trueCol.getClass().getName();
		}

		logger.debug("can't determin the true collection type for the hibernate collection " + col);
		return col.getClass().getName();
	}

	public static boolean isHibernateUnintializedCollection(Object col) {
		if (!isHibernateCollectionType(col)) {
			return false;
		}
		boolean wasInitialized = (Boolean) getFieldValue(col, "initialized");
		return !wasInitialized;
	}

	public static boolean isHibernateCollectionType(Object col) {
		if (col.getClass().getName().startsWith("org.hibernate.collection")) {
			return true;
		}
		return false;
	}

	public static boolean isHibernateLazyEnhanceByCGLIBObject(String clzName) {
		if (clzName.contains("EnhancerByCGLIB")) {
			return true;
		}
		return false;
	}

	private static Object getFieldValue(Object obj, String fieldName) {
		Field field = getField(obj, fieldName);
		return getFieldValue(obj, field);
	}

	private static Field getField(Object obj, String fieldName) {
		Class clz = obj.getClass();
		while (!Object.class.equals(clz) && clz != null) {
			Field field = null;
			try {
				field = clz.getDeclaredField(fieldName);
			} catch (Exception e) {
				// ignore
			}
			if (field != null) {
				return field;
			}
			clz = clz.getSuperclass();
		}
		return null;
	}

	private static Object getFieldValue(Object obj, Field field) {
		try {
			if (field == null) {
				return null;
			}

			if (!field.isAccessible()) {
				field.setAccessible(true);
			}
			return field.get(obj);
		} catch (Exception e) {
			logger.debug("failed to get field " + field + " from " + obj, e);
			return null;
		}

	}
}
