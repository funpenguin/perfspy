package com.cedarsoftware.util.io;

import com.cedarsoftware.util.SafeSimpleDateFormat;

public class JsonConstant {
	public static final String JSON_KEYS = "@ks";
	public static final String JSON_ITEMS = "@vs";
	public static final String JSON_TYPE = "@t";
	public static final String JSON_ID = "@i";
	public static final String JSON_REF = "@r";
	public static final String JSON_VALUE = "@v";
	public static final String JSON_HAHSCODE = "@h";

	static SafeSimpleDateFormat _dateFormat = new SafeSimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	static final String NULL_OBJECT = "null"; // compared with ==

}
