package com.cedarsoftware.util.io;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.NDC;

import com.cedarsoftware.util.TypeUtil;
import com.cedarsoftware.util.config.JSonIOConfig;
import com.cedarsoftware.util.hibernate.HibernateUtil;

/**
 * Output a Java object graph in JSON format. This code handles cyclic
 * references and can serialize any Object graph without requiring a class to be
 * 'Serializeable' or have any specific methods on it. <br/>
 * <ul>
 * <li>
 * Call the static method: <code>JsonWriter.toJson(employee)</code>. This will
 * convert the passed in 'employee' instance into a JSON String.</li>
 * <li>Using streams:
 * 
 * <pre>
 * JsonWriter writer = new JsonWriter(stream);
 * writer.write(employee);
 * writer.close();
 * </pre>
 * 
 * This will write the 'employee' object to the passed in OutputStream.</li>
 * <p>
 * That's it. This can be used as a debugging tool. Output an object graph using
 * the above code. You can copy that JSON output into this site which formats it
 * with a lot of whitespace to make it human readable:
 * http://jsonformatter.curiousconcept.com <br/>
 * <br/>
 * <p>
 * This will output any object graph deeply (or null). Object references are
 * properly handled. For example, if you had A->B, B->C, and C->A, then A will
 * be serialized with a B object in it, B will be serialized with a C object in
 * it, and then C will be serialized with a reference to A (ref), not a
 * redefinition of A.
 * </p>
 * <br/>
 * 
 * @author John DeRegnaucourt (jdereg@gmail.com) <br/>
 *         Copyright (c) John DeRegnaucourt <br/>
 * <br/>
 *         Licensed under the Apache License, Version 2.0 (the "License"); you
 *         may not use this file except in compliance with the License. You may
 *         obtain a copy of the License at <br/>
 * <br/>
 *         http://www.apache.org/licenses/LICENSE-2.0 <br/>
 * <br/>
 *         Unless required by applicable law or agreed to in writing, software
 *         distributed under the License is distributed on an "AS IS" BASIS,
 *         WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *         implied. See the License for the specific language governing
 *         permissions and limitations under the License.
 */
public class JsonWriter implements Closeable, Flushable {
	private static final String THIS$0 = "this$0";
	private final Map<Object, Long> _objVisited = new IdentityHashMap<Object, Long>();
	private final IdentityHashMap<Object, Boolean> _firstTimeVisit = new IdentityHashMap<Object, Boolean>(256);
	private static final Map<String, ClassMeta> _classMetaCache = new ConcurrentHashMap<String, ClassMeta>();
	private static final List<Object[]> _writers = new ArrayList<Object[]>();
	private static final Set<Class> _notCustom = new HashSet<Class>();
	private static Object[] _byteStrings = new Object[256];
	private final Writer _out;
	private long _identity = 1;
	private static final Log logger = LogFactory.getLog(JsonWriter.class);
	private JSonIOConfig ioConfig = new JSonIOConfig();
	private boolean logSystemHashcode = false;
	private boolean logFieldType = false;

	static { // Add customer writers (these make common classes more succinct)
		addWriter(String.class, new JsonStringWriter());
		addWriter(Date.class, new DateWriter());
		addWriter(Timestamp.class, new TimestampWriter());
		addWriter(BigInteger.class, new BigIntegerWriter());
		addWriter(BigDecimal.class, new BigDecimalWriter());
		addWriter(TimeZone.class, new TimeZoneWriter());
		addWriter(Calendar.class, new CalendarWriter());
		addWriter(Locale.class, new LocaleWriter());
		addWriter(StringBuilder.class, new StringBuilderWriter());
		addWriter(StringBuffer.class, new StringBufferWriter());
		addWriter(Class.class, new ClassWriter());
	}

	public interface JsonClassWriter {
		void write(Object o, boolean showType, Writer out) throws IOException;

		boolean hasPrimitiveForm();

		void writePrimitiveForm(Object o, Writer out) throws IOException;
	}

	public static int getDistance(Class a, Class b) {
		Class curr = b;
		int distance = 0;

		while (curr != a) {
			distance++;
			curr = curr.getSuperclass();
			if (curr == null) {
				return Integer.MAX_VALUE;
			}
		}

		return distance;
	}

	public boolean writeIfMatching(Object o, boolean showType, Writer out) throws IOException {
		Class c = o.getClass();
		if (_notCustom.contains(c)) {
			return false;
		}

		return writeCustom(c, o, showType, out);
	}

	public boolean writeArrayElementIfMatching(Class arrayComponentClass, Object o, boolean showType, Writer out)
			throws IOException {
		if (!o.getClass().isAssignableFrom(arrayComponentClass) || _notCustom.contains(o.getClass())) {
			return false;
		}

		return writeCustom(arrayComponentClass, o, showType, out);
	}

	private boolean writeCustom(Class arrayComponentClass, Object o, boolean showType, Writer out) throws IOException {
		JsonClassWriter closestWriter = null;
		int minDistance = Integer.MAX_VALUE;

		if (TypeUtil.isPrimitiveOrWrapperObj(o)) {
			closestWriter = new PrimitiveWriter();
		} else {
			for (Object[] item : _writers) {
				Class clz = (Class) item[0];
				int distance = getDistance(clz, arrayComponentClass);
				if (distance < minDistance) {
					minDistance = distance;
					closestWriter = (JsonClassWriter) item[1];
				}
			}
		}

		if (closestWriter == null) {
			return false;
		}

		if (writeOptionalReference(o)) {
			return true;
		}

		if (!this._objVisited.containsKey(o) && o != null && !TypeUtil.isPrimitiveObj(o)) {
			this._objVisited.put(o, this._identity++);
		}

		/*Apple:get referencibility from _objVisisted*/
		//		boolean referenced = _objsReferenced.containsKey(o);
		boolean referenced = this._objVisited.containsKey(o);

		if (!referenced && !showType && closestWriter.hasPrimitiveForm()) {
			closestWriter.writePrimitiveForm(o, out);
			return true;
		}

		out.write('{');
		if (referenced) {
			writeId(getId(o));
			if (showType) {
				out.write(',');
			}
		}

		if (showType) {
			writeType(o, out);
		}

		if (referenced || showType) {
			out.write(',');
		}

		closestWriter.write(o, showType, out);
		out.write('}');
		return true;
	}

	public static void addWriter(Class c, JsonClassWriter writer) {
		for (Object[] item : _writers) {
			Class clz = (Class) item[0];
			if (clz.equals(c)) {
				item[1] = writer; // Replace writer
				return;
			}
		}
		_writers.add(new Object[] { c, writer });
	}

	public static void addNotCustomWriter(Class c) {
		_notCustom.add(c);
	}

	public static class TimeZoneWriter implements JsonClassWriter {
		public void write(Object obj, boolean showType, Writer out) throws IOException {
			TimeZone cal = (TimeZone) obj;
			out.write("\"zone\":\"");
			out.write(cal.getID());
			out.write('"');
		}

		public boolean hasPrimitiveForm() {
			return false;
		}

		public void writePrimitiveForm(Object o, Writer out) throws IOException {
		}
	}

	public static class CalendarWriter implements JsonClassWriter {
		public void write(Object obj, boolean showType, Writer out) throws IOException {
			Calendar cal = (Calendar) obj;
			JsonConstant._dateFormat.setTimeZone(cal.getTimeZone());
			out.write("\"time\":\"");
			out.write(JsonConstant._dateFormat.format(cal.getTime()));
			out.write("\",\"zone\":\"");
			out.write(cal.getTimeZone().getID());
			out.write('"');
		}

		public boolean hasPrimitiveForm() {
			return false;
		}

		public void writePrimitiveForm(Object o, Writer out) throws IOException {
		}
	}

	public static class DateWriter implements JsonClassWriter {
		public void write(Object obj, boolean showType, Writer out) throws IOException {
			String value = Long.toString(((Date) obj).getTime());
			out.write("\"" + JsonConstant.JSON_VALUE + "\":");
			out.write(value);
		}

		public boolean hasPrimitiveForm() {
			return true;
		}

		public void writePrimitiveForm(Object o, Writer out) throws IOException {
			out.write(Long.toString(((Date) o).getTime()));
		}
	}

	public static class ClassWriter implements JsonClassWriter {
		public void write(Object obj, boolean showType, Writer out) throws IOException {
			String value = ((Class) obj).getName();
			out.write("\"" + JsonConstant.JSON_VALUE + "\":");
			writeJsonUtf8String(value, out);
		}

		public boolean hasPrimitiveForm() {
			return true;
		}

		public void writePrimitiveForm(Object o, Writer out) throws IOException {
			writeJsonUtf8String(((Class) o).getName(), out);
		}
	}

	public static class JsonStringWriter implements JsonClassWriter {
		public void write(Object obj, boolean showType, Writer out) throws IOException {
			out.write("\"" + JsonConstant.JSON_VALUE + "\":");
			writeJsonUtf8String((String) obj, out);
		}

		public boolean hasPrimitiveForm() {
			return true;
		}

		public void writePrimitiveForm(Object o, Writer out) throws IOException {
			writeJsonUtf8String((String) o, out);
		}
	}

	public static class LocaleWriter implements JsonClassWriter {
		public void write(Object obj, boolean showType, Writer out) throws IOException {
			Locale locale = (Locale) obj;

			out.write("\"language\":\"");
			out.write(locale.getLanguage());
			out.write("\",\"country\":\"");
			out.write(locale.getCountry());
			out.write("\",\"variant\":\"");
			out.write(locale.getVariant());
			out.write('"');
		}

		public boolean hasPrimitiveForm() {
			return false;
		}

		public void writePrimitiveForm(Object o, Writer out) throws IOException {
		}
	}

	public static class BigIntegerWriter implements JsonClassWriter {
		public void write(Object obj, boolean showType, Writer out) throws IOException {
			BigInteger big = (BigInteger) obj;
			out.write("\"" + JsonConstant.JSON_VALUE + "\":\"");
			out.write(big.toString(10));
			out.write('"');
		}

		public boolean hasPrimitiveForm() {
			return true;
		}

		public void writePrimitiveForm(Object o, Writer out) throws IOException {
			BigInteger big = (BigInteger) o;
			out.write('"');
			out.write(big.toString(10));
			out.write('"');
		}
	}

	public static class PrimitiveWriter implements JsonClassWriter {
		public void write(Object obj, boolean showType, Writer out) throws IOException {

			if (showType) {
				out.write("\"" + JsonConstant.JSON_VALUE + "\":");
			}
			Class c = obj.getClass();

			if (c.equals(Character.class) || c.equals(char.class)) {
				out.write('\"');
				writeJsonUtf8Char((Character) obj, out);
				out.write('\"');
			} else {
				if (obj.equals(Float.POSITIVE_INFINITY) || obj.equals(Float.NEGATIVE_INFINITY)) {
					writeJsonUtf8String((String) obj.toString(), out);
				} else {
					out.write(obj.toString());
				}
			}
		}

		public boolean hasPrimitiveForm() {
			return true;
		}

		public void writePrimitiveForm(Object o, Writer out) throws IOException {
			BigInteger big = (BigInteger) o;
			out.write('"');
			out.write(big.toString(10));
			out.write('"');
		}
	}

	public static class BigDecimalWriter implements JsonClassWriter {
		public void write(Object obj, boolean showType, Writer out) throws IOException {
			BigDecimal big = (BigDecimal) obj;
			out.write("\"" + JsonConstant.JSON_VALUE + "\":\"");
			out.write(big.toPlainString());
			out.write('"');
		}

		public boolean hasPrimitiveForm() {
			return true;
		}

		public void writePrimitiveForm(Object o, Writer out) throws IOException {
			BigDecimal big = (BigDecimal) o;
			out.write('"');
			out.write(big.toPlainString());
			out.write('"');
		}
	}

	public static class StringBuilderWriter implements JsonClassWriter {
		public void write(Object obj, boolean showType, Writer out) throws IOException {
			StringBuilder builder = (StringBuilder) obj;
			out.write("\"" + JsonConstant.JSON_VALUE + "\":\"");
			out.write(builder.toString());
			out.write('"');
		}

		public boolean hasPrimitiveForm() {
			return true;
		}

		public void writePrimitiveForm(Object o, Writer out) throws IOException {
			StringBuilder builder = (StringBuilder) o;
			out.write('"');
			out.write(builder.toString());
			out.write('"');
		}
	}

	public static class StringBufferWriter implements JsonClassWriter {
		public void write(Object obj, boolean showType, Writer out) throws IOException {
			StringBuffer buffer = (StringBuffer) obj;
			out.write("\"" + JsonConstant.JSON_VALUE + "\":\"");
			out.write(buffer.toString());
			out.write('"');
		}

		public boolean hasPrimitiveForm() {
			return true;
		}

		public void writePrimitiveForm(Object o, Writer out) throws IOException {
			StringBuffer buffer = (StringBuffer) o;
			out.write('"');
			out.write(buffer.toString());
			out.write('"');
		}
	}

	public static class TimestampWriter implements JsonClassWriter {
		public void write(Object o, boolean showType, Writer out) throws IOException {
			Timestamp tstamp = (Timestamp) o;
			out.write("\"time\":\"");
			out.write(Long.toString((tstamp.getTime() / 1000) * 1000));
			out.write("\",\"nanos\":\"");
			out.write(Integer.toString(tstamp.getNanos()));
			out.write('"');
		}

		public boolean hasPrimitiveForm() {
			return false;
		}

		public void writePrimitiveForm(Object o, Writer out) throws IOException {
		}
	}

	static {
		for (short i = -128; i <= 127; i++) {
			char[] chars = Integer.toString(i).toCharArray();
			_byteStrings[i + 128] = chars;
		}
	}

	static class ClassMeta extends LinkedHashMap<String, Field> {
	}

	/**
	 * Convert a Java Object to a JSON String. This is the easy-to-use API - it
	 * returns null if there was an error.
	 * 
	 * @param item
	 *            Object to convert to a JSON String.
	 * @return String containing JSON representation of passed in object, or
	 *         null if an error occurred.
	 * @throws Exception 
	 */
	//	public static String objectToJson(Object item) throws Exception {
	//		return objectToJson(item);
	//	}
	//
	//	public static String objectToJson(Object item, JSonIOConfig config) throws Exception {
	//		return objectToJson(item, config);
	//	}

	/**
	 * Apple: written json to a file
	 * */
	public static void toJsonFile(Object item, String fileName) {
		OutputStream fileStream = null;
		try {
			fileStream = new FileOutputStream(new File(fileName));
			JsonWriter writer = new JsonWriter(fileStream);
			writer.write(item);
			writer.close();
		} catch (Exception e) {
			logger.error("can't write " + item + " to file " + fileName, e);
		} finally {
			if (fileStream != null) {
				try {
					fileStream.close();
				} catch (IOException e) {
					logger.error("can't close " + fileStream, e);
				}
			}
		}
	}

	/**
	 * Convert a Java Object to a JSON String.
	 * 
	 * @param item
	 *            Object to convert to a JSON String.
	 * @return String containing JSON representation of passed in object.
	 * @throws java.io.IOException
	 *             If an I/O error occurs
	 */
	public static String objectToJson(Object item) throws IOException {
		return objectToJson(item, null);
	}

	public static String objectToJson(Object item, boolean logSystemHashcode) throws IOException {
		return objectToJson(item, null, logSystemHashcode);
	}

	public static String objectToJson(Object item, JSonIOConfig config) throws IOException {
		return objectToJson(item, config, false);
	}

	public static String objectToJson(Object item, JSonIOConfig config, boolean logSystemHashcode) throws IOException {
		return objectToJson(item, config, logSystemHashcode, false);
	}

	public static String objectToJson(Object item, JSonIOConfig config, boolean logSystemHashcode, boolean logFieldType)
			throws IOException {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		JsonWriter writer = new JsonWriter(stream);
		if (config != null) {
			writer.ioConfig = config;
		}
		writer.logSystemHashcode = logSystemHashcode;
		writer.logFieldType = true;

		writer.write(item);
		writer.close();
		return new String(stream.toByteArray(), "UTF-8");
	}

	public JsonWriter(OutputStream out) throws IOException {
		try {
			_out = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new IOException("Unsupported encoding.  Get a JVM that supports UTF-8", e);
		}
	}

	public void write(Object obj) throws IOException {
		/*
		 * Apple: remove traceReferences, traceReferences meant to find out the
		 * objects that are referenced later, this logic is included into
		 * writeImpl method. all objects are ided, when writing an object,
		 * checking if the id has been recorded before in _objVisited.
		 * this change makes writing a big object much much faster!
		 */
		_objVisited.clear();
		writeImpl(obj, true);
		flush();
		_objVisited.clear();
		_firstTimeVisit.clear();
	}

	private boolean writeOptionalReference(Object obj) throws IOException {

		if (!this._firstTimeVisit.containsKey(obj)) {
			this._firstTimeVisit.put(obj, true);
			return false;
		}

		final Writer out = _out;
		if (_objVisited.containsKey(obj)) { // Only write (define) an object
											// once in the JSON stream,
											// otherwise emit a @ref
			String id = getId(obj);
			if (id == null) { // Test for null because of Weak/Soft references
								// being gc'd during serialization.
				return false;
			}
			out.write("{\"" + JsonConstant.JSON_REF + "\":");
			out.write(id);
			out.write('}');
			return true;
		}

		// Mark the object as visited by putting it in the Map (this map is
		// re-used / clear()'d after walk()).
		//		_objVisited.put(obj, null);
		return false;
	}

	private void writeImpl(Object obj, boolean showType) throws IOException {
		// Apple: only written to a certain level, do not go further
		if (NDC.getDepth() >= ioConfig.getGoNoFurtherLevel()) {
			_out.write("null");
			return;
		}

		if (obj == null) {
			_out.write(JsonConstant.NULL_OBJECT);
			return;
		}

		if (isSkipWrite(obj)) {
			_out.write(JsonConstant.NULL_OBJECT);
			return;
		}

		NDC.push("");

		if (obj.getClass().isArray()) {
			writeArray(obj, showType);
		} else if (obj instanceof Collection) {
			writeCollection((Collection) obj, showType);
		} else if (obj instanceof JsonObject) { // symmetric support for writing
												// Map of Maps representation
												// back as identical JSON
												// format.
			JsonObject jObj = (JsonObject) obj;
			if (jObj.isArray()) {
				writeJsonObjectArray(jObj, showType);
			} else if (jObj.isCollection()) {
				writeJsonObjectCollection(jObj, showType);
			} else if (jObj.isMap()) {
				writeJsonObjectMap(jObj, showType);
			} else {
				writeJsonObjectObject(jObj, showType);
			}
		} else if (obj instanceof Map) {
			writeMap((Map) obj, showType);
		} else {
			writeObject(obj, showType);
		}

		NDC.pop();

	}

	private void writeId(String id) throws IOException {
		_out.write("\"" + JsonConstant.JSON_ID + "\":");
		_out.write(id == null ? "0" : id);
	}

	private static void writeType(Object obj, Writer out) throws IOException {
		out.write("\"" + JsonConstant.JSON_TYPE + "\":\"");
		// Class c = obj.getClass();
		//
		// if (Boolean.class.equals(c)) {
		// out.write("boolean");
		// } else if (Byte.class.equals(c)) {
		// out.write("byte");
		// } else if (Short.class.equals(c)) {
		// out.write("short");
		// } else if (Integer.class.equals(c)) {
		// out.write("int");
		// } else if (Long.class.equals(c)) {
		// out.write("long");
		// } else if (Double.class.equals(c)) {
		// out.write("double");
		// } else if (Float.class.equals(c)) {
		// out.write("float");
		// } else if (Character.class.equals(c)) {
		// out.write("char");
		// } else if (Date.class.equals(c)) {
		// out.write("date");
		// } else if (Class.class.equals(c)) {
		// out.write("class");
		// } else if (String.class.equals(c)) {
		// out.write("string");
		// } else if (HibernateUtil.isHibernateCollectionType(obj)) { // Apple
		// out.write(HibernateUtil.getHibernateCollectionType(obj));
		// } else {
		// out.write(c.getName());
		// }
		out.write(getType(obj));

		out.write('"');
	}

	private static String getType(Object obj) {

		Class c = obj.getClass();

		if (Boolean.class.equals(c)) {
			return "boolean";
		} else if (Byte.class.equals(c)) {
			return "byte";
		} else if (Short.class.equals(c)) {
			return "short";
		} else if (Integer.class.equals(c)) {
			return "int";
		} else if (Long.class.equals(c)) {
			return "long";
		} else if (Double.class.equals(c)) {
			return "double";
		} else if (Float.class.equals(c)) {
			return "float";
		} else if (Character.class.equals(c)) {
			return "char";
		} else if (Date.class.equals(c)) {
			return "date";
		} else if (Class.class.equals(c)) {
			return "class";
		} else if (String.class.equals(c)) {
			return "string";
		} else if (HibernateUtil.isHibernateCollectionType(obj)) { // Apple
			return HibernateUtil.getHibernateCollectionType(obj);
		} else {
			return c.getName();
		}

	}

	private void writePrimitive(Object obj) throws IOException {
		if (obj instanceof Character) {
			writeJsonUtf8String(String.valueOf(obj), _out);
		} else {
			PrimitiveWriter writer = new PrimitiveWriter();
			writer.write(obj, false, _out);
			//			_out.write(obj.toString());
		}
	}

	private void writeArray(Object array, boolean showType) throws IOException {
		if (writeOptionalReference(array)) {
			return;
		}

		Class arrayType = array.getClass();
		if (isSkipWrite(arrayType)) {
			return;
		}

		int len = Array.getLength(array);

		if (!this._objVisited.containsKey(array) && len > 0) { //&& !TypeUtil.isPrimitiveArray(arrayType)
			this._objVisited.put(array, this._identity++);
		}

		boolean referenced = this._objVisited.containsKey(array);
		boolean typeWritten = showType;// && !Object[].class.equals(arrayType);

		final Writer out = _out; // performance opt: place in final local for
									// quicker access
		if (typeWritten || referenced || logSystemHashcode) {
			out.write('{');
		}

		boolean first = true;

		if (referenced) {
			writeId(getId(array));
			first = false;
		}

		if (typeWritten) {
			if (!first) {
				out.write(',');
			}
			writeType(array, out);
			first = false;
		}

		first = writeSytemHashcoe(out, array, first);
		if (!first) {
			out.write(',');
		}
		if (len == 0) {
			if (typeWritten || referenced || logSystemHashcode) {
				out.write("\"" + JsonConstant.JSON_ITEMS + "\":[]}");
			} else {
				out.write("[]");
			}
			return;
		}

		if (typeWritten || referenced || logSystemHashcode) {
			out.write("\"" + JsonConstant.JSON_ITEMS + "\":[");
		} else {
			out.write('[');
		}

		// Apple: limit array size
		int threshold = ioConfig.getCollectionThreshhold(arrayType.getName());
		if (logger.isDebugEnabled()) {
			if (len - 1 > threshold) {
				logger.debug("limiting array size to " + threshold + " , the total size is " + len);
			}
		}
		final int lenMinus1 = len - 1 > threshold ? threshold : len - 1;

		// Intentionally processing each primitive array type in separate
		// custom loop for speed. All of them could be handled using
		// reflective Array.get() but it is slower. I chose speed over code
		// length.
		if (byte[].class.equals(arrayType)) {
			writeByteArray((byte[]) array, lenMinus1);
		} else if (char[].class.equals(arrayType)) {
			writeJsonUtf8String(new String((char[]) array), out);
		} else if (short[].class.equals(arrayType)) {
			writeShortArray((short[]) array, lenMinus1);
		} else if (int[].class.equals(arrayType)) {
			writeIntArray((int[]) array, lenMinus1);
		} else if (long[].class.equals(arrayType)) {
			writeLongArray((long[]) array, lenMinus1);
		} else if (float[].class.equals(arrayType)) {
			writeFloatArray((float[]) array, lenMinus1);
		} else if (double[].class.equals(arrayType)) {
			writeDoubleArray((double[]) array, lenMinus1);
		} else if (boolean[].class.equals(arrayType)) {
			writeBooleanArray((boolean[]) array, lenMinus1);
		} else {
			final Class componentClass = array.getClass().getComponentType();
			final boolean isPrimitiveArray = JsonReader.isPrimitive(componentClass);
			final boolean isObjectArray = Object[].class.equals(arrayType);

			// Apple: limit array size
			threshold = ioConfig.getCollectionThreshhold(componentClass.getName());
			if (logger.isDebugEnabled()) {
				if (len - 1 > threshold) {
					logger.debug("limiting array size to " + threshold + " , the total size is " + len);
				}
			}
			len = len > threshold ? threshold : len;

			for (int i = 0; i < len; i++) {
				final Object value = Array.get(array, i);

				// Apple: if an element is skipped, written null, otherwise, it
				// will be consecutive commas
				if (value == null || isSkipWrite(value)) {
					out.write("null");
				} else if (isPrimitiveArray || value instanceof Boolean || value instanceof Long
						|| value instanceof Double) {
					//we want float type explitely written, otherwise float will be read back as double
					writePrimitive(value);
				} else if (value instanceof String) { // Have to specially treat
														// String because it
														// could be referenced,
														// but we still want
														// inline (no @type,
														// value:)
					writeJsonUtf8String((String) value, out);
				} else if (writeArrayElementIfMatching(componentClass, value, false, out)) {
				} else if (isObjectArray) {
					if (writeIfMatching(value, true, out)) {
					} else {
						writeImpl(value, true);
					}
				} else { // Specific Class-type arrays - only force type when
							// the instance is derived from array base class.
					boolean forceType = !value.getClass().equals(componentClass);
					writeImpl(value, forceType);
				}

				if (i != len - 1) {
					out.write(',');
				}
			}
		}

		out.write(']');
		if (typeWritten || referenced || logSystemHashcode) {
			out.write('}');
		}
	}

	private void writeBooleanArray(boolean[] booleans, int lenMinus1) throws IOException {
		final Writer out = _out;
		for (int i = 0; i < lenMinus1; i++) {
			out.write(booleans[i] ? "true," : "false,");
		}
		out.write(Boolean.toString(booleans[lenMinus1]));
	}

	private void writeDoubleArray(double[] doubles, int lenMinus1) throws IOException {
		final Writer out = _out;
		for (int i = 0; i < lenMinus1; i++) {
			out.write(Double.toString(doubles[i]));
			out.write(',');
		}
		out.write(Double.toString(doubles[lenMinus1]));
	}

	private void writeFloatArray(float[] floats, int lenMinus1) throws IOException {
		final Writer out = _out;
		for (int i = 0; i < lenMinus1; i++) {
			out.write(Double.toString(floats[i]));
			out.write(',');
		}
		out.write(Float.toString(floats[lenMinus1]));
	}

	private void writeLongArray(long[] longs, int lenMinus1) throws IOException {
		final Writer out = _out;
		for (int i = 0; i < lenMinus1; i++) {
			out.write(Long.toString(longs[i]));
			out.write(',');
		}
		out.write(Long.toString(longs[lenMinus1]));
	}

	private void writeIntArray(int[] ints, int lenMinus1) throws IOException {
		final Writer out = _out;
		for (int i = 0; i < lenMinus1; i++) {
			out.write(Integer.toString(ints[i]));
			out.write(',');
		}
		out.write(Integer.toString(ints[lenMinus1]));
	}

	private void writeShortArray(short[] shorts, int lenMinus1) throws IOException {
		final Writer out = _out;
		for (int i = 0; i < lenMinus1; i++) {
			out.write(Integer.toString(shorts[i]));
			out.write(',');
		}
		out.write(Integer.toString(shorts[lenMinus1]));
	}

	private void writeByteArray(byte[] bytes, int lenMinus1) throws IOException {
		final Writer out = _out;
		final Object[] byteStrs = _byteStrings;
		for (int i = 0; i < lenMinus1; i++) {
			out.write((char[]) byteStrs[bytes[i] + 128]);
			out.write(',');
		}
		out.write((char[]) byteStrs[bytes[lenMinus1] + 128]);
	}

	private void writeCollection(Collection col, boolean showType) throws IOException {
		if (writeOptionalReference(col)) {
			return;
		}

		if (isSkipWrite(col)) {
			return;
		}

		final Writer out = _out;

		/**
		 * Apple: if isHibernateUnintializedCollection, any action on the
		 * collection will trigger access to the database
		 */
		boolean isEmpty = HibernateUtil.isHibernateUnintializedCollection(col);
		if (!isEmpty) {
			isEmpty = col.isEmpty();
		}

		if (!this._objVisited.containsKey(col) && !isEmpty) {
			this._objVisited.put(col, this._identity++);
		}

		/*Apple:get referencibility from _objVisisted*/
		//		boolean referenced = _objsReferenced.containsKey(col);
		boolean referenced = this._objVisited.containsKey(col);

		if (referenced || showType || isEmpty || logSystemHashcode) {
			out.write('{');
		}

		boolean first = true;
		if (referenced) {
			writeId(getId(col));
			first = false;
		}

		if (showType) {
			if (!first) {
				out.write(',');
			}
			writeType(col, out);
			first = false;
		}

		first = writeSytemHashcoe(out, col, first);

		if (isEmpty) {
			out.write('}');
			return;
		}

		if (!first) { //showType || referenced
			out.write(",\"" + JsonConstant.JSON_ITEMS + "\":[");
		} else {
			out.write('[');
		}

		// Apple: limit collection size
		int threshold = ioConfig.getCollectionThreshhold(getCollectionElementType(col));
		int colLen = col.size();
		if (logger.isDebugEnabled()) {
			if (colLen > threshold) {
				logger.debug("limiting collection size to " + threshold + " , the total size is " + col.size());
			}
		}
		threshold = colLen > threshold ? threshold : colLen;

		Iterator iter = col.iterator();
		int t = 0;
		while (t < threshold) {
			Object eleValue = iter.next();
			// Apple: skip writting?
			if (eleValue == null || isSkipWrite(eleValue)) {
				out.write("null");
			} else {
				writeCollectionElement(eleValue);
			}
			if (iter.hasNext()) {
				out.write(',');
			}

			t++;

		}

		out.write(']');
		if (showType || referenced || logSystemHashcode) { // Finished object, as it was output as an
			// object if @id or @type was output
			out.write("}");
		}
	}

	/**
	 * CM get the element type of a collection, assume the collection contains
	 * the same type of stuff
	 */
	private String getCollectionElementType(Collection col) {
		Iterator it = col.iterator();
		while (it.hasNext()) {
			Object obj = it.next();
			if (obj == null) {
				continue;
			}
			return getType(obj);
		}
		return "";
	}

	private void writeJsonObjectArray(JsonObject jObj, boolean showType) throws IOException {
		if (writeOptionalReference(jObj)) {
			return;
		}

		int len = jObj.getLength();
		String type = jObj.type;
		Class arrayClass;

		if (type == null || "[Ljava.lang.Object;".equals(type)) {
			arrayClass = Object[].class;
		} else {
			arrayClass = JsonReader.classForName2(type);
		}

		final Writer out = _out;
		final boolean isObjectArray = Object[].class.equals(arrayClass);
		final Class componentClass = arrayClass.getComponentType();
		/*Apple:get referencibility from _objVisisted*/
		//		boolean referenced = _objsReferenced.containsKey(jObj) && jObj.hasId();
		boolean referenced = this._objVisited.containsKey(jObj);

		boolean typeWritten = showType && !isObjectArray;

		if (typeWritten || referenced) {
			out.write('{');
		}

		if (referenced) {
			writeId(Long.toString(jObj.id));
			out.write(',');
		}

		if (typeWritten) {
			out.write("\"" + JsonConstant.JSON_TYPE + "\":\"");
			out.write(arrayClass.getName());
			out.write("\",");
		}

		if (len == 0) {
			if (typeWritten || referenced) {
				out.write("\"" + JsonConstant.JSON_ITEMS + "\":[]}");
			} else {
				out.write("[]");
			}
			return;
		}

		if (typeWritten || referenced) {
			out.write("\"" + JsonConstant.JSON_ITEMS + "\":[");
		} else {
			out.write('[');
		}

		Object[] items = (Object[]) jObj.get(JsonConstant.JSON_ITEMS);
		final int lenMinus1 = len - 1;

		for (int i = 0; i < len; i++) {
			final Object value = items[i];

			if (value == null) {
				out.write("null");
			} else if (Character.class.equals(componentClass) || char.class.equals(componentClass)) {
				writeJsonUtf8String((String) value, out);
			} else if (value instanceof Boolean || value instanceof Long || value instanceof Double) {
				writePrimitive(value);
			} else if (value instanceof String) { // Have to specially treat
													// String because it could
													// be referenced, but we
													// still want inline (no
													// @type, value:)
				writeJsonUtf8String((String) value, out);
			} else if (isObjectArray) {
				if (writeIfMatching(value, true, out)) {
				} else {
					writeImpl(value, true);
				}
			} else if (writeArrayElementIfMatching(componentClass, value, false, out)) {
			} else { // Specific Class-type arrays - only force type when
						// the instance is derived from array base class.
				boolean forceType = !value.getClass().equals(componentClass);
				writeImpl(value, forceType);
			}

			if (i != lenMinus1) {
				out.write(',');
			}
		}

		out.write(']');
		if (typeWritten || referenced) {
			out.write('}');
		}
	}

	private void writeJsonObjectCollection(JsonObject jObj, boolean showType) throws IOException {
		if (writeOptionalReference(jObj)) {
			return;
		}

		String type = jObj.type;
		Class colClass = JsonReader.classForName2(type);
		/*Apple:get referencibility from _objVisisted*/
		//		boolean referenced = _objsReferenced.containsKey(jObj) && jObj.hasId();	
		boolean referenced = this._objVisited.containsKey(jObj);
		final Writer out = _out;
		int len = jObj.getLength();

		if (referenced || showType || len == 0) {
			out.write('{');
		}

		if (referenced) {
			writeId(String.valueOf(jObj.id));
		}

		if (showType) {
			if (referenced) {
				out.write(',');
			}
			out.write("\"" + JsonConstant.JSON_TYPE + "\":\"");
			out.write(colClass.getName());
			out.write('"');
		}

		if (len == 0) {
			out.write('}');
			return;
		}

		if (showType || referenced) {
			out.write(",\"" + JsonConstant.JSON_ITEMS + "\":[");
		} else {
			out.write('[');
		}

		Object[] items = (Object[]) jObj.get(JsonConstant.JSON_ITEMS);
		final int itemsLen = items.length;
		final int itemsLenMinus1 = itemsLen - 1;

		for (int i = 0; i < itemsLen; i++) {
			writeCollectionElement(items[i]);

			if (i != itemsLenMinus1) {
				out.write(',');
			}

		}
		out.write("]");
		if (showType || referenced) {
			out.write('}');
		}
	}

	private void writeJsonObjectMap(JsonObject jObj, boolean showType) throws IOException {
		if (writeOptionalReference(jObj)) {
			return;
		}

		/*Apple:get referencibility from _objVisisted*/
		//		boolean referenced = _objsReferenced.containsKey(jObj) && jObj.hasId();
		boolean referenced = this._objVisited.containsKey(jObj);

		final Writer out = _out;

		out.write('{');
		if (referenced) {
			writeId(String.valueOf(jObj.id));
		}

		if (showType) {
			if (referenced) {
				out.write(',');
			}
			String type = jObj.type;
			if (type != null) {
				Class mapClass = JsonReader.classForName2(type);
				out.write("\"" + JsonConstant.JSON_TYPE + "\":\"");
				out.write(mapClass.getName());
				out.write('"');
			} else { // type not displayed
				showType = false;
			}
		}

		if (jObj.isEmpty()) { // Empty
			out.write('}');
			return;
		}

		if (showType) {
			out.write(',');
		}

		out.write("\"" + JsonConstant.JSON_KEYS + "\":[");
		Iterator i = jObj.keySet().iterator();

		while (i.hasNext()) {
			writeCollectionElement(i.next());

			if (i.hasNext()) {
				out.write(',');
			}
		}

		out.write("],\"" + JsonConstant.JSON_ITEMS + "\":[");
		i = jObj.values().iterator();

		while (i.hasNext()) {
			writeCollectionElement(i.next());

			if (i.hasNext()) {
				out.write(',');
			}
		}

		out.write("]}");
	}

	private void writeJsonObjectObject(JsonObject jObj, boolean showType) throws IOException {
		if (writeOptionalReference(jObj)) {
			return;
		}

		final Writer out = _out;
		/*Apple:get referencibility from _objVisisted*/
		//		boolean referenced = _objsReferenced.containsKey(jObj) && jObj.hasId();
		boolean referenced = this._objVisited.containsKey(jObj);

		showType = showType && jObj.type != null;
		Class type = null;

		out.write('{');
		if (referenced) {
			writeId(String.valueOf(jObj.id));
		}

		if (showType) {
			if (referenced) {
				out.write(',');
			}
			out.write("\"" + JsonConstant.JSON_TYPE + "\":\"");
			out.write(jObj.type);
			out.write('"');
			try {
				type = JsonReader.classForName2(jObj.type);
			} catch (Exception ignored) {
				type = null;
			}
		}

		if (jObj.isEmpty()) {
			out.write('}');
			return;
		}

		if (showType || referenced) {
			out.write(',');
		}

		Iterator<Map.Entry<String, Object>> i = jObj.entrySet().iterator();

		while (i.hasNext()) {
			Map.Entry<String, Object> entry = i.next();
			final String fieldName = entry.getKey();
			out.write('"');
			out.write(fieldName);
			out.write("\":");
			Object value = entry.getValue();

			if (value == null) {
				out.write("null");
			} else if (value instanceof BigDecimal || value instanceof BigInteger) {
				writeImpl(value, !doesValueTypeMatchFieldType(type, fieldName, value));
			} else if (value instanceof Number || value instanceof Boolean) {
				out.write(value.toString());
			} else if (value instanceof String) {
				writeJsonUtf8String((String) value, out);
			} else if (value instanceof Character) {
				writeJsonUtf8String(String.valueOf(value), out);
			} else {
				writeImpl(value, !doesValueTypeMatchFieldType(type, fieldName, value));
			}
			if (i.hasNext()) {
				out.write(',');
			}
		}
		out.write('}');
	}

	private boolean doesValueTypeMatchFieldType(Class type, String fieldName, Object value) {
		if (type != null) {
			ClassMeta meta = getDeepDeclaredFields(type);
			Field field = meta.get(fieldName);
			return value.getClass().equals(field.getType());
		}
		return false;
	}

	private void writeMap(Map map, boolean showType) throws IOException {
		if (writeOptionalReference(map)) {
			return;
		}

		if (isSkipWrite(map)) {
			return;
		}

		final Writer out = _out;

		/**
		 * if isHibernateUnintializedCollection, any action on the
		 * collection will trigger access to the database
		 */
		boolean isEmpty = HibernateUtil.isHibernateUnintializedCollection(map);
		if (!isEmpty) {
			isEmpty = map.isEmpty();
		}

		if (!this._objVisited.containsKey(map) && !isEmpty) {
			this._objVisited.put(map, this._identity++);
		}

		boolean referenced = this._objVisited.containsKey(map);

		out.write('{');
		boolean first = true;
		if (referenced) {
			writeId(getId(map));
			first = false;
		}

		if (showType) {
			if (!first) {
				out.write(',');
			}
			writeType(map, out);
			first = false;
		}

		first = writeSytemHashcoe(out, map, first);

		if (isEmpty) {
			out.write('}');
			return;
		}

		if (!first) { //showType || referenced
			out.write(',');
		}

		out.write("\"" + JsonConstant.JSON_KEYS + "\":[");

		// Apple: limit collection size
		int threshold = ioConfig.getCollectionThreshhold(map.getClass().getName());
		int colLen = map.size();
		if (logger.isDebugEnabled()) {
			if (colLen > threshold) {
				logger.debug("limiting collection size to " + threshold + " , the total size is " + map.size());
			}
		}
		threshold = colLen > threshold ? threshold : colLen;

		Iterator i = map.keySet().iterator();
		int index = 0;
		while (i.hasNext()) {
			writeCollectionElement(i.next());

			index++;
			if (index == threshold) {
				break;
			}

			if (i.hasNext()) {
				out.write(',');
			}
		}

		out.write("],\"" + JsonConstant.JSON_ITEMS + "\":[");
		i = map.values().iterator();

		index = 0;
		while (i.hasNext()) {
			writeCollectionElement(i.next());

			index++;
			if (index == threshold) {
				break;
			}

			if (i.hasNext()) {
				out.write(',');
			}
		}

		out.write("]}");
	}

	/**
	 * Write an element that is contained in some type of collection or Map.
	 * 
	 * @param o
	 *            Collection element to output in JSON format.
	 * @throws IOException
	 *             if an error occurs writing to the output stream.
	 */
	private void writeCollectionElement(Object o) throws IOException {
		if (o == null) {
			_out.write("null");
		} else if (o instanceof Boolean || o instanceof Long || o instanceof Double) {
			_out.write(o.toString());
		} else if (o instanceof String) {
			writeJsonUtf8String((String) o, _out);
		} else {
			writeImpl(o, true);
		}
	}

	/**
	 * @param obj
	 *            Object to be written in JSON format
	 * @param showType
	 *            boolean true means show the "@type" field, false eliminates
	 *            it. Many times the type can be dropped because it can be
	 *            inferred from the field or array type.
	 * @throws IOException
	 *             if an error occurs writing to the output stream.
	 */
	private void writeObject(Object obj, boolean showType) throws IOException {

		if (isSkipWrite(obj)) {
			return;
		}

		if (writeIfMatching(obj, showType, _out)) {
			return;
		}

		if (writeOptionalReference(obj)) {
			return;
		}

		if (!this._objVisited.containsKey(obj) && obj != null && !TypeUtil.isPrimitiveObj(obj)) {
			this._objVisited.put(obj, this._identity++);
		}

		final Writer out = _out;

		out.write('{');
		/*Apple:get referencibility from _objVisisted*/
		//		boolean referenced = _objsReferenced.containsKey(obj);
		boolean referenced = this._objVisited.containsKey(obj);
		if (referenced) {
			writeId(getId(obj));
		}

		ClassMeta classInfo = getDeepDeclaredFields(obj.getClass());

		if (referenced && showType) {
			out.write(',');
		}

		if (showType) {
			writeType(obj, out);
		}

		boolean first = !showType;
		if (referenced && !showType) {
			first = false;
		}

		first = writeSytemHashcoe(out, obj, first);

		for (Field field : classInfo.values()) {
			// Apple: skip transient fields
			if ((field.getModifiers() & Modifier.TRANSIENT) != 0) {
				continue;
			}

			// CM 515: skip writting?
			if (field.getName().equals(THIS$0)) {
				continue;
			}

			if (!ioConfig.acceptWrittingType(field.getType().getName())) {
				logger.debug("ignore type " + field.getType().getName());
				continue;
			}

			Object o;
			try {
				o = field.get(obj);
			} catch (Exception ignored) {
				o = null;
			}

			/**
			 * Apple: skip fields completely if their values are Hibernate lazy
			 * objects or not configured to be written
			 */
			if (o != null) {
				if (isSkipWrite(o)) {
					continue;
				}
			}

			if (first) {
				first = false;
			} else {
				out.write(',');
			}

			writeJsonUtf8String(field.getName(), out);
			out.write(':');

			if (o == null) { // don't quote null
				out.write("null");
				continue;
			}

			Class type = field.getType();
			// If types are not exactly the same, write "@type" field
			boolean forceType = logFieldType;
			if (!forceType) {
				forceType = o.getClass() != type;
			}

			if (JsonReader.isPrimitive(type)) {
				writePrimitive(o);
			} else if (writeIfMatching(o, forceType, out)) {
			} else {
				writeImpl(o, forceType);
			}
		}

		out.write('}');
	}

	private boolean writeSytemHashcoe(Writer out, Object obj, boolean first) throws IOException {
		if (!logSystemHashcode) {
			return first;
		}

		if (first) {
			first = false;
		} else {
			out.write(',');
		}

		out.write("\"" + JsonConstant.JSON_HAHSCODE + "\":");
		out.write("\"" + getSystemHashCode(obj) + "\"");
		return first;
	}

	private static String getSystemHashCode(Object obj) {
		return Integer.toHexString(System.identityHashCode(obj));
	}

	/**
	 * Apple: whethere to skip writing an object
	 * 
	 * @param obj
	 * @return
	 */
	private boolean isSkipWrite(Object obj) {
		return isSkipWrite(obj.getClass().getName());
	}

	private boolean isSkipWrite(String cls) {
		/**
		 * Apple:do not attempt to get fields of a hibernate unitialized object,
		 * because that will trigger access to database and get values for
		 * unitialized fields, but we do not want to get field values other than
		 * what can be seen at this point newHibernateWrappedObject checks if
		 * obj is a Hibernate uninitialized Object, if so, it news an instance
		 * to replace the Hibernate proxy object
		 */
		if (HibernateUtil.isHibernateLazyEnhanceByCGLIBObject(cls)) {
			return true;
		}

		if (!ioConfig.acceptWrittingType(cls)) {
			logger.debug("ignore type " + cls);
			return true;
		}

		return false;
	}

	/**
	 * Write out special characters "\b, \f, \t, \n, \r", as such, backslash as
	 * \\ quote as \" and values less than an ASCII space (20hex) as "\\u00xx"
	 * format, characters in the range of ASCII space to a '~' as ASCII, and
	 * anything higher in UTF-8.
	 * 
	 * @param s
	 *            String to be written in utf8 format on the output stream.
	 * @throws IOException
	 *             if an error occurs writing to the output stream.
	 */
	public static void writeJsonUtf8String(String s, Writer out) throws IOException {
		out.write('\"');
		int len = s.length();

		for (int i = 0; i < len; i++) {
			char c = s.charAt(i);

			writeJsonUtf8Char(c, out);
		}
		out.write('\"');
	}

	private static void writeJsonUtf8Char(char c, Writer out) throws IOException {
		if (c < ' ') { // Anything less than ASCII space, write either in \\u00xx form, or the special \t, \n, etc. form


			if (c == '\b') {
				out.write("\\b");
			} else if (c == '\t') {
				out.write("\\t");
			} else if (c == '\n') {
				out.write("\\n");
			} else if (c == '\f') {
				out.write("\\f");
			} else if (c == '\r') {
				out.write("\\r");
			} else {
				String hex = Integer.toHexString(c);
				out.write("\\u");
				int pad = 4 - hex.length();
				for (int k = 0; k < pad; k++) {
					out.write('0');
				}
				out.write(hex);
			}
		} else if (c == '\\' || c == '"') {
			out.write('\\');
			out.write(c);
		} else { // Anything else - write in UTF-8 form (multi-byte encoded)
					// (OutputStreamWriter is UTF-8)
			out.write(c);
		}
	}

	/**
	 * @param c
	 *            Class instance
	 * @return ClassMeta which contains fields of class. The results are cached
	 *         internally for performance when called again with same Class.
	 */
	static ClassMeta getDeepDeclaredFields(Class c) {
		ClassMeta classInfo = _classMetaCache.get(c.getName());
		if (classInfo != null) {
			return classInfo;
		}

		classInfo = new ClassMeta();
		Class curr = c;

		while (curr != null) {
			try {
				Field[] local = curr.getDeclaredFields();

				for (Field field : local) {
					if ((field.getModifiers() & Modifier.STATIC) == 0) { // speed
																			// up:
																			// do
																			// not
																			// process
																			// static
																			// fields.
						if (!field.isAccessible()) {
							try {
								field.setAccessible(true);
							} catch (Exception ignored) {
							}
						}
						classInfo.put(field.getName(), field);
					}
				}
			} catch (ThreadDeath t) {
				throw t;
			} catch (Throwable ignored) {
			}

			curr = curr.getSuperclass();
		}

		_classMetaCache.put(c.getName(), classInfo);
		return classInfo;
	}

	public void flush() {
		try {
			if (_out != null) {
				_out.flush();
			}
		} catch (Exception ignored) {
		}
	}

	public void close() {
		try {
			_out.close();
		} catch (Exception ignore) {
		}
	}

	private String getId(Object o) {
		if (o instanceof JsonObject) {
			long id = ((JsonObject) o).id;
			if (id != -1) {
				return String.valueOf(id);
			}
		}
		//		Long id = _objsReferenced.get(o);
		/*Apple:get referencibility from _objVisisted*/
		Long id = _objVisited.get(o);
		return id == null ? null : Long.toString(id);
	}
}
