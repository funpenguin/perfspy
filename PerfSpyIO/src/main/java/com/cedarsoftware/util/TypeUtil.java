package com.cedarsoftware.util;

import java.util.HashSet;
import java.util.Set;

public class TypeUtil {
	private static final Set<Class> _primWrappers = new HashSet<Class>();
	private static final Set<String> _primOrWrapperNames = new HashSet<String>();

	static {
		_primWrappers.add(Byte.class);
		_primWrappers.add(Integer.class);
		_primWrappers.add(Long.class);
		_primWrappers.add(Double.class);
		_primWrappers.add(Character.class);
		_primWrappers.add(Float.class);
		_primWrappers.add(Boolean.class);
		_primWrappers.add(Short.class);

		_primOrWrapperNames.add("byte");
		_primOrWrapperNames.add("int");
		_primOrWrapperNames.add("long");
		_primOrWrapperNames.add("double");
		_primOrWrapperNames.add("char");
		_primOrWrapperNames.add("float");
		_primOrWrapperNames.add("boolean");
		_primOrWrapperNames.add("short");

		_primOrWrapperNames.add(Byte.class.getName());
		_primOrWrapperNames.add(Integer.class.getName());
		_primOrWrapperNames.add(Long.class.getName());
		_primOrWrapperNames.add(Double.class.getName());
		_primOrWrapperNames.add(Character.class.getName());
		_primOrWrapperNames.add(Float.class.getName());
		_primOrWrapperNames.add(Boolean.class.getName());
		_primOrWrapperNames.add(Short.class.getName());

	}

	public static boolean isPrimitiveOrWrapper(Class c) {
		return c.isPrimitive() || _primWrappers.contains(c);
	}

	public static boolean isPrimitiveOrWrapper(String type) {
		return _primOrWrapperNames.contains(type);
	}

	public static boolean isPrimitiveObj(Object obj) {
		return obj.getClass().isPrimitive();
	}

	public static boolean isPrimitiveOrWrapperObj(Object obj) {
		return isPrimitiveOrWrapper(obj.getClass());
	}

	public static boolean isPrimitiveArray(Class arrayType) {
		if (byte[].class.equals(arrayType)) {
			return true;
		}
		if (char[].class.equals(arrayType)) {
			return true;
		}
		if (short[].class.equals(arrayType)) {
			return true;
		}
		if (int[].class.equals(arrayType)) {
			return true;
		}
		if (long[].class.equals(arrayType)) {
			return true;
		}
		if (float[].class.equals(arrayType)) {
			return true;
		}
		if (double[].class.equals(arrayType)) {
			return true;
		}
		if (boolean[].class.equals(arrayType)) {
			return true;
		}
		return false;
	}

	public static boolean isBlankString(Object obj) {
		if (obj instanceof String) {
			return org.apache.commons.lang3.StringUtils.isBlank((String) obj);
		}

		return false;
	}
}
