package com.yourcompany.zoo.animal;

public class Foo {
	public static Foo getFoo() {
		return new Foo();
	}

	public static void doingFooStuff() {
		System.out.println("doing foo stuff...");
		doingFooStuff2();

	}

	public static void doingFooStuff2() {
		System.out.println("doing foo stuff2...");

	}

	public void bar1() {
		System.out.println("bar1 calling bar2");
		bar2();
	}

	public void bar2() {
		System.out.println("bar2 calling bar3");
		bar3();
	}

	public void bar3() {
		System.out.println("bar3 roger");
	}

}
