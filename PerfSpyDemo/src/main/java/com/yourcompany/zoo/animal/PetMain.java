package com.yourcompany.zoo.animal;

public class PetMain {
	public void playWithPets() {
		Pet pet = new Pet();
		pet.accompany();

		FurryPet furryPet = new FurryPet();
		furryPet.accompany();

		Dog dog = new Dog();
		//if you follow "programing towards interface, not implementations"
		//you shouldn't do this
		dog.accompany();

		Pet petDog = new Dog();
		//instead you should do this
		petDog.accompany();

		furryPet.cuddle();
		dog.cuddle();

	}

	public static void main(String[] args) {
		new PetMain().playWithPets();
	}
}
