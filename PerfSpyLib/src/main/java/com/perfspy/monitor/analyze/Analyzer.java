package com.perfspy.monitor.analyze;

import java.util.ArrayList;
import java.util.List;

import com.perfspy.monitor.model.OperationFlow;

public class Analyzer {
	private List<PerfSpyAnalyzer> analyzers = new ArrayList<PerfSpyAnalyzer>();

	public void setAnalyzers(List<PerfSpyAnalyzer> analyzers) {
		this.analyzers = analyzers;
	}

	/**
	 * analyze this operation only, do not go down to the next levels
	 */
	public void analyzeThis(OperationFlow ops) {

		for (PerfSpyAnalyzer analyzer : analyzers) {
			analyzer.analyze(ops);
		}
	}

	public void prepareForAnalyze() {
		for (PerfSpyAnalyzer analyzer : analyzers) {
			analyzer.prepare();
		}
	}

	public List<OperationFlow> doneAnalyze() {
		List<OperationFlow> results = new ArrayList<OperationFlow>();

		for (PerfSpyAnalyzer analyzer : analyzers) {
			results.addAll(analyzer.done());
		}
		return results;
	}
}
