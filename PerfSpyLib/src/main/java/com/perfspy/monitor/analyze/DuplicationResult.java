
package com.perfspy.monitor.analyze;

import com.perfspy.monitor.model.AnalyzeResult;

public class DuplicationResult implements AnalyzeResult {
    private int executionTimes;

    private long totalExecutionTime;

    public DuplicationResult(int executionTimes, long totalExecutionTime) {
        this.executionTimes = executionTimes;
        this.totalExecutionTime = totalExecutionTime;
    }

    public String report() {
        return " invoked " + executionTimes + " times, with total execution time " + totalExecutionTime;
    }
}
