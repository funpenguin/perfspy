package com.perfspy.monitor.analyze;

import com.perfspy.monitor.model.AnalyzeResult;
import com.perfspy.monitor.model.OperationFlow;

public class SlowAnalyzer extends PerfSpyAnalyzer {

	private static final String ANALYZER = "SLOW";

	@Override
	protected void analyze(OperationFlow ops) {
		if (ops.getExecutionTime() > PerfSpyAnalyzer.getSlowThreshold()) {
			ops.addAnalyzeResult(ANALYZER,
					new SlowResult(ops.getExecutionTime()));
		}
	}
}

class SlowResult implements AnalyzeResult {
	private long executionTime;

	public SlowResult(long executionTime) {
		this.executionTime = executionTime;
	}

	@Override
	public String report() {
		return " executed slowly at " + executionTime + " seconds,";
	}
}
