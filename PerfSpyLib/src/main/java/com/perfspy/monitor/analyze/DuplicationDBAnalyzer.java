package com.perfspy.monitor.analyze;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.perfspy.monitor.dao.AbstractDAOTask;
import com.perfspy.monitor.dao.DAOTask;
import com.perfspy.monitor.dao.OperationFlowDAO;
import com.perfspy.monitor.model.DuplicationSummary;
import com.perfspy.monitor.model.OperationFlow;
import com.perfspy.monitor.util.EnteredCFlowFlag;

/**
 * same function as DuplicationAnalyzer, but performance the analysis on the DB
 * side, to minimize the memory footprint
 */
public class DuplicationDBAnalyzer extends PerfSpyAnalyzer {
	private static final Log logger = LogFactory.getLog(DuplicationDBAnalyzer.class);

	private Connection con;

	private String spyEvent;
	private OperationFlowDAO dao;

	private static final String UPDATE_DUP_ANALYSIS = "update PerfSpyOps o set O.ANALYZEREPORT=O.ANALYZEREPORT||? "
			+ "where O.SPYEVENT=? and O.longOPSIGNATURE=?";

	public DuplicationDBAnalyzer() {

	}

	@Override
	protected void prepare() {
		this.con = EnteredCFlowFlag.getConnection();
		this.spyEvent = EnteredCFlowFlag.getSpyEvent();
		try {
			this.con.setAutoCommit(false);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		this.dao = EnteredCFlowFlag.getOperationFlowDao();
	}

	@Override
	protected void analyze(OperationFlow op) {
	}

	@Override
	public List<OperationFlow> done() {
		DAOTask task = new AbstractDAOTask(con) {

			PreparedStatement updateStatement = null;

			@Override
			public void doTask() throws Exception {
				long spyEventId = dao.getEventIdByName(spyEvent);
				Collection<DuplicationSummary> dups = dao.getDupOpSummeries(false, spyEventId);
				updateStatement = con.prepareStatement(UPDATE_DUP_ANALYSIS);
				for (DuplicationSummary dupSummary : dups) {
					if (dupSummary.totalExecutionTime > PerfSpyAnalyzer.getSlowThreshold()) {
						updateStatement.setString(1, dupSummary.getSummary());
						updateStatement.setLong(2, spyEventId);
						updateStatement.setString(3, dupSummary.longOpSig);
						updateStatement.addBatch();
					}
				}

				updateStatement.executeBatch();
				con.commit();

				updateStatement.close();
				updateStatement = null;
			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] { updateStatement };
			}

			@Override
			public Object getResult() {
				return new ArrayList<OperationFlow>();
			}

		};

		EnteredCFlowFlag.getDAOExecutor().executeTask(task);

		return new ArrayList<OperationFlow>();
	}
}
