package com.perfspy.monitor.analyze;

import java.util.ArrayList;
import java.util.List;

import com.perfspy.monitor.model.OperationFlow;

public abstract class PerfSpyAnalyzer {
	private static int slowThreshold; // in seconds

	public static int getSlowThreshold() {
		return slowThreshold;
	}

	public static void setSlowThreshold(int slowThreshold) {
		PerfSpyAnalyzer.slowThreshold = slowThreshold;
	}

	// /**
	// * analyze the whole tree recursively
	// */
	// public static void analyzeOperationTree(OperationFlow ops,
	// String cflowName, String spyEvent) {
	// prepareForAnalyze( spyEvent);
	//
	// analyzeTree(ops);
	//
	// doneAnalyze();
	// }

	// public static void prepareForAnalyze() {
	// List<PerfSpyAnalyzer> analyzers = PerfSpyConfig
	// .getAnalyzers(EnteredCFlowFlag.getCflowName());
	// EnteredCFlowFlag.setAnalyzers(analyzers);
	//
	// for (PerfSpyAnalyzer analyzer : analyzers) {
	// analyzer.prepare();
	// }
	// }

	// public static List<OperationFlow> doneAnalyze() {
	// List<OperationFlow> results = new ArrayList<OperationFlow>();
	// List<PerfSpyAnalyzer> analyzers = EnteredCFlowFlag.getAnalyzers();
	//
	// for (PerfSpyAnalyzer analyzer : analyzers) {
	// results.addAll(analyzer.done());
	// }
	// return results;
	// }

	// private static void analyzeTree(OperationFlow ops, String cflowName) {
	// List<OperationFlow> outOps = ops.getOutOps();
	// List<PerfSpyAnalyzer> analyzers = PerfSpyConfig.getAnalyzers(cflowName);
	// for (OperationFlow outOp : outOps) {
	// analyzeTree(outOp, cflowName);
	// for (PerfSpyAnalyzer analyzer : analyzers) {
	// analyzer.analyze(outOp);
	// }
	// }
	// }

	/**
	 * analyze this operation only, do not go down to the next levels
	 */
	protected abstract void analyze(OperationFlow ops);

	protected void prepare() {

	}

	public List<OperationFlow> done() {
		return new ArrayList<OperationFlow>();
	};
}

// class Analyzers extends ThreadLocal<List<PerfSpyAnalyzer>> {
//
// @Override
// protected List<PerfSpyAnalyzer> initialValue() {
// return PerfSpyConfig.getAnalyzers(EnteredCFlowFlag.getCflowName());
// }
// }
