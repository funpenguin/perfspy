package com.perfspy.monitor.analyze;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.perfspy.monitor.model.OperationFlow;

/**
 * if an operation is invoked many many times (with different parameters or same
 * parameters) consider this operation is a potential candidate for improvement.
 * 
 * such scenario often happens when retrieving some information from the
 * database, instead of getting all information at the same time, it gets it bit
 * by bit, for example, the infamous Hiberante N+1 problem
 */
public class DuplicationAnalyzer extends PerfSpyAnalyzer {

	static class DupOp {
		String op;

		long totalTime = 0;

		public DupOp(String op) {
			super();
			this.op = op;
		}

		public void dupOp(OperationFlow op) {
			this.totalTime += op.getExecutionTime();
		}

		@Override
		public boolean equals(Object obj) {
			DupOp that = (DupOp) obj;
			return this.op.equals(that.op);
		}

		@Override
		public int hashCode() {
			return this.op.hashCode();
		}

		@Override
		public String toString() {
			return this.op + " at " + this.totalTime;
		}
	}

	private Map<DupOp, List<Long>> invocations = new HashMap<DupOp, List<Long>>();

	private static final String ANALYZER = "DUP";

	@Override
	protected void prepare() {
		this.invocations.clear();
	}

	@Override
	protected void analyze(OperationFlow op) {
		DupOp dupOp = new DupOp(op.getSignature());
		List<Long> list;
		if (!invocations.containsKey(dupOp)) {
			list = new ArrayList<Long>();
		} else {
			list = invocations.get(dupOp);
			Set<DupOp> keys = invocations.keySet();
			for (DupOp dupOp2 : keys) {
				if (dupOp2.equals(dupOp)) {
					dupOp = dupOp2;
					break;
				}
			}
		}

		list.add(op.getId());
		dupOp.dupOp(op);
		invocations.put(dupOp, list);
	}

	@Override
	public List<OperationFlow> done() {
		Set<DupOp> keys = invocations.keySet();
		List<OperationFlow> result = new ArrayList<OperationFlow>();
		for (DupOp dupOp : keys) {
			List<Long> dupOpIds = invocations.get(dupOp);
			if (dupOpIds.size() == 1) {
				continue;
			}

			long totalExecutionTime = dupOp.totalTime;

			if (totalExecutionTime > PerfSpyAnalyzer.getSlowThreshold()) {
				for (Long dupOpId : dupOpIds) {
					OperationFlow op = new OperationFlow();
					op.setId(dupOpId);
					op.addAnalyzeResult(ANALYZER, new DuplicationResult(
							dupOpIds.size(), totalExecutionTime));
					result.add(op);
				}

			}
		}

		this.invocations.clear();
		return result;
	}
}
