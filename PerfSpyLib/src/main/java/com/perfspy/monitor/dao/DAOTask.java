package com.perfspy.monitor.dao;

import java.sql.Connection;
import java.sql.Statement;

public interface DAOTask {
	public void doTask() throws Exception;

	public Connection getConnection();

	public Statement[] getStatement();

	public Object getResult();

	public boolean lastTask();
}
