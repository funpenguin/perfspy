package com.perfspy.monitor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.perfspy.monitor.analyze.PerfSpyAnalyzer;
import com.perfspy.monitor.config.PerfSpyConfig;
import com.perfspy.monitor.model.DuplicationSummary;
import com.perfspy.monitor.model.FailToLoad;
import com.perfspy.monitor.model.OperationFlow;

/**
 * not thread safe! This class is used to persist operations as they are created
 * from the aspect, it batches the persisting, and hence are not thread safe. It
 * passes the persisting tasks to DAOExecutor, which runs them in a single
 * thread, so it ensures thread safety.
 */
public class OperationFlowDAO {
	private static final String DETAIL_INFO_LOGGED = "More...";

	private static final String INSERT_OP = "insert into PerfSpyOps "
			+ "(ID,  OPSIGNATURE, ANALYZEREPORT, RETURNValue, PARAMS, executiontime,callingop,SPYEVENT,LONGOPSIGNATURE) "
			+ "values (?,?,?,?,?,?,?,?,?)";

	private static final String INSERT_DETAIL_OP = "insert into PerfSpyOpDetails(ID,SPYEVENT,PARAMS,RETURNVALUE)"
			+ "values (?,?,?,?)";

	private static final String GET_DETAIL_OP = "select ID,SPYEVENT,PARAMS,RETURNVALUE from PerfSpyOpDetails where id=? and spyevent=?";

	private static final String GET_COUNT = "select count(id) from  PerfSpyOps o where O.SPYEVENT=?";

	private static final String GET_SUMMARY = " select * from "
			+ "(select LONGOPSIGNATURE, o.id, O.EXECUTIONTIME, "
			+ "count(O.LONGOPSIGNATURE) over (partition by O.LONGOPSIGNATURE) as times, "
			+ "sum(O.EXECUTIONTIME ) over (partition by O.LONGOPSIGNATURE) as total_time "
			+ "from PerfSpyOps o " //
			+ "where O.SPYEVENT= ?) " //
			+ "where times>2  ";

	private static final String GET_FIRST_N_SUMMARY = "select * from ( " + GET_SUMMARY + ") "
			+ "where rownum<500 order by total_time desc ";

	public static final String UPDATE_ANALYZE_RESULT = "update PerfSpyOps set ANALYZEREPORT=? where ID=? and SPYEVENT=?";

	private static final String INSERT_EVENT = "insert into PerfSpyEvent (id, spyevent) values (SEQ_PERFSPYEVENT.nextval,?)";

	private static final String UPDATE_EVENT = "update PerfSpyEvent set spytime=? where id=? ";

	private static final String GET_EVENT = "select id from PerfSpyEvent where spyevent=?";

	private static final String GET_OP_BY_ID = "select o.id, connect_by_isleaf as isleaf, OPSIGNATURE, ANALYZEREPORT, o.executiontime,"
			+ "callingop, RETURNValue, PARAMS,LONGOPSIGNATURE from PerfSpyOps o "
			+ "inner join PerfSpyEvent e on o.spyevent =e.id and e.id=? "
			+ "connect by prior o.ID= O.CALLINGOP start with o.id=?";

	private static final String GET_OP_PARENTS_BY_ID = "select o.id, connect_by_isleaf as isleaf, OPSIGNATURE, ANALYZEREPORT, o.executiontime,"
			+ "callingop, RETURNValue, PARAMS,LONGOPSIGNATURE from PerfSpyOps o "
			+ "inner join PerfSpyEvent e on o.spyevent =e.id and e.id=? "
			+ "connect by  o.ID= prior O.CALLINGOP start with o.id=?";

	private static final String GET_OP_TREE = //
	"  select id, callingop from ( "
			+ "	        select id, callingop, LONGOPSIGNATURE     from "
			+ "	            (select id, callingop, LONGOPSIGNATURE from PerfSpyOps where spyevent= ?) "
			+ "	        connect by  prior ID=   CALLINGOP start with id= ?  )"
			+ "	  where LONGOPSIGNATURE=?";

	private static final String GET_EVENT_OVERVIEW = " select O.OPSIGNATURE , O.ID, "
			+ "O.EXECUTIONTIME, O.ANALYZEREPORT, O.CALLINGOP from perfspyops o " //
			+ " where O.SPYEVENT=? " //
			//+ " and O.EXECUTIONTIME >0" //
			+ " and o.ANALYZEREPORT is not null" //
			+ " order by O.EXECUTIONTIME desc";

	private PreparedStatement insertStatement;
	private PreparedStatement insertDetailStatement;

	private int simpleBatchSize = 2000;
	private int detailBatchSize = 50;

	private long operationNumberLimit = 600000;

	private int insertBatched = 0;
	private int insertDetailBatched = 0;

	private static final Log logger = LogFactory.getLog(OperationFlowDAO.class);

	private Connection con;

	private long spyEventId = -1;

	private DAOExecutor executor;

	public OperationFlowDAO(Connection con) {
		this.con = con;
	}

	public void setExecutor(DAOExecutor executor) {
		this.executor = executor;
	}

	public void setSimpleBatchSize(int batchSize) {
		this.simpleBatchSize = batchSize;
	}

	public void setDetailBatchSize(int detailBatchSize) {
		this.detailBatchSize = detailBatchSize;
	}

	public void setOperationNumberLimit(long operationNumberLimit) {
		this.operationNumberLimit = operationNumberLimit;
	}

	public void startPersisting(final String spyEvent) {
		DAOTask task = new AbstractDAOTask(con) {
			PreparedStatement insertEventStatement;

			@Override
			public Statement[] getStatement() {
				return new Statement[] { insertEventStatement };
			}

			@Override
			public String toString() {
				return "start persisting " + spyEvent + " with id " + spyEventId;
			}

			@Override
			public void doTask() throws Exception {
				insertEventStatement = con.prepareStatement(INSERT_EVENT);
				insertEventStatement.setString(1, spyEvent);
				insertEventStatement.execute();

				con.commit();

				spyEventId = getEventIdByName(spyEvent);
			}
		};

		executor.start();
		executor.executeTask(task);
	}

	public void commitTask(boolean immediate) {
		DAOTask task = new AbstractDAOTask(con) {
			@Override
			public void doTask() throws Exception {
				if (!con.isClosed()) {
					con.commit();
				}
			}

			@Override
			public Statement[] getStatement() {
				return new Statement[0];
			}

			@Override
			public boolean lastTask() {
				return true;
			}
		};

		if (immediate) {
			try {
				DAOTemplate.performDAO(task);
			} catch (Exception e) {
				logger.error("error in forcing commit immediately", e);
			}
		} else {
			executor.executeTask(task);
		}
	}

	public OperationFlow getOperationDetails(final long id, final long spyEventId) throws Exception {
		DAOTask task = new AbstractDAOTask(con) {
			PreparedStatement getOpDetailStatement = null;
			OperationFlow op;

			@Override
			public void doTask() throws Exception {
				getOpDetailStatement = con.prepareStatement(GET_DETAIL_OP);
				getOpDetailStatement.setLong(1, id);
				getOpDetailStatement.setLong(2, spyEventId);

				ResultSet rs = getOpDetailStatement.executeQuery();

				if (rs.next()) {
					op = new OperationFlow();
					op.setId(id);

					String paramStr = rs.getString(3);
					Object[] params = new Object[1];
					try {
						op.setParametersJson(paramStr);
					} catch (Exception e) {
						logger.error("failed to load parameters for " + spyEventId + ":" + id, e);
						params[0] = FailToLoad.instance;
					}

					String retStr = rs.getString(4);
					Object ret = null;
					try {
						op.setReturnValueJson(retStr);

					} catch (Exception e) {
						logger.error("failed to load return value for " + spyEventId + ":" + id, e);
						ret = FailToLoad.instance;
					}
				} else {
					op = null;
				}
			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] { getOpDetailStatement };
			}

			@Override
			public Object getResult() {
				return op;
			}
		};

		return (OperationFlow) DAOTemplate.performDAO(task);

	}

	public long getEventIdByName(final String spyEvent) throws Exception {
		DAOTask task = new AbstractDAOTask(con) {
			PreparedStatement getEventStatement = null;
			long id = -1;

			@Override
			public void doTask() throws Exception {
				getEventStatement = con.prepareStatement(GET_EVENT);
				getEventStatement.setString(1, spyEvent);
				ResultSet rs = getEventStatement.executeQuery();
				if (rs.next()) {
					id = rs.getLong(1);

				}
			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] { getEventStatement };
			}

			@Override
			public Object getResult() {
				return id;
			}
		};

		return (Long) DAOTemplate.performDAO(task);

	}

	public void persistThis(final OperationFlow op, final String[] logDetailParts) {

		DAOTask task = new AbstractDAOTask(con) {
			@Override
			public String toString() {
				return "persisting operation " + op.getId() + " in batches for spyEvent with id "
						+ spyEventId;
			}

			@Override
			public void doTask() throws Exception {
				if (insertStatement == null) {
					insertStatement = con.prepareStatement(INSERT_OP);
				}

				insertStatement.setLong(1, op.getId());
				insertStatement.setString(2, op.getSignature());
				insertStatement.setString(3, op.getAnalyzeReport());
				if (ArrayUtils.contains(logDetailParts, PerfSpyConfig.DETAIL_RETURN)) {
					insertStatement.setString(4,
							"(" + DETAIL_INFO_LOGGED + ")" + op.getReturnValueToString());
				} else {
					insertStatement.setString(4, op.getReturnValueToString());
				}

				if (ArrayUtils.contains(logDetailParts, PerfSpyConfig.DETAIL_PARAM)) {
					insertStatement.setString(5,
							"(" + DETAIL_INFO_LOGGED + ")" + op.getParametersToString());
				} else {
					insertStatement.setString(5, op.getParametersToString());
				}
				insertStatement.setLong(6, op.getExecutionTime());

				insertStatement.setLong(7, op.getCallingOpId());

				insertStatement.setLong(8, spyEventId);
				insertStatement.setString(9, op.getLongSignature());

				if (insertBatched < simpleBatchSize) {
					insertStatement.addBatch();
					insertBatched++;
				} else {
					insertStatement.executeBatch();
					con.commit();

					if (logger.isDebugEnabled()) {
						logger.debug("execute batch of size " + insertBatched);
					}
					insertStatement.addBatch();
					insertBatched = 1;
				}
			}

			@Override
			public Statement[] getStatement() {
				return null;
			}
		};

		executor.executeTask(task);

		final StringBuffer paramJson = new StringBuffer();
		final StringBuffer retJson = new StringBuffer();

		if (logDetailParts.length > 0) {
			//this serialization to json will take a long time and will happen in the same thread as the spied operations. 
			//it shouldn't be serialized in the DAO thread, as we want to capture the parameters and return value as they are at that moment
			//parameters and return value are reference, their contents might change overtime
			if (ArrayUtils.contains(logDetailParts, PerfSpyConfig.DETAIL_PARAM)) {
				paramJson.append(op.parametersToJson());
			}
			if (ArrayUtils.contains(logDetailParts, PerfSpyConfig.DETAIL_RETURN)) {
				retJson.append(op.returnValueToJson());
			}
		}

		if (logDetailParts.length > 0) {
			DAOTask task2 = new AbstractDAOTask(con) {
				@Override
				public String toString() {
					return "persisting operation details " + op.getId()
							+ " in batches for spyEvent with id " + spyEventId;
				}

				@Override
				public void doTask() throws Exception {
					if (insertDetailStatement == null) {
						insertDetailStatement = con.prepareStatement(INSERT_DETAIL_OP);
					}

					insertDetailStatement.setLong(1, op.getId());
					insertDetailStatement.setLong(2, spyEventId);

					/*
					 * TODO: do not use setString, see
					 * http://rocksolutions.wordpress
					 * .com/2010/06/07/handling-clobs
					 * -made-easy-with-oracle-jdbc-10g/ but then within JBoss,
					 * org.jboss.resource.adapter.jdbc.WrappedPreparedStatement
					 * can't be cast to OraclePreparedStatement
					 */
					insertDetailStatement.setString(3, paramJson.toString());
					// ((OraclePreparedStatement) insertDetailStatement)
					// .setStringForClob(3, paramJson);

					// TODO: can't cast to OraclePreparedStatement
					insertDetailStatement.setString(4, retJson.toString());
					// ((OraclePreparedStatement)
					// insertDetailStatement).setStringForClob(4, retJson);

					if (insertDetailBatched < detailBatchSize) {
						insertDetailStatement.addBatch();
						insertDetailBatched++;
					} else {
						insertDetailStatement.executeBatch();
						con.commit();

						if (logger.isDebugEnabled()) {
							logger.debug("execute batch of size " + insertDetailBatched);
						}
						insertDetailStatement.addBatch();
						insertDetailBatched = 1;
					}
				}

				@Override
				public Statement[] getStatement() {
					return null;
				}
			};

			executor.executeTask(task2);
		}

	}

	public void lastOpPersisted() {
		DAOTask task = new AbstractDAOTask(con) {
			@Override
			public String toString() {
				return "persisting last operation for spyEvent with id " + spyEventId;
			}

			@Override
			public void doTask() throws Exception {
				if (insertBatched != 0) {
					insertStatement.executeBatch();

					con.commit();
					if (logger.isDebugEnabled()) {
						logger.debug("execute operation batch at " + insertBatched);
					}
				}

				if (insertDetailBatched != 0) {
					insertDetailStatement.executeBatch();

					con.commit();
					if (logger.isDebugEnabled()) {
						logger.debug("execute detailed operation batch at " + insertDetailBatched);
					}
				}

			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] { insertStatement, insertDetailStatement };
			}
		};

		executor.executeTask(task);

	}

	public void finishedPersisting(final long spyStartTime, final List<OperationFlow> analyzeResults) {
		DAOTask task = new AbstractDAOTask(con) {
			PreparedStatement updateEventStatement = null;

			@Override
			public String toString() {
				return "finished persisting for spyEvent with id " + spyEventId;
			}

			@Override
			public void doTask() throws Exception {
				persistAnalyzeResults(analyzeResults);

				updateEventStatement = con.prepareStatement(UPDATE_EVENT);
				long spytime = (System.currentTimeMillis() - spyStartTime) / 1000;
				updateEventStatement.setLong(1, spytime);
				updateEventStatement.setLong(2, spyEventId);
				updateEventStatement.execute();

				con.commit();
			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] { updateEventStatement };
			}

			@Override
			public boolean lastTask() {
				return true;
			}
		};

		executor.executeTask(task);

		executor.endOfTask();

	}

	private void persistAnalyzeResults(final List<OperationFlow> analyzeResults) throws Exception {

		DAOTask task = new AbstractDAOTask(con) {
			PreparedStatement updateOpStatement = null;

			@Override
			public String toString() {
				return "persisting analyze results for spyEvent with id " + spyEventId;
			}

			@Override
			public void doTask() throws Exception {

				if (analyzeResults == null || analyzeResults.size() == 0) {
					return;
				}

				updateOpStatement = con.prepareStatement(UPDATE_ANALYZE_RESULT);
				for (OperationFlow op : analyzeResults) {
					updateOpStatement.setString(1, op.getAnalyzeReport());
					updateOpStatement.setLong(2, op.getId());
					updateOpStatement.setLong(3, spyEventId);
					updateOpStatement.addBatch();
				}

				updateOpStatement.executeBatch();

				con.commit();

			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] { updateOpStatement };
			}
		};
		DAOTemplate.performDAO(task);

	}

	public OperationFlow getOperationTreeByEventId(final long eventId, final long... operationId)
			throws Exception {
		DAOTask task = new AbstractDAOTask(con) {
			PreparedStatement getStatement = null;

			OperationFlow op;

			@Override
			public Object getResult() {
				return op;
			}

			@Override
			public void doTask() throws Exception {
				long count = getCountOfOperationTree(eventId);
				if (count > operationNumberLimit) {
					op = getOperationSummaryTree(eventId);
					op.setAnalyzeReport("this operation tree contains "
							+ count
							+ " operations, it is impossible to load it into UI, so only show the operations that are invoked most often ");
					return;
				}

				getStatement = con.prepareStatement(GET_OP_BY_ID);
				getStatement.setLong(1, eventId);
				if (operationId.length > 0) {
					getStatement.setLong(2, operationId[0]);
				} else {
					getStatement.setLong(2, 1);
				}
				ResultSet rs = getStatement.executeQuery();

				op = assembleOperationTreeFromResultSet(rs);
				if (logger.isDebugEnabled()) {
					logger.debug("got the operation tree for spy event " + eventId
							+ (operationId.length > 0 ? " and operation " + operationId[0] : ""));
				}

				if (operationId.length > 0 && operationId[0] != 1) {
					logger.debug("continue to find out the parents of operation " + operationId[0]);

					OperationFlow[] parentAndMeOp = getOperationParentsByEventId(eventId,
							operationId[0]);
					if (parentAndMeOp != null) {
						OperationFlow me = parentAndMeOp[1];
						if (me != null) {
							// op=DUMMY, op.getFirstOutOp=me, sticks the two "me"
							// together
							if (op.getFirstOutOp() != null) {
								me.addOutOps(op.getFirstOutOp().getOutOps());
							}
							me.setSelected(true);
							op = parentAndMeOp[0];
						}

					}
				}
			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] { getStatement };
			}
		};

		return (OperationFlow) DAOTemplate.performDAO(task);
	}

	OperationFlow[] getOperationParentsByEventId(final long eventId, final long operationId)
			throws Exception {
		DAOTask task = new AbstractDAOTask(con) {
			PreparedStatement getStatement = null;
			OperationFlow[] result;

			@Override
			public void doTask() throws Exception {
				getStatement = con.prepareStatement(GET_OP_PARENTS_BY_ID);
				getStatement.setLong(1, eventId);
				getStatement.setLong(2, operationId);

				ResultSet rs = getStatement.executeQuery();
				result = assembleOperationParentsResultSet(rs);
			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] { getStatement };
			}

			@Override
			public Object getResult() {
				return result;
			}
		};

		return (OperationFlow[]) DAOTemplate.performDAO(task);
	}

	public List<OperationFlow> getSpyEventOverview(final long eventId) throws Exception {
		DAOTask task = new AbstractDAOTask(con) {
			PreparedStatement getStatement = null;
			List<OperationFlow> results = new ArrayList<OperationFlow>();

			public void doTask() throws Exception {
				getStatement = con.prepareStatement(GET_EVENT_OVERVIEW);
				getStatement.setLong(1, eventId);
				ResultSet rs = getStatement.executeQuery();
				while (rs.next()) {
					OperationFlow flow = new OperationFlow();
					String longOp = rs.getString(1);
					flow.setLongSignature(longOp);

					long id = rs.getLong(2);
					flow.setId(id);

					long time = rs.getLong(3);
					flow.setExecutionTime(time);

					String report = rs.getString(4);
					flow.setAnalyzeReport(report);

					long callId = rs.getLong(5);
					flow.setCallingOpId(callId);

					results.add(flow);
				}

			};

			public Statement[] getStatement() {
				return new Statement[] { getStatement };
			};

			public Object getResult() {
				return results;
			};
		};

		return (List<OperationFlow>) DAOTemplate.performDAO(task);

	}

	public OperationFlow getOperationTreeByEventName(final String spyEvent) throws Exception {
		DAOTask task = new AbstractDAOTask(con) {
			OperationFlow result;

			@Override
			public Object getResult() {
				return result;
			}

			@Override
			public void doTask() throws Exception {
				long eventId = getEventIdByName(spyEvent);
				long count = getCountOfOperationTree(eventId);
				if (count > operationNumberLimit) {
					result = getOperationSummaryTree(eventId);
					result.setAnalyzeReport("this operation tree contains "
							+ count
							+ " operations, it is impossible to load it into memory, so only show the operations that are invoked most often ");
					return;
				}

				result = getOperationTreeByEventId(eventId);
			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] {};
			}
		};

		return (OperationFlow) DAOTemplate.performDAO(task);
	}

	private long getCountOfOperationTree(final long eventId) throws Exception {
		DAOTask task = new AbstractDAOTask(con) {
			PreparedStatement getStatement = null;

			long result = 0;

			@Override
			public void doTask() throws Exception {

				getStatement = con.prepareStatement(GET_COUNT);
				getStatement.setLong(1, eventId);
				ResultSet rs = getStatement.executeQuery();
				while (rs.next()) {
					result = rs.getLong(1);
					if (logger.isDebugEnabled()) {
						logger.debug("got the number of nodes of the operation tree for spy event "
								+ eventId + ":" + result);
					}
					return;
				}

			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] { getStatement };
			}

			@Override
			public Object getResult() {
				return result;
			}
		};
		return (Long) DAOTemplate.performDAO(task);
	}

	private OperationFlow getOperationSummaryTree(long eventId) throws Exception {
		OperationFlow first = OperationFlow.DUMMY();

		Collection<DuplicationSummary> dupSums = this.getDupOpSummeries(true, eventId);
		int index = 0;
		for (DuplicationSummary dupSum : dupSums) {
			OperationFlow op = new OperationFlow();
			op.setSignature(dupSum.longOpSig);
			op.setId(index++);
			op.setAnalyzeReport(dupSum.toString());

			first.addOutOp(op);
		}

		return first;
	}

	private OperationFlow assembleOperationTreeFromResultSet(ResultSet rs) throws SQLException {
		OperationFlow first = OperationFlow.DUMMY();
		OperationFlow parent = first;
		OperationFlow previousParent = null;

		while (rs.next()) {
			OperationFlow op = new OperationFlow();
			op.setId(rs.getLong(1));

			op.setSignature(rs.getString(3));
			op.setAnalyzeReport(rs.getString(4));
			op.setExecutionTime(rs.getLong(5));

			long callingOpId = rs.getLong(6);
			if (parent == null) {
				parent = previousParent.findOperationBottomUp(callingOpId);
			}
			parent.addOutOp(op);

			String returnValue = rs.getString(7);
			op.setReturnValueToString(returnValue);

			String paramsValue = rs.getString(8);
			op.setParametersToString(paramsValue);

			String longSig = rs.getString(9);
			op.setLongSignature(longSig);

			boolean isleaf = rs.getInt(2) == 0 ? false : true;
			if (!isleaf) {
				parent = op;
			} else {
				previousParent = parent;
				parent = null;
			}
		}
		return first;
	}

	private OperationFlow[] assembleOperationParentsResultSet(ResultSet rs) throws SQLException {

		OperationFlow me = null;
		OperationFlow child = null;

		while (rs.next()) {
			OperationFlow op = new OperationFlow();
			op.setId(rs.getLong(1));

			op.setSignature(rs.getString(3));
			op.setAnalyzeReport(rs.getString(4));
			op.setExecutionTime(rs.getLong(5));

			// long callingOpId = rs.getLong(6);

			String returnValue = rs.getString(7);
			op.setReturnValueToString(returnValue);

			String paramsValue = rs.getString(8);
			op.setParametersToString(paramsValue);

			String longSig = rs.getString(9);
			op.setLongSignature(longSig);

			if (child == null) {
				me = op;
			} else {
				op.addOutOp(child);
			}
			child = op;
		}
		return new OperationFlow[] { child, me };
	}

	public Collection<DuplicationSummary> getDupOpSummeries(final boolean firstN,
			final long spyEventId) throws Exception {
		DAOTask task = new AbstractDAOTask(con) {
			PreparedStatement getEventStatement = null;
			PreparedStatement getOpTreeStatement = null;

			Map<String, DuplicationSummary> dups = new HashMap<String, DuplicationSummary>();

			@Override
			public void doTask() throws Exception {
				if (firstN) {
					getEventStatement = con.prepareStatement(GET_FIRST_N_SUMMARY);
				} else {
					getEventStatement = con.prepareStatement(GET_SUMMARY);
				}
				getEventStatement.setLong(1, spyEventId);
				ResultSet rs = getEventStatement.executeQuery();
				while (rs.next()) {
					DuplicationSummary dup;

					String longOpSig = rs.getString(1);
					if (dups.containsKey(longOpSig)) {
						dup = dups.get(longOpSig);
					} else {
						dup = new DuplicationSummary();
						dups.put(longOpSig, dup);
					}

					dup.longOpSig = longOpSig;
					dup.addId2Time(rs.getLong(2), rs.getLong(3));
					dup.dupTimes = rs.getLong(4);
					dup.totalExecutionTime = rs.getLong(5);
				}

				getOpTreeStatement = con.prepareStatement(GET_OP_TREE);
				Set<String> longSigs = dups.keySet();

				for (String longSig : longSigs) {
					DuplicationSummary dup = dups.get(longSig);
					if (dup.totalExecutionTime < PerfSpyAnalyzer.getSlowThreshold()) {
						continue;
					}

					long id = dup.smallestId();
					while (id != -1) {
						getOpTreeStatement.setLong(1, spyEventId);
						getOpTreeStatement.setLong(2, id);
						getOpTreeStatement.setString(3, longSig);

						ResultSet rs2 = getOpTreeStatement.executeQuery();
						while (rs2.next()) {
							long childId = rs2.getLong(1);
							if (childId == id) {
								continue;
							}

							dup.thisDupIsAChildOp(childId);
						}

						dup.addNonChildDup(id);
						id = dup.smallestId();
					}

				}

			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] { getEventStatement, getOpTreeStatement };
			}

			@Override
			public Object getResult() {
				return dups.values();
			}
		};

		return (Collection<DuplicationSummary>) DAOTemplate.performDAO(task);
	}

}
