package com.perfspy.monitor.dao;

import java.sql.Connection;

import org.dom4j.Element;

public interface ConnectionProvider {
	public Connection getConnection();

	public void parseConnectionProvideEle(Element conEle);
}
