package com.perfspy.monitor.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * a simple template to avoid having to write repetitive try{}catch{}finally{}
 */
public class DAOTemplate {
	private static final Log logger = LogFactory.getLog(DAOTemplate.class);

	public static Object performDAO(DAOTask task) throws Exception {
		try {
			task.doTask();
			return task.getResult();
		} catch (SQLException e) {
			closeStatements(task);
			closeConnection(task.getConnection());
			throw e;
		} finally {
			closeStatements(task);
			if (task.lastTask()) {
				closeConnection(task.getConnection());
			}
		}

	}

	private static void closeStatements(DAOTask task) throws Exception {
		Statement[] states = task.getStatement();
		if (states != null) {
			for (Statement state : states) {
				closeStatement(state);
			}
		}
	}

	public static void closeConnection(Connection con) throws Exception {
		con.close();
	}

	/*
	 * statement.isClosed() throws out exception
	 * "java.lang.AbstractMethodError: org.jboss.resource.adapter.jdbc.WrappedPreparedStatement.isClosed()Z"
	 * plain jdbc doesn't have this problem, do not know why, so just close the
	 * statement and capture exceptions
	 */
	private static void closeStatement(Statement statement) throws Exception {
		if (statement != null) {
			statement.close();
			statement = null;
		}

	}
}
