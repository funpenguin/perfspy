package com.perfspy.monitor.dao;

import java.sql.Connection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.perfspy.monitor.exception.FatalException;
import com.perfspy.monitor.util.EnteredCFlowFlag;

public class DAOExecutor {

	private BlockingQueueWithEnd<DAOTask> dataQueue;
	private Thread runner;

	private AtomicBoolean started = new AtomicBoolean(false);
	private CountDownLatch waitToFinish = new CountDownLatch(1);
	private static final Log logger = LogFactory.getLog(DAOExecutor.class);
	private volatile FatalException faltalException;
	private Connection con;

	public DAOExecutor() {
		this(4000);
	}

	public DAOExecutor(int queueSize) {
		this.dataQueue = new BlockingQueueWithEnd<DAOTask>(queueSize); //500000
	}

	public void setConnection(Connection con) {
		this.con = con;
	}

	public void start() {
		if (started.compareAndSet(false, true)) {
			runner = new Thread(new TaskRunner(), "PerfSpyDAOTaskRunnerThread-"
					+ EnteredCFlowFlag.getSpyEvent());
			runner.setDaemon(false);
			runner.start();
		} else {
			throw new FatalException(" DAOExecutor is already running, you can't start it again!");
		}
	}

	public void executeTask(DAOTask task) {
		if (faltalException != null) {
			waitToFinish.countDown();
			logger.debug("put an end to the queue upon exception");
			dataQueue.putEnd();
			throw faltalException;
		} else {
			dataQueue.put(task);
		}
	}

	public void endOfTask() {
		logger.debug("put an end to the queue at the end of the task");
		dataQueue.putEnd();
	}

	public void waitToFinish() {
		try {
			waitToFinish.await();
			logger.debug("executor done");
		} catch (InterruptedException e) {
			throw new FatalException("error in waiting for the DAOExecutor to finish", e);
		}
	}

	class TaskRunner implements Runnable {
		public void run() {
			while (true) {

				DAOTask task = dataQueue.take();
				if (task == null) {
					break;
				}

				if (faltalException != null) {
					try {
						Thread.currentThread().sleep(50);
					} catch (InterruptedException e) {
						//ignore
					}
					continue;
				}

				try {
					DAOTemplate.performDAO(task);
					logger.debug("finished executing " + task);
					task = null;
				} catch (Exception e) {
					String msg = "Can't perform DAOTask " + task;
					logger.error(msg, e);
					faltalException = new FatalException(msg, e);
				}

			}

			//			System.gc();

			waitToFinish.countDown();

		}
	}

}
