package com.perfspy.monitor.dao;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;

import com.perfspy.monitor.exception.FatalException;

public class SimpleConnectionProvider extends AbstractConnectionProvider {

	private String jdbcUrl; // "jdbc:oracle:thin:@192.168.1.10:1521:ORCL";//
							// "jdbc:oracle:thin:@16.155.207.79:1521:ORCL";//
							// //

	private String user;// = "HR";// "PPM_CBA_914";// "HR";//

	private String password;// = "HR";// "PPM_CBA_914";// "HR";//
	private static final Log logger = LogFactory.getLog(SimpleConnectionProvider.class);

	@Override
	public void parseConnectionProvideEle(Element conEle) {
		Element urlEle = conEle.element("url");
		if (urlEle != null) {
			jdbcUrl = StringUtils.trim(urlEle.getStringValue());
		}
		Element userEle = conEle.element("user");
		if (userEle != null) {
			user = StringUtils.trim(userEle.getStringValue());
		}
		Element passEle = conEle.element("password");
		if (passEle != null) {
			password = StringUtils.trim(passEle.getStringValue());
		}
	}

	public void setJdbcUrl(String jdbc_url) {
		this.jdbcUrl = jdbc_url;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public SimpleConnectionProvider() throws Exception {

	}

	@Override
	public Connection getConnection() {
		try {
			//running inside tomcat7, have to explicitly register the driver
			//see http://stackoverflow.com/questions/5556664/how-to-fix-no-suitable-driver-found-for-jdbcmysql-localhost-dbname-error-w
			Class.forName("oracle.jdbc.driver.OracleDriver");

			Connection con = DriverManager.getConnection(jdbcUrl, user, password);
			// "jdbc:oracle:thin:@192.168.1.10:1521:ORCL", "HR", "HR");
			con.setAutoCommit(false);
			return con;
		} catch (Exception e) {
			logger.error(
					"Can't get a connection for:" + jdbcUrl + " with " + user + ":" + password, e);
			throw new FatalException("Can't get a connection", e);
		}
	}
}
