package com.perfspy.monitor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.perfspy.monitor.model.SpyEvent;

public class SpyEventDAO {
	private static final String GET_LAST_EVENTS = " select  * from ("
			+ "  select E.ID, E.SPYEVENT, E.SPYTIME from PerfSpyEvent e " //
			+ " /*where e.id in (#) */"//
			+ "  order by e.id desc" //
			+ ") where rownum <= ?";

	private static final String GET_OP_COUNT = " select o.spyevent, count(o.id) from perfspyops o"
			+ " where o.spyevent in (#) "//
			+ " group by o.spyevent";

	private static final String DELETE_EVENT_OPS = "delete from perfspyops o where o.spyevent in (#) ";
	private static final String DELETE_EVENT_OPDETAILS = "delete from PERFSPYOPDETAILS o where o.spyevent in (#)";
	private static final String DELETE_EVENT = "delete from PerfSpyEvent e where e.id in (#)";

	// if(eventId==-1){ delete all events }
	public static int deleteEvent(final Connection con, final boolean deleteAll, final long[] eventIds)
			throws Exception {
		DAOTask task = new AbstractDAOTask(con) {
			PreparedStatement deleteEventStatement = null;
			PreparedStatement deleteEventOpsStatement = null;
			PreparedStatement deleteEventOpDetailssStatement = null;
			int delRows;

			@Override
			public void doTask() throws Exception {

				if (deleteAll) { // delete all
					deleteEventOpsStatement = con.prepareStatement(stripWhere(DELETE_EVENT_OPS));
					deleteEventOpDetailssStatement = con.prepareStatement(stripWhere(DELETE_EVENT_OPDETAILS));
				} else {
					deleteEventOpsStatement = prepareInStatement(DELETE_EVENT_OPS, con, eventIds);

					deleteEventOpDetailssStatement = prepareInStatement(DELETE_EVENT_OPDETAILS, con, eventIds);
				}

				int delOpRows = deleteEventOpsStatement.executeUpdate();
				delOpRows += deleteEventOpDetailssStatement.executeUpdate();

				if (deleteAll) { // delete all
					deleteEventStatement = con.prepareStatement(stripWhere(DELETE_EVENT));
				} else {
					deleteEventStatement = prepareInStatement(DELETE_EVENT, con, eventIds);
				}
				int delEventRow = deleteEventStatement.executeUpdate();

				con.commit();

				delRows = delOpRows + delEventRow;
			}

			private PreparedStatement prepareInStatement(String sqlTemplate, Connection con, long[] inEles)
					throws SQLException {
				String deleteEventOpStr = replaceWithMarks(inEles.length, sqlTemplate);
				PreparedStatement prepareStatement = con.prepareStatement(deleteEventOpStr);
				int i = 1;
				for (Long id : inEles) {
					prepareStatement.setLong(i++, id);
				}

				return prepareStatement;
			}

			private String stripWhere(String sqlTemplate) {
				int pos = sqlTemplate.indexOf("where");
				return sqlTemplate.substring(0, pos);

			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] { deleteEventOpsStatement, deleteEventStatement };
			}

			@Override
			public Object getResult() {
				return delRows;
			}
		};

		return (Integer) DAOTemplate.performDAO(task);
	}

	public static Collection<SpyEvent> getLastNEvents(final Connection con, final int n, final Long... ids)
			throws Exception {
		DAOTask task = new AbstractDAOTask(con) {
			PreparedStatement getStatement = null;
			PreparedStatement getOpCountStatement = null;

			Map<Long, SpyEvent> events = new LinkedHashMap<Long, SpyEvent>();

			@Override
			public void doTask() throws Exception {
				if (n == 0) {
					return;
				}
				String getLastEventSql = GET_LAST_EVENTS;
				if (ids.length > 0) {
					getLastEventSql = replaceWithMarks(ids.length, getLastEventSql);
					getLastEventSql = getLastEventSql.replace("/*", "");
					getLastEventSql = getLastEventSql.replace("*/", "");
				}
				getStatement = con.prepareStatement(getLastEventSql);
				if (ids.length > 0) {
					int i = 1;
					for (Long id : ids) {
						getStatement.setLong(i++, id);
					}
					// "where rownum <= ?" is not required, yet removing it is
					// such a chore, as it is not going to affect performance,
					// leave it there
					getStatement.setLong(i, n);
				} else {
					getStatement.setLong(1, n);
				}

				ResultSet rs = getStatement.executeQuery();

				while (rs.next()) {
					long id = rs.getLong(1);
					String name = rs.getString(2);
					long spyTime = rs.getLong(3);
					if (rs.wasNull()) {
						spyTime = -1;
					}
					SpyEvent event = new SpyEvent(id, name, spyTime);
					events.put(id, event);
				}

				Set<Long> eventIds = events.keySet();
				int eventNums = eventIds.size();
				if (eventNums == 0) {
					return;
				}

				String getOpCountSql = GET_OP_COUNT;
				getOpCountSql = replaceWithMarks(eventNums, getOpCountSql);

				getOpCountStatement = con.prepareStatement(getOpCountSql);
				int i = 1;
				for (Long id : eventIds) {
					getOpCountStatement.setLong(i++, id);
				}
				rs = getOpCountStatement.executeQuery();
				while (rs.next()) {
					long id = rs.getLong(1);
					long count = rs.getLong(2);
					SpyEvent event = events.get(id);
					event.setOperationCount(count);
				}

			}

			@Override
			public Statement[] getStatement() {
				return new Statement[] { getStatement, getOpCountStatement };
			}

			@Override
			public Object getResult() {
				return events.values();
			}
		};

		return (Collection<SpyEvent>) DAOTemplate.performDAO(task);
	}

	private static String replaceWithMarks(int num, String sql) {
		StringBuffer mark = new StringBuffer("?");
		for (int i = 1; i < num; i++) {
			mark.append(",?");
		}
		sql = sql.replace("#", mark.toString());
		return sql;
	}

}
