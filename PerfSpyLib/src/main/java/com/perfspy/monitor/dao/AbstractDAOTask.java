package com.perfspy.monitor.dao;

import java.sql.Connection;

public abstract class AbstractDAOTask implements DAOTask {
	private Connection con;

	public AbstractDAOTask(Connection con) {
		this.con = con;
	}

	public AbstractDAOTask() {
	}

	@Override
	public Connection getConnection() {
		return con;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean lastTask() {
		return false;
	}
}