package com.perfspy.monitor.dao;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.google.common.collect.AbstractIterator;

/**
 * a wrapper of BlockingQueue, where the producer can signal the end *
 */
public class BlockingQueueWithEnd<T> extends AbstractIterator<T> {

	private BlockingQueue<Carrier<T>> blockingQueue;
	private String queueName;
	private volatile boolean ended = false;

	public BlockingQueueWithEnd() {
		this(5000, "queue");
	}

	public BlockingQueueWithEnd(int capacity, String queueName) {
		this.blockingQueue = new ArrayBlockingQueue<Carrier<T>>(capacity);
		this.queueName = queueName;
	}

	public BlockingQueueWithEnd(int capacity) {
		this(capacity, "queue");
	}

	private static class Carrier<T> {
		T data;

		public Carrier(T data) {
			this.data = data;
		}
	}

	public void put(T data) {
		try {
			this.blockingQueue.put(new Carrier<T>(data));
		} catch (InterruptedException e) {
			throw new RuntimeException("interrupted while putting " + data + " into the queue "
					+ this.queueName);
		}
	}

	public void putEnd() {
		if (ended) {
			return;
		}
		this.put(null);
		ended = true;
	}

	public T take() {
		Carrier<T> carrier = null;
		try {
			carrier = this.blockingQueue.take();
		} catch (InterruptedException e) {
			throw new RuntimeException("interrupted while getting data " + " out of the queue "
					+ this.queueName);
		}
		return carrier.data;
	}

	// public void empty() {
	// this.blockingQueue.clear();
	// this.reset();
	// }

	@Override
	protected T computeNext() {
		Carrier<T> carrier = null;
		try {
			carrier = this.blockingQueue.take();
		} catch (InterruptedException e) {
			throw new RuntimeException("interrupted while getting data " + " out of the queue "
					+ this.queueName);
		}

		if (carrier.data != null) {
			return carrier.data;
		}
		return this.endOfData();

	}

	@Override
	public String toString() {
		return this.queueName;
	}
}
