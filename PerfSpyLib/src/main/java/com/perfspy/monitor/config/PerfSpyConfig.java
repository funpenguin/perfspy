package com.perfspy.monitor.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.InputSource;

import com.cedarsoftware.util.config.JSonIOConfig;
import com.google.common.collect.Maps;
import com.perfspy.monitor.analyze.PerfSpyAnalyzer;
import com.perfspy.monitor.dao.ConnectionProvider;
import com.perfspy.monitor.exception.FatalException;

public class PerfSpyConfig {
	public static final String DETAIL_THIS = "this";
	public static final String DETAIL_PARAM = "param";
	public static final String DETAIL_RETURN = "return";

	static class EntryTarget {
		String name;
		String persist;
		boolean enabled;

		List<String> skipOperations = new ArrayList<String>();
		List<DetailOp> detailOps = new ArrayList<DetailOp>();
		List<String> analyzers = new ArrayList<String>();
		List<String> referSkips = new ArrayList<String>();

		Element conProviderEle;
		ConnectionProvider conProvider;

		EntryTarget(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return this.name;
		}
	}

	static class DetailOp {

		String operationName;
		private String[] parts;

		boolean isCaptureAllMethods() {
			return "all".equals(operationName);
		}

		public void setParts(String captureParts) {
			if ("all".equals(captureParts) || captureParts == null) {
				parts = new String[] { DETAIL_RETURN, DETAIL_PARAM };
			} else {
				parts = captureParts.split("[^\\w\\d]+");
			}
		}

	}

	static class UIConfig {
		long nodeLimit = -1;
		int pollFrequncy; // in seconds
	}

	private Map<String, EntryTarget> entryTargets = new HashMap<String, EntryTarget>();
	private Map<String, EntryTarget> baseTargets = new HashMap<String, EntryTarget>();
	private Map<String, EntryTarget> packageTargets = new HashMap<String, EntryTarget>();

	private static final Log logger = LogFactory.getLog(PerfSpyConfig.class);
	private Set<String> defaultSkipOperations = new HashSet<String>();
	private Set<String> defaultAnalyzers = new HashSet<String>();
	private Element defaultConProviderEle;
	private ConnectionProvider defaultConProvider;
	private boolean skipHibernateLazyEnhanceByCGLib = true;
	private UIConfig uiConfig;
	private JSonIOConfig ioConfig;

	private Map<String, Set<String>> skippedMethods = Maps.newHashMap();

	private int simpleQueueSize = 4000;
	private int detailQueueSize = 150;
	private int simpleBatchSize = 2000;
	private int detailBatchSize = 50;

	private PerfSpyConfig() {
	}

	public static PerfSpyConfig config(String fileName) throws Exception {
		PerfSpyConfig config = new PerfSpyConfig();
		org.dom4j.Document doc = config.readConfigFile(null, fileName);
		config.parseConfig(doc);
		return config;
	}

	public static PerfSpyConfig config(Class callingFrom, String fileName) throws Exception {
		PerfSpyConfig config = new PerfSpyConfig();
		org.dom4j.Document doc = config.readConfigFile(callingFrom, fileName);
		config.parseConfig(doc);
		return config;
	}

	private void parseConfig(org.dom4j.Document doc) throws Exception {
		parseSkips(doc);
		parseAnalysis(doc);
		parseConnection(doc);
		parseEntries(doc);
		parseLogToDb(doc);
		parseUI(doc);

		Element jsonioEle = doc.getRootElement().element("perfspyio");
		if (jsonioEle != null) {
			this.ioConfig = JSonIOConfig.configFromJSonIOElement(jsonioEle);
		}
	}

	private org.dom4j.Document readConfigFile(Class callingFrom, String fileName) throws Exception {
		InputStream stream = getResourceAsStream(callingFrom, fileName);
		if (stream == null) {
			throw new IOException("can't find file " + fileName);
		}
		org.dom4j.Document doc;
		try {
			SAXReader saxReader = new SAXReader();
			doc = saxReader.read(new InputSource(stream));
		} catch (DocumentException e) {
			throw new Exception("Could not parse configuration: " + fileName, e);
		} finally {
			stream.close();
		}
		return doc;
	}

	private synchronized static InputStream getResourceAsStream(Class callingFrom, String fileName) throws Exception {
		if (callingFrom == null) {
			callingFrom = PerfSpyConfig.class;
		}
		InputStream stream = callingFrom.getResourceAsStream(fileName);
		if (stream == null) {
			stream = callingFrom.getClassLoader().getResourceAsStream(fileName);
		}
		if (stream == null) {
			stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
		}

		if (stream == null) {
			stream = new FileInputStream(new File(fileName));
		}

		return stream;
	}

	private void parseSkips(org.dom4j.Document doc) {
		Element skips = doc.getRootElement().element("skips");
		parseSkips(null, skips);
	}

	private void parseSkips(String target, Element skips) {
		if (skips == null) {
			return;
		}
		EntryTarget t = null;
		if (target != null) {
			t = entryTargets.get(target);
		}
		List<Element> skipEles = skips.elements("skip");
		for (Element skip : skipEles) {
			String skipOp = skip.getStringValue();
			if (StringUtils.isNotBlank(skipOp)) {
				if (t == null) {
					defaultSkipOperations.add(StringUtils.strip(skipOp));
				} else {
					t.skipOperations.add(StringUtils.strip(skipOp));
				}
			}
		}

		if (t == null) {
			return;
		}

		List<Element> referSkipEles = skips.elements("refer");
		for (Element referSkipEle : referSkipEles) {
			String referSkip = referSkipEle.getStringValue();
			if (StringUtils.isNotBlank(referSkip)) {
				t.referSkips.add(StringUtils.strip(referSkip));
			}
		}

	}

	private void parseDetailOps(String target, Element detailOpsEle) {
		if (detailOpsEle == null) {
			return;
		}
		EntryTarget t = entryTargets.get(target);

		String captureAllMethods = detailOpsEle.attributeValue("all");
		DetailOp detailOp = null;
		if ("true".equals(captureAllMethods)) {
			detailOp = new DetailOp();
			detailOp.operationName = "all";
		}

		String captureParts = detailOpsEle.attributeValue("parts");
		if (detailOp != null) {
			detailOp.setParts(captureParts);
		}

		if (detailOp != null && detailOp.isCaptureAllMethods()) {
			t.detailOps.add(detailOp);
			return;
		}

		List<Element> detailOpEles = detailOpsEle.elements("detailOp");
		for (Element detailOpEle : detailOpEles) {
			String detailOpName = detailOpEle.getStringValue();
			DetailOp detailOp0 = new DetailOp();
			detailOp0.operationName = detailOpName;

			String captureParts0 = detailOpEle.attributeValue("parts");
			if (StringUtils.isNotBlank(captureParts0)) {
				detailOp0.setParts(captureParts0);
			} else {
				// inherit parts
				detailOp0.setParts(captureParts);
			}

			t.detailOps.add(detailOp0);
		}

	}

	private void parseEntries(org.dom4j.Document doc) {
		List<Element> entries = doc.getRootElement().elements("entry");
		for (Element entry : entries) {
			EntryTarget t = null;
			String target = entry.attributeValue("target");
			if (StringUtils.isNotBlank(target)) {
				t = new EntryTarget(target);
				entryTargets.put(target, t);

				if (target.endsWith("+")) {
					baseTargets.put(target.substring(0, target.length() - 1), t);
					skippedMethods.put(target.substring(0, target.length() - 1), new HashSet<String>());
				} else if (target.endsWith("..")) {
					packageTargets.put(target.substring(0, target.length() - 2), t);
					skippedMethods.put(target.substring(0, target.length() - 2), new HashSet<String>());
				} else {
					skippedMethods.put(target, new HashSet<String>());
				}

			} else {
				continue;
			}

			String persist = entry.attributeValue("persist");
			if (StringUtils.isBlank(persist)) {
				persist = "FILE";
			}
			t.persist = persist;

			String enabled = entry.attributeValue("enabled");
			if (StringUtils.isBlank(enabled)) {
				t.enabled = true;
			} else {
				t.enabled = Boolean.valueOf(enabled);
			}

			Element skips = entry.element("skips");
			parseSkips(target, skips);

			Element detailOps = entry.element("detailOps");
			parseDetailOps(target, detailOps);

			Element conEle = entry.element("connection");
			parseConnection(target, conEle);

			Element anaEle = entry.element("analysis");
			parseAnalyzers(target, anaEle);
		}
	}

	private void parseUI(Document doc) {
		Element rootEle = doc.getRootElement();
		Element uiEle = rootEle.element("ui");
		if (uiEle == null) {
			return;
		}

		UIConfig uiConfig = new UIConfig();
		this.uiConfig = uiConfig;

		Element operationTreeEle = uiEle.element("tree");
		String nodeLimitTxt = operationTreeEle.attributeValue("nodeLimit");
		long nodeLimit = -1;
		try {
			nodeLimit = Long.decode(nodeLimitTxt);
		} catch (NumberFormatException e) {
			// ignore
		}

		uiConfig.nodeLimit = nodeLimit;
	}

	public Long getTreeNodeLimit() {
		if (uiConfig != null) {
			return uiConfig.nodeLimit;
		}

		return (long) -1;
	}

	private void parseAnalysis(org.dom4j.Document doc) {
		Element rootEle = doc.getRootElement();
		Element analysisEle = rootEle.element("analysis");
		if (analysisEle == null) {
			return;
		}
		Element slow = analysisEle.element("slow");
		String slowThreshold = slow.getStringValue();
		if (StringUtils.isNotBlank(slowThreshold)) {
			try {
				int i = Integer.valueOf(slowThreshold);
				PerfSpyAnalyzer.setSlowThreshold(i);

			} catch (NumberFormatException e) {
				logger.error(
						"can't parse slow threshold '" + slowThreshold + "', use default value 1000 million seconds",
						e);
				PerfSpyAnalyzer.setSlowThreshold(1000);
			}
		}

		parseAnalyzers(null, analysisEle);
	}

	private void parseAnalyzers(String target, Element analysis) {
		if (analysis == null) {
			return;
		}
		EntryTarget t = null;
		if (target != null) {
			t = entryTargets.get(target);
		}
		List<Element> anaEles = analysis.elements("analyzer");
		for (Element anaEle : anaEles) {
			String analyzerName = anaEle.getStringValue();
			if (StringUtils.isNotBlank(analyzerName)) {
				if (t == null) {
					defaultAnalyzers.add(analyzerName);
				} else {
					t.analyzers.add(analyzerName);
				}
			}
		}
	}

	private void parseConnection(Document doc) {
		Element conEle = doc.getRootElement().element("connection");
		parseConnection(null, conEle);
	}

	private void parseConnection(String target, Element conEle) {
		if (conEle == null) {
			return;
		}
		EntryTarget t = null;
		if (target != null) {
			t = entryTargets.get(target);
		}

		if (t == null) {
			defaultConProviderEle = conEle;
		} else {
			t.conProviderEle = conEle;
		}
	}

	private void parseLogToDb(Document doc) {
		Element logToDbEle = doc.getRootElement().element("logToDb");
		parseLogToDb(logToDbEle);
	}

	private void parseLogToDb(Element logToDbEle) {
		if (logToDbEle == null) {
			return;
		}
		Element queueSizeEle = logToDbEle.element("queuesize");
		int simple = parseIntegerAttriValue(queueSizeEle, "simple");
		if (simple != -1) {
			this.simpleQueueSize = simple;
		}

		int detail = parseIntegerAttriValue(queueSizeEle, "detail");
		if (detail != -1) {
			this.detailQueueSize = detail;
		}

		Element batchSizeEle = logToDbEle.element("batchsize");
		int simpleBatch = parseIntegerAttriValue(batchSizeEle, "simple");
		if (simpleBatch != -1) {
			this.simpleBatchSize = simpleBatch;
		}

		int detailBatch = parseIntegerAttriValue(batchSizeEle, "detail");
		if (detailBatch != -1) {
			this.detailBatchSize = detailBatch;
		}

	}

	private int parseIntegerAttriValue(Element ele, String attribute) {
		String simpleQueueV = ele.attributeValue(attribute);
		if (StringUtils.isNotBlank(simpleQueueV)) {
			try {
				return Integer.valueOf(simpleQueueV);
			} catch (NumberFormatException e) {
				logger.error("can't parse the " + ele, e);
				return -1;
			}
		}
		return -1;
	}

	public String getPersistTo(String target) {
		// Set<String> keys = entryTargets.keySet();
		// for (String entryMethodTarget : keys) {
		// if (target.contains(entryMethodTarget)) {
		// return entryTargets.get(entryMethodTarget).persist;
		// }
		// }
		// return "FILE";

		return "DB";
	}

	public Set<String> getDefaultSkipOperations() {
		return defaultSkipOperations;
	}

	public List<PerfSpyAnalyzer> getAnalyzers(String entryTarget) {
		List<PerfSpyAnalyzer> analyzers = new ArrayList<PerfSpyAnalyzer>();
		// analyzers are not thread safe, so create new instances everytime

		for (String analyzerName : defaultAnalyzers) {
			PerfSpyAnalyzer analyzer = createAnalyzerInstance(analyzerName);
			analyzers.add(analyzer);
		}

		EntryTarget t = entryTargets.get(entryTarget);
		if (t != null) {
			for (String analyzerName : t.analyzers) {
				PerfSpyAnalyzer analyzer = createAnalyzerInstance(analyzerName);
				analyzers.add(analyzer);
			}
		}

		return analyzers;

	}

	private PerfSpyAnalyzer createAnalyzerInstance(String analyzerName) {
		PerfSpyAnalyzer analyzer;
		try {
			analyzer = (PerfSpyAnalyzer) Class.forName(analyzerName).newInstance();
		} catch (Exception e) {
			throw new RuntimeException("Can't create an analyzer instance for " + analyzerName);
		}
		return analyzer;
	}

	public boolean isTargetEnabled(String entryTarget) {
		logger.trace("checking " + entryTarget + " among all targets");
		logger.trace(entryTargets);

		EntryTarget target = entryTargets.get(entryTarget);
		if (target != null) {
			return target.enabled;
		} else {
			Set<String> bases = baseTargets.keySet();
			for (String base : bases) {
				try {
					if (Class.forName(base).isAssignableFrom(Class.forName(entryTarget))) {
						entryTargets.put(entryTarget, baseTargets.get(base));

						skippedMethods.put(entryTarget, skippedMethods.get(base));

						return baseTargets.get(base).enabled;
					}
				} catch (ClassNotFoundException e) {
					logger.error("Can't determin if entryTarget is a subclass of " + base, e);
				}
			}

			Set<String> packages = packageTargets.keySet();
			for (String pack : packages) {
				if (entryTarget.startsWith(pack)) {
					entryTargets.put(entryTarget, packageTargets.get(pack));

					skippedMethods.put(entryTarget, skippedMethods.get(pack));

					return packageTargets.get(pack).enabled;
				}
			}

			return false;
		}
	}

	public Connection getConnection(String entryTarget) throws Exception {
		if (!StringUtils.isBlank(entryTarget)) {

			EntryTarget t = entryTargets.get(entryTarget);

			if (t != null) {
				if (t.conProvider != null) {
					Connection con = t.conProvider.getConnection();
					con.setAutoCommit(false);
					return con;
				} else {
					if (t.conProviderEle != null) {
						String conProviderName = t.conProviderEle.attributeValue("provider");
						ConnectionProvider p;
						try {
							p = (ConnectionProvider) Class.forName(conProviderName).newInstance();
							p.parseConnectionProvideEle(t.conProviderEle);
						} catch (Exception e) {
							throw new FatalException("Can't get a ConnectionProvider", e);
						}
						t.conProvider = p;
						Connection con = t.conProvider.getConnection();
						con.setAutoCommit(false);
						return con;
					}
				}
			}

		}

		if (defaultConProvider != null) {
			return defaultConProvider.getConnection();
		} else {
			if (defaultConProviderEle != null) {
				String defaultConProviderName = defaultConProviderEle.attributeValue("provider");

				ConnectionProvider p;
				try {
					p = (ConnectionProvider) Class.forName(defaultConProviderName).newInstance();
					p.parseConnectionProvideEle(defaultConProviderEle);
				} catch (Exception e) {
					throw new FatalException("Can't get a ConnectionProvider", e);
				}
				defaultConProvider = p;
				Connection con = defaultConProvider.getConnection();
				con.setAutoCommit(false);
				return con;

			}
		}

		throw new FatalException("no ConnectionProvider is configured, or configured wrongly");

	}

	public int getSimpleQueueSize() {
		return simpleQueueSize;
	}

	public int getDetailQueueSize() {
		return detailQueueSize;
	}

	public int getSimpleBatchSize() {
		return simpleBatchSize;
	}

	public int getDetailBatchSize() {
		return detailBatchSize;
	}

	public int getQueueSize(String entryTarget) {
		if (isToLogDetails(entryTarget)) {
			return this.detailQueueSize;
		}
		return this.simpleQueueSize;
	}

	public boolean isToLogDetails(String entryTarget) {
		EntryTarget t = entryTargets.get(entryTarget);
		if (t != null) {
			return t.detailOps.size() > 0;
		}
		return false;

	}

	public String[] logDetails(String entryTarget, String longOpSig) {

		longOpSig = getSigWithoutModifierParameter(longOpSig);

		EntryTarget t = entryTargets.get(entryTarget);

		if (t != null) {
			if (t.detailOps.size() == 0) {
				return new String[0];
			}

			if (t.detailOps.get(0).isCaptureAllMethods()) {
				return t.detailOps.get(0).parts;
			}

			for (DetailOp detailOp : t.detailOps) {
				if (longOpSig.startsWith(detailOp.operationName)) {
					return detailOp.parts;
				}
			}
		}

		return new String[0];
	}

	public boolean isToSkip(String entryTarget, String longOpSig0) {

		if (skippedMethods.get(entryTarget) != null && skippedMethods.get(entryTarget).contains(longOpSig0)) {
			return true;
		}

		String longOpSig = getSigWithoutModifierParameter(longOpSig0);

		for (String toSkip : defaultSkipOperations) {
			if (longOpSig.startsWith(toSkip)) {
				skippedMethods.get(entryTarget).add(longOpSig0);
				return true;
			}
		}

		EntryTarget t = entryTargets.get(entryTarget);
		if (t == null || t.skipOperations == null) {
			return false;
		}
		for (String toSkip : t.skipOperations) {
			if (longOpSig.contains(toSkip)) {
				skippedMethods.get(entryTarget).add(longOpSig0);
				return true;
			}
		}

		for (String referSkip : t.referSkips) {
			if (isToSkip(referSkip, longOpSig)) {
				skippedMethods.get(entryTarget).add(longOpSig0);
				return true;
			}
		}

		return false;
	}

	static String getSigWithoutModifierParameter(String op) {
		int index = op.indexOf('(');

		if (index != -1) {
			op = op.substring(0, index);
		}

		Matcher matcher = METHOD_NAME_PATTERN.matcher(op);
		if (matcher.find()) {
			return matcher.group(2);
		}

		return op;
	}

	public JSonIOConfig getJsonIOConfig() {
		return ioConfig;
	}

	private final static Pattern METHOD_NAME_PATTERN = Pattern.compile("(.*)\\s+(.*)");

}
