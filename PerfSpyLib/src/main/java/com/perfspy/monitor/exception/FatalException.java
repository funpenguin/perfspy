package com.perfspy.monitor.exception;

/**
 * a marker class
 */
public class FatalException extends RuntimeException {
	public FatalException(String message, Throwable cause) {
		super(message, cause);
	}

	public FatalException(String message) {
		super(message);
	}
}
