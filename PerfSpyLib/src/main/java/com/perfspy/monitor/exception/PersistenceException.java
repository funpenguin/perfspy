package com.perfspy.monitor.exception;


public class PersistenceException extends FatalException {
	public PersistenceException(String message, Throwable cause) {
		super(message, cause);
	}
}
