package com.perfspy.monitor.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Ordering;

public class DuplicationSummary {
	public String longOpSig;

	public long dupTimes;

	public long totalExecutionTime;

	private Map<Long, Long> id2Time = new HashMap<Long, Long>();
	private Map<Long, Long> nonChildDup = new HashMap<Long, Long>();

	public void addId2Time(Long id, Long executionTime) {
		id2Time.put(id, executionTime);
	}

	public void thisDupIsAChildOp(Long id) {
		if (id2Time.containsKey(id)) {
			dupTimes--;
			totalExecutionTime -= id2Time.get(id);
			id2Time.remove(id);
		}
	}

	public void addNonChildDup(Long id) {
		nonChildDup.put(id, id2Time.get(id));
		id2Time.remove(id);
	}

	public Long smallestId() {
		Set<Long> keys = id2Time.keySet();
		if (keys.size() > 0) {
			return Ordering.natural().min(keys);
		} else {
			return (long) -1;
		}
	}

	// public List<Long> sortedIds() {
	// Set<Long> keys = id2Time.keySet();
	// return Ordering.natural().sortedCopy(keys);
	// }

	public String getSummary() {
		return " invoked " + dupTimes + " times, with total execution time "
				+ totalExecutionTime;
	}

	@Override
	public String toString() {
		return longOpSig + " was invoked " + dupTimes
				+ " times, with total execution time " + totalExecutionTime
				+ ", details:" + nonChildDup;
	}
}
