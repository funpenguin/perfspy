package com.perfspy.monitor.model;

public class FailToLoad {

	public static final FailToLoad instance = new FailToLoad();

	private static final String FAIL_TO_LOAD = "Fail to load";

	private FailToLoad() {
	}

	@Override
	public String toString() {
		return FAIL_TO_LOAD;
	}
}
