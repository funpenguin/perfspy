package com.perfspy.monitor.model;

public class SpyEvent implements Comparable<SpyEvent> {
	private long id;
	private String name;
	private long spyTime = -1;
	private long operationCount;

	public SpyEvent(long id, String name, long spyTime) {
		super();
		this.id = id;
		this.name = name;
		this.spyTime = spyTime;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SpyEvent)) {
			return false;
		}

		SpyEvent that = (SpyEvent) obj;
		return this.id == that.id;
	}

	@Override
	public int hashCode() {
		return (int) this.id;
	}

	@Override
	public int compareTo(SpyEvent o) {
		return (int) (this.id - o.id);
	}

	public boolean isFinished() {
		return spyTime != -1;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public long getSpyTime() {
		return spyTime;
	}

	public void setSpyTime(long spyTime) {
		this.spyTime = spyTime;
	}

	public long getOperationCount() {
		return operationCount;
	}

	public void setOperationCount(long operationCount) {
		this.operationCount = operationCount;
	};

}
