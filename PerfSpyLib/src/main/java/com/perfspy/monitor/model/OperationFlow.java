package com.perfspy.monitor.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.NDC;

import com.cedarsoftware.util.io.JsonWriter;
import com.perfspy.monitor.util.EnteredCFlowFlag;
import com.perfspy.monitor.util.ToStringUtil;

public class OperationFlow implements Serializable {
	private static final String DUMMY = "DUMMY";

	private long id = 0;

	private String signature;

	private String longSignature;

	private long executionTime = 0; // in seconds

	private List<OperationFlow> outOps = new ArrayList<OperationFlow>();

	public OperationFlow callingOp;
	public long callingOpId = -1;

	private Object[] parameters;

	private String[] parametersNames;

	private transient String parametersToString = null;
	private transient String returnValueToString = null;

	private transient String parametersJson = null;
	private transient String returnValueJson = null;

	private Object returnValue;

	private Map<String, AnalyzeResult> analyzeResults = new HashMap<String, AnalyzeResult>();

	private String analyzeReport;

	// private boolean slow = false;

	private boolean memoryRisk = false;

	private int objectCount;
	private boolean selected = false;

	private static final Log logger = LogFactory.getLog(OperationFlow.class);
	// TODO: configure this
	private int toStringLimit = 1000;

	private String shortClassName;

	// if an operation's parameters and return value are logged in details
	private boolean detailLogged = true;
	// if an operation's parameters and return value details are fetched from DB
	// and set upon the operation
	private boolean fleshed = false;

	public OperationFlow() {

	}

	public OperationFlow(String signature) {
		this.signature = signature;
		this.longSignature = signature;
	}

	public static OperationFlow DUMMY() {
		OperationFlow d = new OperationFlow(DUMMY);
		d.setId(0);
		d.setLongSignature(DUMMY);
		return d;
	}

	public boolean isDummy() {
		return this.signature.equals(DUMMY);
	}

	public void removeOutOp(OperationFlow op) {
		this.outOps.remove(op);
	}

	public OperationFlow findOperationUpDown(long id) {
		if (this.getId() == id) {
			return this;
		}

		for (OperationFlow op : outOps) {
			OperationFlow op1 = op.findOperationUpDown(id);
			if (op1 != null) {
				return op1;
			}
		}
		return null;
	}

	public OperationFlow findOperationBottomUp(long id) {
		if (this.getId() == id) {
			return this;
		}

		if (this.callingOp == null) {
			return null;
		}

		return this.callingOp.findOperationBottomUp(id);
	}

	public void runAwayFromParent() {
		this.callingOp.removeOutOp(this);
		this.callingOp = null;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getShortClassName() {
		if (this.shortClassName != null) {
			return shortClassName;
		} else {
			int i = this.signature.indexOf(".");
			this.shortClassName = this.signature.substring(0, i);
			return this.shortClassName;
		}
	}

	public String getLongSignature() {
		return longSignature;
	}

	public void setLongSignature(String longSignature) {
		this.longSignature = longSignature;
	}

	public void addOutOps(List<OperationFlow> ops) {
		this.outOps.addAll(ops);

		for (OperationFlow op : ops) {
			op.callingOp = this;
			op.callingOpId = this.getId();
		}
	}

	public void addOutOp(OperationFlow op) {
		outOps.add(op);
		op.callingOp = this;
		op.callingOpId = this.getId();
	}

	public OperationFlow getFirstOutOp() {
		if (this.outOps.size() > 0) {
			return this.outOps.get(0);
		}
		return null;
	}

	public OperationFlow getCallingOp() {
		return callingOp;
	}

	public long getCallingOpId() {
		return callingOpId;
	}

	public void setCallingOpId(long callingOpId) {
		this.callingOpId = callingOpId;
	}

	// TODO: return an immutable list
	public List<OperationFlow> getOutOps() {
		return this.outOps;
	}

	// TODO: return an immutable list
	// public List<Object> getParameters() {
	// return parameters;
	// }

	public String parametersToJson() {
		String parameterAsJson = "";

		// size() && iterator() method might trigger aspect to capture them, so
		// enterSkip to avoid capturing
		EnteredCFlowFlag.enterSkip();

		if (this.parameters != null && this.parameters.length > 0) {
			try {
				parameterAsJson = JsonWriter.objectToJson(this.parameters, EnteredCFlowFlag
						.getPerfSpyConfig().getJsonIOConfig(), true, true);
			} catch (Exception e) {
				logger.debug("error in writting paramter json for " + this.longSignature, e);
			}
		}

		EnteredCFlowFlag.exitSkip();

		return parameterAsJson;
	}

	public String getParametersToString() {
		if (this.parametersToString != null) {
			return this.parametersToString;
		}

		if (this.parameters == null || this.parameters.length == 0) {
			this.parametersToString = "";
			return this.parametersToString;
		}

		// size() && iterator() method might trigger aspect to capture them, so
		// enterSkip to avoid capturing
		EnteredCFlowFlag.enterSkip();

		StringBuffer paramBuffer = new StringBuffer("");
		for (int i = 0, n = parameters.length; i < n; i++) {
			Object param = parameters[i];
			String paramName = parametersNames[i];

			if (param == null) {
				paramBuffer.append(paramName + ":null");
			} else {
				paramBuffer.append(paramName + ":" + ToStringUtil.toString(param));
			}
			if (i != n - 1) {
				paramBuffer.append(", ");
			}

			this.parametersToString = paramBuffer.toString();

			if (this.parametersToString.length() > toStringLimit) {
				this.parametersToString = this.parametersToString.substring(0,
						this.toStringLimit - 3) + "...";
			}

		}

		EnteredCFlowFlag.exitSkip();

		return this.parametersToString;
	}

	public void setParametersToString(String parametersAsString) {
		this.parametersToString = parametersAsString;
	}

	// public Object getReturnValue() {
	// return returnValue;
	// }

	public void setReturnValue(Object returnValue) {
		this.returnValue = returnValue;
	}

	public void setReturnValueToString(String returnValueAsString) {
		this.returnValueToString = returnValueAsString;
	}

	public String returnValueToJson() {
		EnteredCFlowFlag.enterSkip();

		String returnValueAsJson = "";
		try {
			returnValueAsJson = JsonWriter.objectToJson(this.returnValue, EnteredCFlowFlag
					.getPerfSpyConfig().getJsonIOConfig(), true, true);
		} catch (Exception e) {
			logger.debug("error in writting return json for " + this.longSignature, e);
		}

		EnteredCFlowFlag.exitSkip();
		return returnValueAsJson;
	}

	public String getReturnValueToString() {
		if (this.returnValueToString != null) {
			return this.returnValueToString;
		}

		EnteredCFlowFlag.enterSkip();

		if (this.returnValue == null) {
			this.returnValueToString = "null";
		} else {
			this.returnValueToString = ToStringUtil.toString(this.returnValue);
		}

		if (this.returnValueToString != null && this.returnValueToString.length() > toStringLimit) {
			this.returnValueToString = this.returnValueToString
					.substring(0, this.toStringLimit - 3) + "...";
		}

		EnteredCFlowFlag.exitSkip();

		return this.returnValueToString;
	}

	public String getParametersJson() {
		return parametersJson;
	}

	public void setParametersJson(String parametersJson) {
		this.parametersJson = parametersJson;
	}

	public String getReturnValueJson() {
		return returnValueJson;
	}

	public void setReturnValueJson(String returnValueJson) {
		this.returnValueJson = returnValueJson;
	}

	public void setParameters(Object[] params) {
		this.parameters = params;
	}

	// public Object[] getParameters() {
	// return parameters;
	// }

	public boolean isDetailLogged() {
		return detailLogged;
	}

	public void setDetailLogged(boolean detailLogged) {
		this.detailLogged = detailLogged;
	}

	// public void setParameterAndReturnValue(Object[] params, Object retValue)
	// {
	// this.parameters = params;
	// this.returnValue = retValue;
	// this.fleshed = true;
	// this.detailLogged = true;
	// }

	/**
	 * whether parameters and the return value is fully fleshed. parameters and
	 * the return value can be configured to be recorded into DB selectively,
	 * when user clicks on a node in the operation tree, it will load parameters
	 * and the return value from DB if they are recorded
	 */
	public boolean isFleshed() {
		return fleshed;
	}

	public void setFleshed(boolean fleshed) {
		this.fleshed = fleshed;
	}

	public void setParameterName(String[] paraNames) {
		this.parametersNames = paraNames;
	}

	public long getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(long executionTime) {
		this.executionTime = executionTime;
	}

	public void addExecutionTime(long executionTime) {
		this.executionTime += executionTime;
	}

	public void addAnalyzeResult(String analyzer, AnalyzeResult result) {
		this.analyzeResults.put(analyzer, result);
	}

	public Map<String, AnalyzeResult> getAnalyzeResults() {
		return analyzeResults;
	}

	public void setAnalyzeReport(String analyzeReport) {
		this.analyzeReport = analyzeReport;
	}

	public String getAnalyzeReport() {
		if (this.analyzeReport != null) {
			return this.analyzeReport;
		}

		StringBuffer sb = new StringBuffer();
		Set<String> analyzers = this.getAnalyzeResults().keySet();
		for (String a : analyzers) {
			AnalyzeResult result = this.getAnalyzeResults().get(a);
			sb.append(result.report());
			sb.append(",");
		}
		if (analyzers.size() > 0) {
			sb.deleteCharAt(sb.length() - 1); // delete the last ","
		}

		this.analyzeReport = sb.toString();
		return this.analyzeReport;
	}

	public boolean isMemoryRisk() {
		return memoryRisk;
	}

	public void setMemoryRisk(boolean memoryRisk) {
		this.memoryRisk = memoryRisk;
	}

	public int getObjectCount() {
		return objectCount;
	}

	public void setObjectCount(int objectCount) {
		this.objectCount = objectCount;
	}

	public boolean isHealthy() {
		return this.analyzeResults.size() == 0 && StringUtils.isBlank(this.analyzeReport);
	}

	@Override
	public String toString() {
		return this.signature;
	}

	public String toLongString() {
		StringBuffer sb = new StringBuffer();
		NDC.push("");
		toString(sb, this);
		return sb.toString();
	}

	private void toString(StringBuffer sb, OperationFlow ops) {
		String leadingSpaces = NDC.get();
		sb.append(leadingSpaces);
		sb.append(ops.signature + "(id:" + ops.id + " time:" + ops.executionTime + ")");

		sb.append("[Return: ");
		sb.append(getReturnValueToString());
		sb.append("]");

		sb.append("[Args: ");
		sb.append(getParametersToString());
		sb.append("]");
		sb.append("\n");

		NDC.push("	");
		for (OperationFlow outOp : ops.outOps) {
			outOp.toString(sb, outOp);
		}
		NDC.pop();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof OperationFlow)) {
			return false;
		}

		OperationFlow that = (OperationFlow) obj;
		if (this.id != that.id) {
			return false;
		}

		if (this.outOps.size() != that.outOps.size()) {
			return false;
		}

		for (int i = 0, n = this.outOps.size(); i < n; i++) {
			OperationFlow op1 = this.outOps.get(i);
			OperationFlow op2 = that.outOps.get(i);
			if (!(op1.equals(op2))) {
				return false;
			}
		}
		return true;

	}

	@Override
	public int hashCode() {
		return (int) this.getId();

	}

	// help GC destroy itself
	public void suicideBomb() {
		this.returnValue = null;
		this.parameters = null;
		this.parametersNames = null;
		this.callingOp = null;
		this.parametersToString = null;
		this.returnValueToString = null;

		for (OperationFlow op : this.outOps) {
			op.suicideBomb();
		}

		this.outOps.clear();
	}

	public String getCallingHierarchy() {
		Stack<OperationFlow> ch = new Stack<OperationFlow>();
		ch.push(this);
		OperationFlow parent = this.getCallingOp();
		while (parent != null) {
			ch.push(parent);
			parent = parent.getCallingOp();
		}

		StringBuffer sb = new StringBuffer();
		String ws = "--";
		int level = 0;
		while (!ch.empty()) {
			OperationFlow op = ch.pop();
			sb.append(StringUtils.repeat(ws, level++) + op.getSignature() + "(id:" + op.getId()
					+ ")" + "\n");
		}

		return sb.toString();

	}

	public void setSelected(boolean b) {
		this.selected = true;
	}

	public boolean isSelected() {
		return selected;
	}

}
