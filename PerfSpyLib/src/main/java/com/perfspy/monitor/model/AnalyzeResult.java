package com.perfspy.monitor.model;

public interface AnalyzeResult {
	public String report();
}
