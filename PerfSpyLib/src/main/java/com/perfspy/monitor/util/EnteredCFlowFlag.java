package com.perfspy.monitor.util;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.perfspy.monitor.analyze.Analyzer;
import com.perfspy.monitor.analyze.PerfSpyAnalyzer;
import com.perfspy.monitor.config.PerfSpyConfig;
import com.perfspy.monitor.dao.DAOExecutor;
import com.perfspy.monitor.dao.OperationFlowDAO;

/**
 * originally, this class was embedded into PerformanceMontitoringAspect, but
 * then when running the application with aspectjt 1.6.2 and aspectjweaver 1.6.2
 * (PerfSpy is also compiled with 1.6.2), it throws out this exception:
 * 
 * java.lang.RuntimeException: Problem processing attributes in
 * N:\aspectj169-dev\A.class Caused by: org.aspectj.weaver.BCException: Unable
 * to continue, this version of AspectJ supports classes built with weaver
 * version 6.0 but the class A is version 7.0
 * 
 * reasons unknown!
 */
public class EnteredCFlowFlag extends ThreadLocal<EnteredCFlow> {
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd-HH:mm:ss:SSS");

	private static final EnteredCFlowFlag holder = new EnteredCFlowFlag();

	@Override
	protected EnteredCFlow initialValue() {
		return new EnteredCFlow();
	}

	public static boolean isEntered() {
		return holder.get().entered;
	}

	public static String getCflowTarget() {
		return holder.get().cflowTarget;
	}

	public static Object getAspectInstance() {
		return holder.get().aspectInstance;
	}

	public static long getId() {
		return holder.get().id++;
	}

	public static void setOperationFlowDao(OperationFlowDAO dao) {
		// System.out.println("keep " + dao + " for " +
		// Thread.currentThread().getName());
		holder.get().dao = dao;
	}

	public static OperationFlowDAO getOperationFlowDao() {
		return holder.get().dao;
	}

	public static void setPerfSpyConfig(PerfSpyConfig config) {
		holder.get().perfSpyConfig = config;
	}

	public static PerfSpyConfig getPerfSpyConfig() {
		return holder.get().perfSpyConfig;
	}

	public static void setSpyEventId(long spyEventId) {
		holder.get().spyEventId = spyEventId;
	}

	public static String getSpyEvent() {
		return holder.get().spyEvent;
	}

	public static long getSpyEventId() {
		return holder.get().spyEventId;
	}

	public static long getStartTime() {
		return holder.get().startTime;
	}

	public static void enterCFlow(String cflowTarget, Object aspectInstance) {
		// System.out.println("starting to keep stuff for thread " +
		// Thread.currentThread().getName());
		EnteredCFlow enteredCFlow0 = holder.get();
		enteredCFlow0.entered = true;
		enteredCFlow0.aspectInstance = aspectInstance;
		enteredCFlow0.id = 1;
		enteredCFlow0.dao = null;
		enteredCFlow0.cflowTarget = cflowTarget;
		enteredCFlow0.spyEvent = getShortCflowTarget(cflowTarget) + "-"
				+ simpleDateFormat.format(new Date()) + "-" + new Random().nextInt(100);
		enteredCFlow0.startTime = System.currentTimeMillis();

		List<PerfSpyAnalyzer> analyzers = enteredCFlow0.perfSpyConfig.getAnalyzers(cflowTarget);
		Analyzer analyzer = new Analyzer();
		analyzer.setAnalyzers(analyzers);
		enteredCFlow0.analyzer = analyzer;
	}

	private static String getShortCflowTarget(String cflowTarget) {
		int lastIndex = cflowTarget.lastIndexOf(".");
		return cflowTarget.substring(lastIndex + 1);
	}

	public static Analyzer getAnalyzer() {
		EnteredCFlow enteredCFlow0 = holder.get();
		return enteredCFlow0.analyzer;
	}

	public static void enterSkip() {
		EnteredCFlow enteredCFlow0 = holder.get();
		enteredCFlow0.enteredSkip = true;
	}

	//TODO: there is a big bug here: how to deal with connection leak, thread leak in DAOExecutor? 
	public static void encounterFatalError() {
		EnteredCFlow enteredCFlow0 = holder.get();
		enteredCFlow0.fatalError = true;
		enteredCFlow0.dao.commitTask(true);
		enteredCFlow0.daoExecutor.endOfTask();
	}

	public static void setDAOExecutor(DAOExecutor executor) {
		EnteredCFlow enteredCFlow0 = holder.get();
		enteredCFlow0.daoExecutor = executor;
	}

	public static DAOExecutor getDAOExecutor() {
		EnteredCFlow enteredCFlow0 = holder.get();
		return enteredCFlow0.daoExecutor;
	}

	public static boolean isFatallyWounded() {
		EnteredCFlow enteredCFlow0 = holder.get();
		return enteredCFlow0.fatalError;
	}

	public static void exitSkip() {
		EnteredCFlow enteredCFlow0 = holder.get();
		enteredCFlow0.enteredSkip = false;

	}

	public static Connection getConnection() {
		EnteredCFlow enteredCFlow0 = holder.get();
		return enteredCFlow0.con;
	}

	public static void setConnection(Connection con) {
		EnteredCFlow enteredCFlow0 = holder.get();
		enteredCFlow0.con = con;
	}

	public static boolean isSkip() {
		EnteredCFlow enteredCFlow0 = holder.get();
		return enteredCFlow0.enteredSkip;
	}

	public static void exitCFlow() {
		EnteredCFlow enteredCFlow0 = holder.get();
		enteredCFlow0.entered = false;
		enteredCFlow0.cflowTarget = "";
		enteredCFlow0.aspectInstance = null;
		enteredCFlow0.spyEvent = "";
		enteredCFlow0.spyEventId = 0;
		enteredCFlow0.id = 1;
		enteredCFlow0.con = null;
		enteredCFlow0.dao = null;
		enteredCFlow0.startTime = 0;
		enteredCFlow0.enteredSkip = false;
		enteredCFlow0.fatalError = false;
		enteredCFlow0.analyzer = null;
		enteredCFlow0.daoExecutor = null;
		enteredCFlow0.perfSpyConfig = null;

	}

}

class EnteredCFlow {
	boolean entered = false;

	String cflowTarget = "";

	//the aspect instance capturing the cflow, store it as an object, 
	//so this project PerfSpyLib doesn't not have to have a dependency on AspectJ
	Object aspectInstance = null;

	long id = 1;

	OperationFlowDAO dao;
	Connection con;
	DAOExecutor daoExecutor;

	String spyEvent;

	long spyEventId;

	long startTime; // in million seconds

	boolean enteredSkip = false;
	boolean fatalError = false;
	Analyzer analyzer;

	PerfSpyConfig perfSpyConfig;

}
