package com.perfspy.monitor.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TypeUtil {

	private static final Set<Class> _prims = new HashSet<Class>();
	private static final Set<String> _primNames = new HashSet<String>();

	static {
		_prims.add(Byte.class);
		_prims.add(Integer.class);
		_prims.add(Long.class);
		_prims.add(Double.class);
		_prims.add(Character.class);
		_prims.add(Float.class);
		_prims.add(Boolean.class);
		_prims.add(Short.class);
		_prims.add(String.class);
		_prims.add(Class.class);

		_primNames.add("byte");
		_primNames.add("int");
		_primNames.add("long");
		_primNames.add("double");
		_primNames.add("char");
		_primNames.add("float");
		_primNames.add("boolean");
		_primNames.add("short");

		_primNames.add(Byte.class.getName());
		_primNames.add(Integer.class.getName());
		_primNames.add(Long.class.getName());
		_primNames.add(Double.class.getName());
		_primNames.add(Character.class.getName());
		_primNames.add(Float.class.getName());
		_primNames.add(Boolean.class.getName());
		_primNames.add(Short.class.getName());
		_primNames.add(String.class.getName());
		_primNames.add(Class.class.getName());

	}

	public static boolean isCollection(Object value) {
		if (value == null) {
			return false;
		}
		return value instanceof Collection || value instanceof Map;
	}

	public static boolean isEmptyCollection(Object value) {
		if (value instanceof Collection) {
			return ((Collection) value).size() == 0;
		}

		if (value instanceof Map) {
			return ((Map) value).size() == 0;
		}

		return false;
	}

	public static boolean isEmptyArray(Object value) {
		if (isArray(value)) {
			int length = Array.getLength(value);
			return length == 0;
		}
		return false;
	}

	public static boolean isArray(Object value) {
		if (value == null) {
			return false;
		}
		return value.getClass().isArray();
	}

	public static boolean isPrimitive(Class c) {
		return c.isPrimitive() || _prims.contains(c);
	}

	public static boolean isPrimitive(Object value) {
		if (value == null) {
			return true;
		}
		return isPrimitive(value.getClass());
	}

}
