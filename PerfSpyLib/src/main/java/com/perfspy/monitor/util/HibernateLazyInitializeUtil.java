package com.perfspy.monitor.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * handle Hibernate lazy initialized objects. Use reflection to detect if an
 * object is a Hibernate lazy initialized one, so no need to pack in
 * Hibernate.jar and its dependent jars. Hibernate will log
 * LazyInitializationException stacktrace, this is because
 * LazyInitializationException() constructor uses
 * LogFactory.getLog(LazyInitializationException.class).error(msg, this); to log
 * the error, so annoying! Hibernate should just throw the error and have
 * applications to handle it!
 * 
 */
public class HibernateLazyInitializeUtil {
	private static final Log logger = LogFactory.getLog(HibernateLazyInitializeUtil.class);

	public static boolean isHibernateLazyInitialized(Object obj) {
		if (obj == null) {
			return false;
		}
		return isHibernateLazyEnhanceByCGLIBObject(obj) || isHibernateUnintializedCollection(obj);
	}

	private static boolean isHibernateLazyEnhanceByCGLIBObject(Object obj) {
		Class c = obj.getClass();
		return isHibernateLazyEnhanceByCGLib(c.getName());
	}

	public static boolean isHibernateLazyEnhanceByCGLib(String clzName) {
		if (clzName.contains("EnhancerByCGLIB")) {
			return true;
		}
		return false;
	}

	private static boolean isHibernateUnintializedCollection(Object col) {

		if (!isHibernateCollectionType(col)) {
			return false;
		}
		boolean wasInitialized = (Boolean) ReflectionUtil.getFieldValue(col, "initialized");
		return !wasInitialized;
	}

	private static boolean isHibernateCollectionType(Object col) {
		if (col.getClass().getName().startsWith("org.hibernate.collection")) {
			return true;
		}
		return false;
	}

}
