package com.perfspy.monitor.util;

import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.cedarsoftware.util.SafeSimpleDateFormat;
import com.cedarsoftware.util.hibernate.HibernateUtil;
import com.cedarsoftware.util.io.JsonConstant;
import com.cedarsoftware.util.io.JsonObject;
import com.cedarsoftware.util.io.JsonReader.DateReader;
import com.cedarsoftware.util.io.JsonReader.LocaleReader;
import com.cedarsoftware.util.io.JsonReader.SqlDateReader;
import com.cedarsoftware.util.io.JsonReader.TimestampReader;

public class ToStringUtil {

	private static int collectionLimit = 5;
	private static final Log logger = LogFactory.getLog(ToStringUtil.class);
	private static SafeSimpleDateFormat dateFormat = new SafeSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	public static void setLimit(int limit) {
		ToStringUtil.collectionLimit = limit;
	}

	public static String toString(Object obj) {
		if (TypeUtil.isArray(obj)) {
			return getArrayDescription(obj);
		} else if (TypeUtil.isCollection(obj)) {
			return ToStringUtil.getCollectionDescription(obj);
		} else {
			return ToStringUtil.getObjectDescription(obj);
		}
	}

	// private static boolean isPrimitiveWrapper(String type) {
	// return _primNames.contains(type);
	// }

	static String getArrayDescription(Object value) {
		Class clz = value.getClass();
		boolean prim = false;
		if (TypeUtil.isPrimitive(clz)) {
			prim = true;
		}
		String clzName = clz.getName();
		// "[Ljava.lang.String;" will be "String"
		clzName = getSimpleClassName(clzName);
		int length = Array.getLength(value);
		StringBuffer ret = new StringBuffer(clzName.substring(0, clzName.length() - 1) + length
				+ "]");
		ret.append("@" + getSystemHashCode(value));

		if (length > 0) {
			ret.append(":");
			int n = length < collectionLimit ? length : collectionLimit;
			for (int i = 0; i < n; i++) {
				ret.append(toString(Array.get(value, i)));
				ret.append(",");
			}
		}
		return ret.toString();
	}

	private static String getMapDescription(Map map) {
		try {
			int size;
			if (HibernateUtil.isHibernateUnintializedCollection(map)) {
				size = 0;
			} else {
				size = map.size();
			}
			String clzName = map.getClass().getName();
			clzName = getSimpleClassName(clzName);
			if (size == 0) {
				return clzName + "[0]";
			}
			StringBuffer ret = new StringBuffer(clzName + "@" + getSystemHashCode(map) + "[" + size
					+ "]");

			ret.append(":");
			int n = size < collectionLimit ? size : collectionLimit;
			int i = 0;

			Set keys = map.keySet();
			for (Iterator it = keys.iterator(); it.hasNext();) {
				Object key = (Object) it.next();
				ret.append(getObjectDescription(key) + "->");

				Object value = map.get(key);

				ret.append(getObjectDescription(value));

				ret.append(",");

				i++;
				if (i == n) {
					break;
				}
			}

			return ret.toString();
		} catch (Exception e) {
			return getSystemHashCode(map);
		}
	}

	public static String getObjectDescription(Object obj) {
		if (obj == null) {
			return "null";
		}
		if (TypeUtil.isPrimitive(obj)) {
			return obj.toString();
		}

		if (obj instanceof Date || obj instanceof java.sql.Date || obj instanceof Timestamp) {
			return dateFormat.format(obj);
		}

		if (TypeUtil.isEmptyCollection(obj) || TypeUtil.isEmptyArray(obj)) {
			return getSimpleClassNameForObject(obj) + "@" + getSystemHashCode(obj) + "(empty)";
		}
		return getSimpleClassNameForObject(obj) + "@" + getSystemHashCode(obj);
	}

	public static String getJsonObjectDescription(JsonObject jsonObj) {

		String type = jsonObj.getType();

		if (jsonObj.containsKey(JsonConstant.JSON_VALUE)) {
			if (StringUtils.isNotBlank(type)) {
				if (type.equals("date")) { // see JSonWriter.getType()
					try {
						Object date = new DateReader().read(jsonObj, null);
						return dateFormat.format(date);
					} catch (IOException e) {
						logger.debug("failed to read a timestamp object from:" + jsonObj, e);
					}
				} else if (type.equals(java.sql.Date.class.getName())) {
					try {
						Object date = new SqlDateReader().read(jsonObj, null);
						return dateFormat.format(date);
					} catch (IOException e) {
						logger.debug("failed to read a timestamp object from:" + jsonObj, e);
					}
				}
			}
			return ToStringUtil.getObjectDescription(jsonObj.get(JsonConstant.JSON_VALUE));
		} else if (jsonObj.isArray() || jsonObj.isCollectionAssumption()) {
			if (jsonObj.getArray() == null) {
				return type + "(null)";
			} else if (jsonObj.getArray().length == 0) {
				return getSimpleClassName(type.toString()) + "@" + jsonObj.getSystemHashcode()
						+ "(empty)";
			}
			// return ToStringUtil.getObjectDescription(jsonObj.getArray());
		} else if (jsonObj.isMapAssumption()) {
			Object[] keys = (Object[]) jsonObj.get(JsonConstant.JSON_KEYS);
			if (keys == null) {
				return type + "(null)";
			} else if (keys.length == 0) {
				return getSimpleClassName(type.toString()) + "@" + jsonObj.getSystemHashcode()
						+ "(empty)";
			}
		} else if (type.equals(Timestamp.class.getName())) {
			try {
				Object ts = new TimestampReader().read(jsonObj, null);
				return dateFormat.format(ts);
			} catch (IOException e) {
				logger.debug("failed to read a timestamp object from:" + jsonObj, e);
			}
		} else if (type.equals("java.util.Locale")) {
			try {
				Object locale = new LocaleReader().read(jsonObj, null);
				return locale.toString();
			} catch (IOException e) {
				logger.debug("failed to read a timestamp object from:" + jsonObj, e);
			}

		}
		return getSimpleClassName(type.toString()) + "@" + jsonObj.getSystemHashcode();
	}

	private static String getSystemHashCode(Object obj) {
		return Integer.toHexString(System.identityHashCode(obj));
	}

	static String getCollectionDescription(Object value) {
		if (value instanceof Map) {
			return getMapDescription((Map) value);
		}

		try {
			Collection col = (Collection) value;
			int size;
			if (HibernateUtil.isHibernateUnintializedCollection(col)) {
				size = 0;
			} else {
				size = col.size();
			}
			String clzName = value.getClass().getName();
			clzName = getSimpleClassName(clzName);
			if (size == 0) {
				return clzName + "[0]";
			}

			StringBuffer ret = new StringBuffer(clzName + "@" + getSystemHashCode(value) + "["
					+ size + "]");

			ret.append(":");
			int n = size < collectionLimit ? size : collectionLimit;
			int i = 0;
			for (Iterator it = col.iterator(); it.hasNext();) {
				Object obj = (Object) it.next();
				ret.append(getObjectDescription(obj));
				ret.append(",");
				i++;
				if (i == n) {
					break;
				}
			}

			return ret.toString();
		} catch (Exception e) {
			return getSimpleClassNameForObject(value) + "@" + getSystemHashCode(value);
		}
	}

	private static String getSimpleClassNameForObject(Object obj) {
		return getSimpleClassName(obj.getClass().getName());
	}

	private static String getSimpleClassName(String clzName) {
		boolean isArray = false;
		// Array class name starts with [L and ends with ,
		if (clzName.startsWith("[L")) {
			isArray = true;
		}
		clzName = clzName.substring(clzName.lastIndexOf('.') + 1, clzName.length());

		return isArray ? clzName.substring(0, clzName.length() - 1) + "[]" : clzName;
	}

}
