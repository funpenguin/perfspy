import java.util.Random;

import junit.framework.TestCase;

import com.perfspy.monitor.exception.FatalException;

public class Test extends TestCase {
	public void testException() {
		try {
			int a;
			// throw new FatalException("abcd", null);
			throw new RuntimeException("abcd");
		} catch (FatalException e) {
			System.out.println("fatal exception");
		} catch (Throwable e) {
			System.out.println("other exception");
		}

	}

	public void testRandomInt() {

		for (int i = 0; i < 100; i++) {
			System.out.println(new Random().nextInt(100));
		}

	}
}
