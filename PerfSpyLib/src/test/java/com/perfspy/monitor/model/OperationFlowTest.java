package com.perfspy.monitor.model;

import junit.framework.TestCase;

public class OperationFlowTest extends TestCase {

	private OperationFlow op2;
	private OperationFlow op2_31;
	private OperationFlow op2_31_1;
	private OperationFlow op2_31_2;
	private OperationFlow op2_32;
	private OperationFlow op1;

	public void testGetCallingHierarchy() {
		createOps();

		String callingHierarchy = op2_31.getCallingHierarchy();
		System.out.println(callingHierarchy);
		StringBuffer expect = new StringBuffer("");
		expect.append("DUMMY(id:0)\n");
		expect.append("--ClassA.op1(id:1)\n");
		expect.append("----op2(id:2)\n");
		expect.append("------op3(id:3)\n");
		assertEquals(expect.toString(), callingHierarchy);

		String shortClassName = op1.getShortClassName();
		System.out.println(shortClassName);
		assertEquals("ClassA", shortClassName);
	}

	private OperationFlow createOps() {
		long id = 1;
		OperationFlow op0 = OperationFlow.DUMMY();

		op1 = new OperationFlow("ClassA.op1");
		op1.setId(id++);
		op0.addOutOp(op1);

		op2 = new OperationFlow("op2");
		op2.setId(id++);
		op1.addOutOp(op2);

		op2_31 = new OperationFlow("op3");
		op2_31.setExecutionTime(1);
		op2_31.setId(id++);
		op2.addOutOp(op2_31);

		op2_31_1 = new OperationFlow("op3");
		op2_31_1.setExecutionTime(1);
		op2_31_1.setId(id++);
		op2_31.addOutOp(op2_31_1);

		op2_31_2 = new OperationFlow("op3");
		op2_31_2.setExecutionTime(1);
		op2_31_2.setId(id++);
		op2_31.addOutOp(op2_31_2);

		op2_32 = new OperationFlow("op3");
		op2_32.setExecutionTime(1);
		op2_32.setId(id++);
		op2.addOutOp(op2_32);

		OperationFlow op2_32_1 = new OperationFlow("op3");
		op2_32_1.setExecutionTime(1);
		op2_32_1.setId(id++);
		op2_32.addOutOp(op2_32_1);

		OperationFlow op2_32_2 = new OperationFlow("op3");
		op2_32_2.setExecutionTime(1);
		op2_32_2.setId(id++);
		op2_32.addOutOp(op2_32_2);

		OperationFlow op1_31 = new OperationFlow("op3");
		op1_31.setExecutionTime(1);
		op1_31.setId(id++);
		op1.addOutOp(op1_31);

		OperationFlow op1_32 = new OperationFlow("op3");
		op1_32.setExecutionTime(1);
		op1_32.setId(id++);
		op1.addOutOp(op1_32);

		System.out.println("created an op:");
		System.out.println(op0.toLongString());
		// --op0 (id 0 DUMMY)
		// + --op1 (id 1)
		// +++ --op2 (id 2)
		// +++++ --op2_31 (id 3)
		// +++++++ --op2_31_1 (id 4)
		// +++++++ --op2_31_2 (id 5)
		// +++++ --op2_32 (id 6)
		// +++++++ --op2_32_1 (id 7)
		// +++++++ --op2_32_2 (id 8)
		// +++ --op1_31 (id 9)
		// +++ --op1_32 (id 10)
		/**
		 * op3 is invoked duplicately 4 times (not 8 times), duplicated ids are
		 * 3, 6, 9, 10
		 */
		return op0;

	}
}
