package com.perfspy.monitor.config;

import java.sql.Connection;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.lang3.ArrayUtils;

import com.perfspy.monitor.analyze.DuplicationDBAnalyzer;
import com.perfspy.monitor.analyze.PerfSpyAnalyzer;
import com.perfspy.monitor.analyze.SlowAnalyzer;
import org.junit.Ignore;

public class PerfSpyConfigTest extends TestCase {

	@Ignore("Ignore this test as it needs to access an Oracle db")
	public void atestConfig() throws Exception {
		PerfSpyConfig config = PerfSpyConfig.config(this.getClass(), "PerfSpyConfigTest.xml");

		assertTrue(config.getPersistTo("EditStaffingProfileAction").equals("DB"));
		assertTrue(config.getPersistTo("OpenProjectAction").equals("DB"));
		assertTrue(config.isTargetEnabled("AnalyzeAssignmentLoadPortletAction") == false);
		assertTrue(config.isTargetEnabled("OpenProjectAction") == true);

		Set<String> skipOperations = config.getDefaultSkipOperations();
		System.out.println(skipOperations);
		assertTrue(skipOperations.contains("com.mercury.itg.core.persistency.HibernatedObject"));
		assertTrue(skipOperations.contains("com.kintana.core.util.LocaleUtil"));
		assertTrue(skipOperations.contains("com.kintana.core.util.CodeString"));

		assertTrue(config.isToSkip("OpenProjectAction", "com.mercury.itg.pm.pmo.ProjectTypeImpl"));
		assertTrue(config.isToSkip("OpenProjectAction", "com.mercury.itg.AnalyzeAssignmentLoad"));

		/* <detailOps parts="param,return">
			<detailOp parts="param,return,this">com.kintana.core.cache.Cache.get</detailOp> <!-- TODO: support regular expression -->
			<detailOp>com.kintana.core.cache.Cache.put</detailOp>
			<detailOp parts="all">com.kintana.core.cache.Cache.clone</detailOp>
			</detailOps>
		*/

		String[] logDetails = config.logDetails("AnalyzeAssignmentLoadPortletAction",
				"com.kintana.core.cache.Cache.get");
		assertEquals(2, logDetails.length);
		assertTrue(ArrayUtils.contains(logDetails, PerfSpyConfig.DETAIL_PARAM));
		assertTrue(ArrayUtils.contains(logDetails, PerfSpyConfig.DETAIL_RETURN));

		logDetails = config.logDetails("com.mercury.dashboard.arch.host.DashboardServlet",
				"com.kintana.core.cache.Cache.get");
		assertEquals(2, logDetails.length);
		assertTrue(ArrayUtils.contains(logDetails, PerfSpyConfig.DETAIL_PARAM));
		assertTrue(ArrayUtils.contains(logDetails, PerfSpyConfig.DETAIL_RETURN));

		logDetails = config.logDetails("AnalyzeAssignmentLoadPortletAction",
				"com.kintana.core.cache.Cache.put");
		assertEquals(2, logDetails.length);
		assertTrue(ArrayUtils.contains(logDetails, PerfSpyConfig.DETAIL_PARAM));
		assertTrue(ArrayUtils.contains(logDetails, PerfSpyConfig.DETAIL_RETURN));

		logDetails = config.logDetails("AnalyzeAssignmentLoadPortletAction",
				"com.kintana.core.cache.Cache.clone");
		assertEquals(2, logDetails.length);
		assertTrue(ArrayUtils.contains(logDetails, PerfSpyConfig.DETAIL_PARAM));
		assertTrue(ArrayUtils.contains(logDetails, PerfSpyConfig.DETAIL_RETURN));

		logDetails = config.logDetails("AnalyzeAssignmentLoadPortletAction",
				"com.boring.hopeless.hp");
		assertTrue(logDetails.length == 0);

		/*	<entry target="EditStaffingProfileAction" persist="DB">
				<detailOps all="true" parts="all"/>
			</entry>
		 */
		logDetails = config.logDetails("EditStaffingProfileAction",
				"com.kintana.core.cache.Cache.get");
		assertEquals(2, logDetails.length);
		assertTrue(ArrayUtils.contains(logDetails, PerfSpyConfig.DETAIL_PARAM));
		assertTrue(ArrayUtils.contains(logDetails, PerfSpyConfig.DETAIL_RETURN));

		Connection con1 = config.getConnection("EditStaffingProfileAction");
		Connection con2 = config.getConnection("OpenProjectAction");
		boolean b1 = con1.getAutoCommit();
		boolean b2 = con2.getAutoCommit();
		assertEquals(b1, b2);

		List<PerfSpyAnalyzer> analyzers = config.getAnalyzers("OpenProjectAction");
		assertTrue(analyzers.size() == 2);
		assertTrue(analyzers.get(0) instanceof SlowAnalyzer);
		assertTrue(analyzers.get(1) instanceof DuplicationDBAnalyzer);

		/**
		 * 	<logToDb>
				<queuesize simple="40000" detail="2000"/>
				<batchsize simple="20000" detail="500"/>
		</logToDb>
		 */

		assertEquals(20000, config.getSimpleBatchSize());
		assertEquals(40000, config.getSimpleQueueSize());
		assertEquals(2000, config.getDetailQueueSize());
		assertEquals(500, config.getDetailBatchSize());
		assertTrue(config.getJsonIOConfig().getGoNoFurtherLevel() == 5);
	}

	public void testInterfaceTarget() throws Exception {
		/*
		 *   <entry target="com.perfspy.monitor.config.AInterface+" enabled="true">		
		 *	</entry>
		 */
		PerfSpyConfig config = PerfSpyConfig.config(this.getClass(), "PerfSpyConfigTest.xml");
		boolean targetEnabled = config.isTargetEnabled("com.perfspy.monitor.config.AClass");
		System.out.println(targetEnabled);
		assertTrue(targetEnabled);

	}

	public void testPackageTarget() throws Exception {
		/*
			 <entry target="com.perfspy.monitor.config.." enabled="true">		
			 </entry>
		 */
		PerfSpyConfig config = PerfSpyConfig.config(this.getClass(), "PerfSpyConfigTest.xml");
		boolean targetEnabled = config.isTargetEnabled("com.perfspy.monitor.config.BClass");
		System.out.println(targetEnabled);
		assertTrue(targetEnabled);

		//the second run should get the result from entryTargets, step debug to confirm
		targetEnabled = config.isTargetEnabled("com.perfspy.monitor.config.BClass");
		System.out.println(targetEnabled);
		assertTrue(targetEnabled);
	}

	public void testGetSigWithoutModifierParameter() {
		String longSig = "public    void   com.hp.a.b.c.ClassA.method1(String s1, String s2)";
		String str = PerfSpyConfig.getSigWithoutModifierParameter(longSig);
		System.out.println(str);
		assertEquals("com.hp.a.b.c.ClassA.method1", str);
	}

	public void testConfig2() throws Exception {
		PerfSpyConfig config = PerfSpyConfig.config(this.getClass(), "PerfSpyConfigTest2.xml");

		boolean toSkip = config.isToSkip("a", "b");
		assertEquals(toSkip, false);

		toSkip = config.isToSkip("com.hp.ov.sm.server.appserver.servlet.Servlet",
				"com.hp.ov.sm.common.core.JLog.abc");
		assertEquals(toSkip, true);
	}
}
