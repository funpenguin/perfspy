package com.perfspy.monitor.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import junit.framework.TestCase;

import com.perfspy.monitor.model.DuplicationSummary;
import com.perfspy.monitor.model.OperationFlow;

public class OperationFlowDaoTest extends TestCase {

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd-HH-mm-ss");

	public void testWriteAndRead() throws Exception {
		OperationFlow op0 = createOps();
		String spyEvent = writeToDb(op0);
		OperationFlow op1 = readOpTreeByEventName(spyEvent);

		assertTrue(op0.equals(op1));
	}

	private String writeToDb(OperationFlow op0) throws Exception {

		OperationFlowDAO dao = new OperationFlowDAO(getConnection());
		dao.setSimpleBatchSize(5);

		DAOExecutor executor = new DAOExecutor();
		dao.setExecutor(executor);
		String spyEvent = "testWriteToDb-" + simpleDateFormat.format(new Date());
		dao.startPersisting(spyEvent);
		persistTree(dao, op0);
		dao.lastOpPersisted();
		dao.finishedPersisting(System.currentTimeMillis(), new ArrayList<OperationFlow>());
		executor.waitToFinish();

		return spyEvent;
	}

	private OperationFlow readOpTreeByEventName(String spyEvent) throws Exception {
		OperationFlowDAO dao = new OperationFlowDAO(getConnection());
		long eventId = dao.getEventIdByName(spyEvent);
		OperationFlow op1 = dao.getOperationTreeByEventId(eventId);
		// "com.mercury.itg.rsc.staffing.web.EditStaffingProfileAction-13-01-04-13-12"
		System.out.println("read an op from db:");
		System.out.println(op1.toLongString());
		return op1;

	}

	private OperationFlow createOps() {
		long id = 1;
		OperationFlow op0 = OperationFlow.DUMMY();

		OperationFlow op1 = new OperationFlow("op1");
		op1.setId(id++);
		op0.addOutOp(op1);

		OperationFlow op2 = new OperationFlow("op2");
		op2.setId(id++);
		op1.addOutOp(op2);

		OperationFlow op2_31 = new OperationFlow("op3");
		op2_31.setExecutionTime(1);
		op2_31.setId(id++);
		op2.addOutOp(op2_31);

		OperationFlow op2_31_1 = new OperationFlow("op3");
		op2_31_1.setExecutionTime(1);
		op2_31_1.setId(id++);
		op2_31.addOutOp(op2_31_1);

		OperationFlow op2_31_2 = new OperationFlow("op3");
		op2_31_2.setExecutionTime(1);
		op2_31_2.setId(id++);
		op2_31.addOutOp(op2_31_2);

		OperationFlow op2_32 = new OperationFlow("op3");
		op2_32.setExecutionTime(1);
		op2_32.setId(id++);
		op2.addOutOp(op2_32);

		OperationFlow op2_32_1 = new OperationFlow("op3");
		op2_32_1.setExecutionTime(1);
		op2_32_1.setId(id++);
		op2_32.addOutOp(op2_32_1);

		OperationFlow op2_32_2 = new OperationFlow("op3");
		op2_32_2.setExecutionTime(1);
		op2_32_2.setId(id++);
		op2_32.addOutOp(op2_32_2);

		OperationFlow op1_31 = new OperationFlow("op3");
		op1_31.setExecutionTime(1);
		op1_31.setId(id++);
		op1.addOutOp(op1_31);

		OperationFlow op1_32 = new OperationFlow("op3");
		op1_32.setExecutionTime(1);
		op1_32.setId(id++);
		op1.addOutOp(op1_32);

		System.out.println("created an op:");
		System.out.println(op0.toLongString());
		// --op0 (id 0)
		// + --op1 (id 1)
		// +++ --op2 (id 2)
		// +++++ --op2_31 (id 3)
		// +++++++ --op2_31_1 (id 4)
		// +++++++ --op2_31_2 (id 5)
		// +++++ --op2_32 (id 6)
		// +++++++ --op2_32_1 (id 7)
		// +++++++ --op2_32_2 (id 8)
		// +++ --op1_31 (id 9)
		// +++ --op1_32 (id 10)
		/**
		 * op3 is invoked duplicately 4 times (not 8 times), duplicated ids are
		 * 3, 6, 9, 10
		 */
		return op0;

	}

	public void testGetDupOpSummeries() throws Exception {
		OperationFlow op0 = createOps();

		String spyEvent = writeToDb(op0);
		Connection connection = getConnection();
		OperationFlowDAO dao = new OperationFlowDAO(connection);
		long eventId = dao.getEventIdByName(spyEvent);
		Collection<DuplicationSummary> dupOpSummeries = dao.getDupOpSummeries(false, eventId);
		connection.close();

		assertTrue(dupOpSummeries.size() == 1);
		for (DuplicationSummary dup : dupOpSummeries) {
			assertTrue(dup.dupTimes == 4);
			assertTrue(dup.totalExecutionTime == 4);
		}

	}

	// public void testGetDupOpSummeriesById() throws Exception {
	// PerfSpyAnalyzer.setSlowThreshold(2);
	// OperationFlowDAO dao = new OperationFlowDAO(getConnection());
	// long eventId = 1103;
	// Collection<DuplicationSummary> dupOpSummeries = dao.getDupOpSummeries(
	// false, eventId);
	// dao.closeConnection();
	//
	// for (DuplicationSummary dup : dupOpSummeries) {
	// System.out.println(dup);
	// }
	// }

	public void testGetOpertionParentsById() throws Exception {
		OperationFlow op0 = createOps();
		String spyEvent = writeToDb(op0);
		OperationFlowDAO dao = new OperationFlowDAO(getConnection());
		long eventId = dao.getEventIdByName(spyEvent);

		OperationFlow[] op = dao.getOperationParentsByEventId(eventId, 3);
		if (op == null) {
			throw new Exception("unexpecated! should be able to find out parents for eventId:" + eventId
					+ " and operation id:" + 3);
		}
		System.out.println(op[0].toLongString());
		// DUMMY(id:0 time:0)[Return: ][Args: ]
		// +++++++++ op1(id:1 time:0)[Return: ][Args: ]
		// ++++++++++++++++++ op2(id:2 time:0)[Return: ][Args: ]
		// ++++++++++++++++++++++++++ op3(id:3 time:1)[Return: ][Args: ]
		assertTrue(op[0].getSignature().equals("DUMMY"));
		assertTrue(op[0].getFirstOutOp().getSignature().equals("op1"));
		assertTrue(op[0].getFirstOutOp().getFirstOutOp().getSignature().equals("op2"));
		assertTrue(op[0].getFirstOutOp().getFirstOutOp().getFirstOutOp().getSignature().equals("op3"));

		assertTrue(op[1].getSignature().equals("op3"));
	}

	public void testGetOperationWholeTreeById() throws Exception {
		OperationFlow op0 = createOps();
		String spyEvent = writeToDb(op0);
		OperationFlowDAO dao = new OperationFlowDAO(getConnection());
		long eventId = dao.getEventIdByName(spyEvent);

		OperationFlow op = dao.getOperationTreeByEventId(eventId, 3);
		if (op == null) {
			throw new Exception("unexpecated! should be able to find out parents for eventId:" + eventId
					+ " and operation id:" + 3);
		}
		System.out.println(op.toLongString());
		assertTrue(op.getSignature().equals("DUMMY"));
		assertTrue(op.getOutOps().size() == 1);
		// +++++++ DUMMY(id:0 time:0)[Return: ][Args: ]
		// ++++++++++++++ op1(id:1 time:0)[Return: ][Args: ]
		// +++++++++++++++++++++ op2(id:2 time:0)[Return: ][Args: ]
		// +++++++++++++++++++++ op3(id:3 time:1)[Return: ][Args: ]
		// ++++++++++++++++++++++++++++ op3(id:4 time:1)[Return: ][Args: ]
		// ++++++++++++++++++++++++++++ op3(id:5 time:1)[Return: ][Args: ]

	}

	//	public void testGetOperationDetail() throws Exception {
	//		OperationFlowDAO dao = new OperationFlowDAO(getConnection());
	//		OperationFlow op = dao.getOperationDetails(5, 1006);
	//
	//		Object[] params = op.getParameters();
	//		for (Object param : params) {
	//			System.out.println(param);
	//		}
	//
	//		Object ret = op.getReturnValue();
	//		System.out.println(ret);
	//	}

	private void persistTree(OperationFlowDAO dao, OperationFlow op) {
		dao.persistThis(op, new String[0]);
		List<OperationFlow> children = op.getOutOps();
		for (OperationFlow child : children) {
			persistTree(dao, child);
		}
	}

	private Connection getConnection() throws Exception {
		Class.forName("oracle.jdbc.driver.OracleDriver");

		Connection con = DriverManager.getConnection(jdbc_url, user, password);
		// "jdbc:oracle:thin:@192.168.1.10:1521:ORCL", "HR", "HR");

		con.setAutoCommit(false);

		return con;

	}

	private String jdbc_url = "jdbc:oracle:thin:@16.155.207.79:1521:ORCL"; // "jdbc:oracle:thin:@192.168.1.10:1521:ORCL";//
																			// "jdbc:oracle:thin:@16.155.207.79:1521:ORCL";//"jdbc:oracle:thin:@127.0.0.1:1521:ORCL"

	private String user = "PPM_CBA_914";// "PPM_CBA_914";// "HR";//

	private String password = "PPM_CBA_914";// "PPM_CBA_914";// "HR";//
}
