package com.perfspy.monitor.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

public class ToStringUtilTest extends TestCase {
	public void testArrayDescription() {
		ToStringUtil.setLimit(5);

		String[] ss = new String[6];
		ss[0] = "a";
		ss[1] = "b";
		ss[2] = "c";
		ss[3] = "d";
		ss[4] = "e";
		ss[5] = "f";

		String desc = ToStringUtil.getArrayDescription(ss);
		System.out.println(desc);
		assertTrue(desc.matches("String\\[6\\]@(\\d||\\w){8}:a,b,c,d,e,"));
		//		assertEquals("String[6]:a,b,c,d,e,", desc);
	}

	public void testCollectionDescription() {
		ToStringUtil.setLimit(5);

		List<String> ss = new ArrayList<String>();
		ss.add("a");
		ss.add("b");
		ss.add("c");
		ss.add("d");
		ss.add("e");
		ss.add("f");

		String desc = ToStringUtil.getCollectionDescription(ss);
		System.out.println(desc);
		assertTrue(desc.matches("ArrayList@(\\d||\\w){8}\\[6\\]:a,b,c,d,e,"));
		//		ArrayList@355d56d5[6]:a,b,c,d,e,

		Map<String, Integer> mm = new HashMap<String, Integer>();
		mm.put("a", 1);
		mm.put("b", 2);
		mm.put("c", 3);
		mm.put("d", 4);
		mm.put("e", 5);
		mm.put("f", 6);

		desc = ToStringUtil.getCollectionDescription(mm);
		System.out.println(desc);
		assertTrue(desc
				.matches("HashMap@(\\d||\\w){8}\\[6\\]:\\w->\\d,\\w->\\d,\\w->\\d,\\w->\\d,\\w->\\d,"));
		//		HashMap@ae94e92[6]:f->6,d->4,e->5,b->2,c->3,

	}

	public void testDateDescription() {
		System.out.println(ToStringUtil.getObjectDescription(new Date().getTime()));

	}
}
