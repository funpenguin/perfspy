package com.perfspy.monitor.util;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import com.google.common.collect.Lists;

public class ReflectionUtilTest extends TestCase {

	class A {
		private String method1(String a) {
			return a;
		}

		private String method2() {
			return "method2";
		}

		private String method3(int i) {
			return "method3_" + i;
		}
	}

	public void testInvokeMethod() {
		A a = new A();

		Map<Class, Object> params = new HashMap<Class, Object>();
		params.put(String.class, "abcd");
		Object value = ReflectionUtil.invokeMethod(a, "method1", params);
		System.out.println(value);
		assertTrue(value.equals("abcd"));

		value = ReflectionUtil.invokeMethod(a, "method2");
		System.out.println(value);
		assertTrue(value.equals(a.method2()));

		boolean errored = false;
		try {
			value = ReflectionUtil.invokeMethod(a, "method1");
		} catch (Exception e) {
			errored = true;
		}
		assertTrue(errored);

		params.clear();
		params.put(int.class, 5);
		value = ReflectionUtil.invokeMethod(a, "method3", params);
		System.out.println(value);
		assertTrue(value.equals(a.method3(5)));

	}

	static class B {
		private String bs = "bs";
	}

	static class D {
		private String ds = "ds";
	}

	static class A1 {
		protected String as1_1;
		private String as1_2;
		public String as1_3;
		public static final String as1_4_static = "as1_4";
		public static final B b_static = new B();

		private String getAs1_1() {
			return as1_1;
		}

		private void setAs1_1(String as1_1) {
			this.as1_1 = as1_1;
		}

		public String getAs1_2() {
			return as1_2;
		}

		public void setAs1_2(String as1_2) {
			this.as1_2 = as1_2;
		}
	}

	static class A2 extends A1 {
		private String as2_1 = "this is as2";
		public final D d_non_static = new D();

		public String getAs2_1() {
			return as2_1;
		}

		public void setAs2_1(String as2_1) {
			this.as2_1 = as2_1;
		}

	}

	public void testGetFields() throws IllegalArgumentException, IllegalAccessException {

		A2 a2 = new A2();

		a2.setAs1_2("as1_2 set by a2");
		a2.setAs2_1("as2_1");

		List<String> fieldNames = Lists.newArrayList();

		Field[] fields = ReflectionUtil.getFields(a2);
		for (Field field : fields) {
			System.out.println(field.getName() + ":");
			fieldNames.add(field.getName());
		}

		// Collections.binarySearch has to be invoked on a sorted list
		Collections.sort(fieldNames);

		int pos = Collections.binarySearch(fieldNames, "as1_1");
		System.out.println("find as1_1 at " + pos);
		assertTrue(pos >= 0);

		pos = Collections.binarySearch(fieldNames, "as1_3");
		System.out.println("find as1_3 at " + pos);
		assertTrue(pos >= 0);

		pos = Collections.binarySearch(fieldNames, "as2_1");
		System.out.println("find as2_1 at " + pos);
		assertTrue(pos >= 0);

		pos = Collections.binarySearch(fieldNames, "as1_4_static");
		System.out.println("find as1_4_static at " + pos);
		assertTrue(pos < 0);

		pos = Collections.binarySearch(fieldNames, "d_non_static");
		System.out.println("find d_non_static at " + pos);
		assertTrue(pos >= 0);

	}

}
