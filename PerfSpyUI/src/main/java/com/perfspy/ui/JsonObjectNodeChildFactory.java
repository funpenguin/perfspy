package com.perfspy.ui;

import java.util.List;

import org.apache.log4j.Logger;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

class JsonObjectNodeChildFactory extends ChildFactory<JsonObjectBean> {
	private JsonObjectBean jsonObjectBean;
	private static final Logger logger = Logger.getLogger(JsonObjectNodeChildFactory.class);

	public JsonObjectNodeChildFactory(JsonObjectBean jsonObjectBean) {
		this.jsonObjectBean = jsonObjectBean;
	}

	@Override
	protected Node createNodeForKey(JsonObjectBean key) {
		return new JsonObjectNode(key);
	}

	@Override
	protected boolean createKeys(List<JsonObjectBean> list) {

		list.addAll(jsonObjectBean.getChildJsonNodes());
		return true;
	}
}
