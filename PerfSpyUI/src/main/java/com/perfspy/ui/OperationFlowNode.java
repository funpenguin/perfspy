package com.perfspy.ui;

import java.awt.Image;

import javax.swing.Action;

import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ProxyLookup;

import com.perfspy.monitor.model.OperationFlow;
import com.perfspy.ui.tree.action.DispayHiddenOperations;
import com.perfspy.ui.tree.action.ExpandAction;
import com.perfspy.ui.tree.action.HideOperationAction;
import com.perfspy.ui.tree.action.MarkAncestorsAction;
import com.perfspy.ui.tree.action.MarkOperationAction;
import com.perfspy.ui.tree.action.ShowAllOperations;
import com.perfspy.ui.tree.action.ShowDetailAction;
import com.perfspy.ui.tree.action.ShowMarkedAction;
import com.perfspy.ui.tree.action.ShowUnhealthyOperationAction;

public final class OperationFlowNode extends EntityNode<OperationFlow> {

	private OperationFlowBean opBean;

	public static final String SIGNATURE_PROP = "signature";

	public static final String ANALYZE_PROP = "analyze report";

	public static final String EXECUTIONTIME_PROP = "execution time";

	public static final String RETURN_PROP = "return";

	public static final String PARAM_PROP = "parameters";

	public static final String Long_SIGNATURE_PROP = "long signature";

	public static final String OP_ID = "opertion id";
	public static final String Calling_Op = "calling operation";

	//	private boolean marked = false;

	public OperationFlowNode(OperationFlowBean opEntity) {
		this(opEntity, new InstanceContent());
	}

	private OperationFlowNode(OperationFlowBean opEntity, InstanceContent ic) {
		super(Children.create(new OperationFlowNodeChildFactory(opEntity), true), new ProxyLookup(
				opEntity.getLookup(), new AbstractLookup(ic)));

		this.opBean = opEntity;

	}

	@Override
	public String getDisplayName() {
		return opBean.getDisplay();
	}

	@Override
	public String getEntityDiscrption() {
		OperationFlow op = this.opBean.getOperation();
		return op.getSignature() + "(id:" + op.getId() + ")";
	}

	@Override
	public String getHtmlDisplayName() {
		if (this.isMarked()) {
			return "<font color='0000FF'>" + getDisplayName() + "</font>";
		}

		if (opBean.getOperation().isSelected()) {
			return "<font color='00FF00'>" + getDisplayName() + "</font>";
		}

		if (!opBean.getOperation().isHealthy()) {
			return "<font color='FF0000'>" + getDisplayName() + "</font>";
		}
		return opBean.getDisplay();

	}

	@Override
	public Image getIcon(int type) {
		return ImageUtilities.loadImage("query.png");
	}

	@Override
	public Image getOpenedIcon(int type) {
		return ImageUtilities.loadImage("open.png");
	}

	@Override
	public Action[] getActions(boolean context) {
		if (this.opBean.getOperation().isDummy()) {
			return new Action[] { new ShowAllOperations(), new ShowUnhealthyOperationAction(),
					new DispayHiddenOperations() };
		}
		OutlineView outlineView = PerfSpyAdmin.getPerfSpyAdmin().getOutlineView();
		ExplorerManager explorerManager = PerfSpyAdmin.getPerfSpyAdmin().getExplorerManager();

		return new Action[] {
				new HideOperationAction(this),
				MarkOperationAction.markAction.config(explorerManager, outlineView),
				MarkAncestorsAction.markAction.config(explorerManager, outlineView),
				new ShowMarkedAction(this, explorerManager, outlineView),
				// new HideOperationAction(this, true), new
				// ShowUnhealthyOperationAction(),
				// new ShowOnlyThisOperationAction(this),
				new ShowDetailAction(this),
				new ShowAllOperations(this, explorerManager, outlineView),
				new DispayHiddenOperations(), new ExpandAction(this, outlineView),
				new ExpandAction(this, true, outlineView) };
	}

	@Override
	protected Sheet createSheet() {
		Sheet sheet = Sheet.createDefault();
		Sheet.Set set = Sheet.createPropertiesSet();

		PropertySupport analyzeProp = new PropertySupport.ReadOnly<String>(ANALYZE_PROP,
				String.class, ANALYZE_PROP, "The execution time of the operation") {
			@Override
			public String getValue() {
				if (opBean == null) {
					return "";
				}
				return opBean.getOperation().getAnalyzeReport();

			}
		};
		set.put(analyzeProp);

		PropertySupport executionTimeProp = new PropertySupport.ReadOnly<String>(
				EXECUTIONTIME_PROP, String.class, EXECUTIONTIME_PROP,
				"The execution time of the operation") {
			@Override
			public String getValue() {
				if (opBean == null) {
					return 0 + "";
				}
				return opBean.getOperation().getExecutionTime() + "";

			}
		};
		set.put(executionTimeProp);

		PropertySupport returnProp = new PropertySupport.ReadOnly<String>(RETURN_PROP,
				String.class, RETURN_PROP, "The return value of the operation") {
			@Override
			public String getValue() {
				if (opBean == null) {
					return "";
				}
				return opBean.getOperation().getReturnValueToString();

			}
		};
		set.put(returnProp);

		PropertySupport paramProp = new PropertySupport.ReadOnly<String>(PARAM_PROP, String.class,
				PARAM_PROP, "The parameters of the operation") {
			@Override
			public String getValue() {
				if (opBean == null) {
					return "";
				}
				return opBean.getOperation().getParametersToString();

			}
		};
		set.put(paramProp);

		PropertySupport longSigProp = new PropertySupport.ReadOnly<String>(Long_SIGNATURE_PROP,
				String.class, Long_SIGNATURE_PROP, "The long siguration of the operation") {
			@Override
			public String getValue() {
				if (opBean == null) {
					return "";
				}
				return opBean.getOperation().getLongSignature();
			}
		};
		set.put(longSigProp);

		PropertySupport callingOpProp = new PropertySupport.ReadOnly<String>(Calling_Op,
				String.class, Calling_Op, "The calling operation of this operation") {
			@Override
			public String getValue() {
				if (opBean == null) {
					return "";
				}

				OperationFlow callingOp = opBean.getOperation().getCallingOp();
				if (callingOp != null) {
					return callingOp.getId() + "";
				} else {
					return "";
				}
			}

			@Override
			public String getHtmlDisplayName() {
				return "<i>" + getValue() + "</i>";
			}

			@Override
			public String getDisplayName() {
				return this.getValue();
			}
		};
		/**
		 * first of all, PropertySupport's HTMLRenderer usage is very twisted, i
		 * had to step into the source code to find out that "htmlDisplayValue"
		 * has to be set for it to use HTMLRenderer, see
		 * org.openide.explorer.propertysheet
		 * .RendererFactory.StringRenderer.paint() method
		 * 
		 * then, i found out HTMLRenderer doesn't support <a>, it only supports
		 * a fine limit of html entities.
		 * 
		 * image my disappointment!
		 */
		try {
			callingOpProp.setValue("htmlDisplayValue", callingOpProp.getHtmlDisplayName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		set.put(callingOpProp);

		PropertySupport opIdProp = new PropertySupport.ReadOnly<String>(OP_ID, String.class, OP_ID,
				"The operation id") {
			@Override
			public String getValue() {
				if (opBean == null) {
					return "";
				}
				return opBean.getOperation().getId() + "";
			}
		};
		set.put(opIdProp);

		sheet.put(set);
		return sheet;
	}

	public OperationFlow getEntity() {
		return opBean.getOperation();
	}

	@Override
	public String toString() {
		return this.getDisplayName();
	}

}
