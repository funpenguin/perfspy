package com.perfspy.ui;

import java.util.List;

import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;

class OperationFlowNodeChildFactory extends ChildFactory<OperationFlowBean> {
	private OperationFlowBean operationFlowEntity;

	public OperationFlowNodeChildFactory(OperationFlowBean query) {
		this.operationFlowEntity = query;
	}

	@Override
	protected Node createNodeForKey(OperationFlowBean key) {
		return new OperationFlowNode(key);
	}

	@Override
	protected boolean createKeys(List<OperationFlowBean> list) {
		list.addAll(operationFlowEntity.getChildOps());
		return true;
	}
}
