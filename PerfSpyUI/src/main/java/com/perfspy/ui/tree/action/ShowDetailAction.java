package com.perfspy.ui.tree.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;

import com.perfspy.ui.EntityNode;
import com.perfspy.ui.panel.EntityDetailPanelFacctory;

public class ShowDetailAction extends AbstractAction {

	private EntityNode node;

	public ShowDetailAction(EntityNode node) {
		this.node = node;
		putValue(AbstractAction.NAME, "Show Details");
		putValue(AbstractAction.SHORT_DESCRIPTION, "Show Details Of This Operation");

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		DialogDescriptor opertionDetailDlg = new DialogDescriptor(
				EntityDetailPanelFacctory.setEntity(node), "Detail");
		DialogDisplayer.getDefault().notifyLater(opertionDetailDlg);

	}

}
