package com.perfspy.ui.tree.action;

import java.util.List;

import org.netbeans.swing.etable.QuickFilter;

import com.google.common.collect.Lists;

public class ChainFilters implements QuickFilter {

	private List<QuickFilter> filters = Lists.newArrayList();

	public ChainFilters(QuickFilter filter1) {
		filters.add(filter1);
	}

	public QuickFilter chainFilter(QuickFilter filter1) {
		this.filters.add(filter1);
		return this;
	}

	// QuickFilter shows the rows that passed the filter
	@Override
	public boolean accept(Object aValue) {

		for (QuickFilter filter : filters) {
			if (!filter.accept(aValue)) {
				return false;
			}
		}

		return true;
	}

}
