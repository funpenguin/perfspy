package com.perfspy.ui.tree.action;

import java.awt.Cursor;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.Set;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openide.nodes.Node;

import com.google.common.collect.Sets;
import com.perfspy.ui.EntityNode;
import com.perfspy.ui.OperationFlowNode;
import com.perfspy.ui.PerfSpyAdmin;
import com.perfspy.ui.panel.LoadOperationFlowTreePanel;

public class ActionRecorder {

	private static final String SETTING_SELECTED = "selected";
	private static final String SETTING_SHOW_MARKED = "showMarked";
	private static final String SETTING_EVENT_ID = "eventId";
	private static final String SETTING_OPERATION_ID = "operationId";
	private static final String SETTING_HIDDEN = "hidden";
	private static final String SETTING_MARKED = "marked";
	public static final String SETTING_BY_ID = "byId";
	public static final String SETTING_BY_SIG = "bySig";
	private static final String SETTING_MODE = "mode";

	private static Set<OperationFlowNode> markedNodes = Sets.newHashSet();
	private static boolean showOnlyMarked = false;
	private static final Logger logger = Logger.getLogger(SearchAction.class);

	public static void markNode(OperationFlowNode node) {
		if (!node.isMarked()) {
			markedNodes.remove(node);
		} else {
			markedNodes.add(node);
		}
	}

	public static void showOnlyMarked(boolean showOnlyMarked0) {
		showOnlyMarked = showOnlyMarked0;
	}

	public static boolean isShowOnlyMarked() {
		return showOnlyMarked;
	}

	public static void saveActions(File file, boolean byId) throws Exception {
		LoadOperationFlowTreePanel loadOperationTreePanel = PerfSpyAdmin.getPerfSpyAdmin()
				.getLoadOperationTreePanel();
		long eventId = loadOperationTreePanel.getEventId();
		long operationId = loadOperationTreePanel.getOperatoinId();

		java.util.Properties prop = new java.util.Properties();

		if (byId) {
			Long[] markedIds = new Long[markedNodes.size()];
			int index = 0;
			for (OperationFlowNode node : markedNodes) {
				markedIds[index++] = node.getEntity().getId();
			}
			prop.setProperty(SETTING_MARKED, StringUtils.join(markedIds, "#"));
		} else {
			String[] markedSigs = new String[markedNodes.size()];
			int index = 0;
			for (OperationFlowNode node : markedNodes) {
				markedSigs[index++] = node.getEntity().getLongSignature();
			}
			prop.setProperty(SETTING_MARKED, StringUtils.join(markedSigs, "#"));
		}

		prop.setProperty(SETTING_SHOW_MARKED, Boolean.toString(showOnlyMarked));

		Set<String> hiddenOperationNames = HideOperationAction.getHiddenOperationNames();
		prop.setProperty(SETTING_HIDDEN, StringUtils.join(hiddenOperationNames, "#"));

		Node[] selectedNodes = PerfSpyAdmin.getPerfSpyAdmin().getExplorerManager()
				.getSelectedNodes();
		if (selectedNodes.length > 0) {
			if (byId) {
				Long[] selectedNodeIds = new Long[selectedNodes.length];
				for (int i = 0; i < selectedNodes.length; i++) {
					selectedNodeIds[i] = ((OperationFlowNode) selectedNodes[i]).getEntity().getId();
				}
				prop.setProperty(SETTING_SELECTED, StringUtils.join(selectedNodeIds, "#"));
			} else {
				String[] selectedNodeSigs = new String[selectedNodes.length];
				for (int i = 0; i < selectedNodes.length; i++) {
					selectedNodeSigs[i] = ((OperationFlowNode) selectedNodes[i]).getEntity()
							.getLongSignature();
				}
				prop.setProperty(SETTING_SELECTED, StringUtils.join(selectedNodeSigs, "#"));
			}
		}

		prop.setProperty(SETTING_OPERATION_ID, operationId + "");
		prop.setProperty(SETTING_EVENT_ID, eventId + "");

		prop.setProperty(SETTING_MODE, byId ? SETTING_BY_ID : SETTING_BY_SIG);

		FileOutputStream fo = null;
		try {
			fo = new FileOutputStream(file);
			prop.store(fo, null);
		} finally {
			if (fo != null) {
				fo.close();
			}
		}

	}

	public static String replayActions(final File file, final JLabel lblStatus, final boolean apply)
			throws Exception {
		final Properties prop = new Properties();
		FileInputStream fi = null;

		try {
			fi = new FileInputStream(file);
			prop.load(fi);
		} finally {
			if (fi != null) {
				fi.close();
			}
		}

		String mode = prop.getProperty(SETTING_MODE);
		final boolean byId = mode.equals(SETTING_BY_ID) ? true : false;

		final long eventId = getLongProperty(prop, SETTING_EVENT_ID);
		if (eventId == -1) {
			return mode;
		}

		final long opId = getLongProperty(prop, SETTING_OPERATION_ID);
		final PerfSpyAdmin admin = PerfSpyAdmin.getPerfSpyAdmin();

		final String[] marked = getStringArrayProperty(prop, SETTING_MARKED);
		final String[] selected = getStringArrayProperty(prop, SETTING_SELECTED);
		final String[] hidden = getStringArrayProperty(prop, SETTING_HIDDEN);

		final boolean showMarked = Boolean.parseBoolean(prop.getProperty(SETTING_SHOW_MARKED));

		final Runnable loadAction = new Runnable() {
			public void run() {

				LoadOperationFlowTreePanel loadOperationTreePanel = admin
						.getLoadOperationTreePanel();
				if (loadOperationTreePanel.getEventId() != eventId
						|| loadOperationTreePanel.getOperatoinId() != opId) {
					loadOperationTreePanel.triggerLoad(eventId, opId);
				}

				lblStatus.setText("loaded from DB");

			}
		};

		final Runnable expandAction = new Runnable() {
			public void run() {
				if (ArrayUtils.isNotEmpty(marked)) {
					Node rootNode = admin.getExplorerManager().getRootContext();
					ExpandAction.expandAll(admin.getOutlineView(), (EntityNode) rootNode);
					lblStatus.setText("expanded tree");
				}
			}
		};

		final Runnable markAction = new Runnable() {
			public void run() {
				if (ArrayUtils.isNotEmpty(marked)) {

					if (byId) {
						SearchAction.selectOpsByColumn(admin.getExplorerManager(),
								admin.getOutlineView(), marked, 6, admin, false, true);
					} else {
						SearchAction.selectOpsByColumn(admin.getExplorerManager(),
								admin.getOutlineView(), marked, 5, admin, false, true);
					}

					lblStatus.setText("marked tree");
				}
			}
		};

		final Runnable showAction = new Runnable() {
			public void run() {
				if (showMarked) {
					ShowMarkedAction.showOnlyMarked(admin.getExplorerManager(),
							admin.getOutlineView(), null, true);
					lblStatus.setText("show only marked");
				}
			}
		};

		final Runnable hideAction = new Runnable() {
			@Override
			public void run() {
				if (ArrayUtils.isNotEmpty(hidden)) {
					HideOperationAction.setHiddenOperationNames(hidden);
					HideOperationAction.hideNode(admin.getOutlineView());
				}
			}
		};

		final Runnable selectedAction = new Runnable() {
			public void run() {
				if (ArrayUtils.isNotEmpty(selected)) {

					if (byId) {
						SearchAction.selectOpsByColumn(admin.getExplorerManager(),
								admin.getOutlineView(), selected, 6, admin, true, false);
					} else {
						SearchAction.selectOpsByColumn(admin.getExplorerManager(),
								admin.getOutlineView(), selected, 5, admin, true, false);
					}
					lblStatus.setText("selected tree");
				}
			}
		};

		Runnable replayThread = new Runnable() {
			public void run() {
				try {
					if (!apply) {
						SwingUtilities.invokeAndWait(loadAction);
					}
					SwingUtilities.invokeAndWait(new Runnable() {

						@Override
						public void run() {
							admin.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
						}
					});

					/*the reason to go into all these trouble of invoking another thread, and put these actions one by one into the swing thread is because:
					 * if all these actions are run in sequence as one method after user clicks on the load button, although these actions do 
					 * get run one-by-one, swing doesn't seem to update the UI after each action. so it won't show the tree
					 * until after "ShowMarkedAction.showOnlyMarked()" is finished; it also won't update the eventId or operationId fields. 
					 * 
					 * After all actions are run, Swing updates the eventId or operationId fields, and shows the tree -- it is not expanded. 
					 * It is almost like Swing batches all ui-update events, and processes them at the end of the user action, and because expand action
					 * happens when the tree is invisible, i guess ui events from it were not processed?
					 * 
					 * this poses a big problem: i want to expand the tree after it is loaded, if the tree is not shown, then even through the expand action 
					 * is executed, it doesn't get expanded. 
					 */
					SwingUtilities.invokeAndWait(expandAction);

					SwingUtilities.invokeAndWait(markAction);

					SwingUtilities.invokeAndWait(hideAction);

					SwingUtilities.invokeAndWait(selectedAction);

					SwingUtilities.invokeAndWait(showAction);

					lblStatus.setText("loaded from " + file.getName());

				} catch (Exception e) {
					logger.error("Error in loading file: " + file, e);
				} finally {
					try {
						SwingUtilities.invokeAndWait(new Runnable() {
							@Override
							public void run() {
								admin.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
							}
						});
					} catch (Exception e) {
						logger.error("Error in loading file: " + file, e);
					}

				}
			}
		};

		new Thread(replayThread).start();

		return mode;

	}

	private static long getLongProperty(Properties prop, String key) {
		String value = prop.getProperty(key);
		return Long.parseLong(value);
	}

	private static String[] getStringArrayProperty(Properties prop, String key) {
		String value = prop.getProperty(key);
		if (StringUtils.isBlank(value)) {
			return null;
		}

		return value.split("#");
	}

	private static Long[] parseToLongArray(String[] longStr) {
		Long[] ls = new Long[longStr.length];

		for (int i = 0; i < longStr.length; i++) {
			try {
				ls[i] = Long.parseLong(longStr[i]);
			} catch (NumberFormatException e) {
				ls[i] = -1l;
				logger.debug("error in parse a long number:", e);
			}
		}

		return ls;
	}

}
