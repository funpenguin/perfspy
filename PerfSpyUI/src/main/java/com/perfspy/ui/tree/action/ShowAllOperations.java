package com.perfspy.ui.tree.action;

import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;

import javax.swing.AbstractAction;

import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;

import com.perfspy.ui.EntityNode;
import com.perfspy.ui.OperationFlowNode;

public class ShowAllOperations extends AbstractAction {

	private EntityNode node;
	private OutlineView outlineView;
	private ExplorerManager exploreManager;

	public ShowAllOperations() {
		putValue(AbstractAction.NAME, "Show All Operations");
		putValue(AbstractAction.SHORT_DESCRIPTION, "Show All Operations");
	}

	public ShowAllOperations(EntityNode operationFlowNode, ExplorerManager exploreManager0,
			OutlineView outlineView0) {
		this();
		this.node = operationFlowNode;

		exploreManager = exploreManager0;
		outlineView = outlineView0;

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		Outline outline = outlineView.getOutline();
		outline.unsetQuickFilter();
		HideOperationAction.reset();
		ShowMarkedAction.reset(outlineView, node instanceof OperationFlowNode);

		try {
			exploreManager.setSelectedNodes(new Node[] { node });
		} catch (PropertyVetoException e1) {
			e1.printStackTrace();
		}
	}

}
