package com.perfspy.ui.tree.action;

import java.awt.Component;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;

import com.google.common.collect.Lists;
import com.perfspy.monitor.model.OperationFlow;
import com.perfspy.monitor.util.ReflectionUtil;
import com.perfspy.ui.EntityNode;
import com.perfspy.ui.OperationFlowNode;

public class SearchAction {

	private static int searchRowIndex = -1;
	private static final String getNodeFromRowMethod = "getNodeFromRow";
	private static final Logger logger = Logger.getLogger(SearchAction.class);

	public static void selectOpsByColumn(ExplorerManager explorerManager, OutlineView outlineView,
			String[] opIds, int column, Component host, boolean select, boolean mark) {
		try {
			selectByColumn(explorerManager, outlineView, opIds, column, select, mark);
		} catch (Exception e) {
			logger.error("error in selecting op with calling op id:" + ArrayUtils.toString(opIds),
					e);
		}
	}

	public static void selectOp(ExplorerManager explorerManager, OutlineView outlineView,
			long opId, Component host) {
		try {
			selectByColumn(explorerManager, outlineView, new String[] { opId + "" }, 6, true, false);
		} catch (Exception e) {
			logger.error("error in selecting op with calling op id:" + opId, e);
		}

	}

	public static void selectParent(ExplorerManager explorerManager, OutlineView outlineView,
			OperationFlow op, Component host) {

		long callingOpId = op.getCallingOpId();

		try {
			selectByColumn(explorerManager, outlineView, new String[] { callingOpId + "" }, 6,
					true, false);
		} catch (Exception e) {
			logger.error("error in selecting op with calling op id:" + callingOpId, e);
		}

	}

	public static void selectByColumn(ExplorerManager explorerManager, OutlineView outlineView,
			String[] columnValues, int columnIndex, boolean select, boolean mark) {

		List<Integer> matched = new ArrayList<Integer>();

		Outline outline = outlineView.getOutline();

		int rowCount = outline.getRowCount();
		for (int i = 1; i < rowCount; i++) {
			Object value = null;
			if (columnIndex == 0) {
				//outline.getValueAt(i, 0) returns org.openide.explorer.view.VisualizerNode
				//column0 is a treeNode, while column 1...n are table columns
				value = outline.getValueAt(i, 0).toString();
			} else {
				Object valueAt = outline.getValueAt(i, columnIndex);
				PropertySupport prop = (PropertySupport) valueAt;
				try {
					value = prop.getValue();
				} catch (Exception e) {
					logger.error("error in searching for " + ArrayUtils.toString(columnValues)
							+ " at column " + columnIndex, e);
					continue;
				}
			}

			if (ArrayUtils.contains(columnValues, value)) {
				matched.add(i);
				if (matched.size() == columnValues.length) {
					break;
				}
			}

		}

		selectAndMark(explorerManager, outlineView, matched, select, mark);
	}

	public static void search(ExplorerManager explorerManager, OutlineView outlineView,
			int columnIndex, String toLookFor, Component host, boolean all, boolean mark) {

		toLookFor = toLookFor.toLowerCase();
		List<Integer> matched = new ArrayList<Integer>();

		Outline outline = outlineView.getOutline();

		if (all) {
			searchRowIndex = -1;
		} else {
			ListSelectionModel selectionModel = outline.getSelectionModel();

			if (selectionModel.getMaxSelectionIndex() != -1) {
				searchRowIndex = selectionModel.getMaxSelectionIndex();
			}
		}

		/**
		 * model.getRowCount(); this includes all rows, including the filtered
		 * ones
		 * 
		 * outlineView.getOutline().getRowCount() excludes the filtered rows
		 * 
		 * this is a big difference, if use model.getValueAt(), since the row
		 * size and row order is destroyed when there are filtered rows,
		 * searching is not accurate.
		 */
		int rowCount = outline.getRowCount();
		for (int i = searchRowIndex + 1; i < rowCount; i++) {
			String value = null;
			if (columnIndex == 0) {
				//outline.getValueAt(i, 0) returns org.openide.explorer.view.VisualizerNode
				//column0 is a treeNode, while column 1...n are table columns
				value = outline.getValueAt(i, 0).toString();
			} else {
				Object valueAt = outline.getValueAt(i, columnIndex);
				PropertySupport prop = (PropertySupport) valueAt;
				try {
					value = (String) prop.getValue();
				} catch (Exception e) {
					logger.error("error in searching for " + toLookFor + " at column "
							+ columnIndex, e);
					continue;
				}
			}

			if (value.toLowerCase().contains(toLookFor)) {
				matched.add(i);
				if (!all) {
					searchRowIndex = i;
					break;
				}
			}
		}

		if (matched.size() == 0) {
			JOptionPane
					.showMessageDialog(
							host,
							"no operation contains "
									+ toLookFor
									+ "can be found. You may:\n 1) select the first node and search again. "
									+ "\n 2) check if the nodes you are looking for haven't been expanded, locate their parants and expand them."
									+ "\n 3) unfortunately, there is a bug: if you've hidden some nodes, search will not be able to locate a node correctly,"
									+ "\n    you can search nearby and good luck :-).", "INFO",
							JOptionPane.INFORMATION_MESSAGE);
			searchRowIndex = -1;
			return;
		}

		selectAndMark(explorerManager, outlineView, matched, true, mark);
	}

	public static void selectAndMark(ExplorerManager explorerManager, OutlineView outlineView,
			int matched, boolean select, boolean mark) {
		selectAndMark(explorerManager, outlineView, Lists.newArrayList(matched), select, mark);
	}

	public static void selectAndMark(ExplorerManager explorerManager, OutlineView outlineView,
			int[] selectedRows, boolean select, boolean mark) {
		selectAndMark(explorerManager, outlineView,
				Arrays.asList(ArrayUtils.toObject(selectedRows)), select, mark);
	}

	public static void selectAndMark(ExplorerManager explorerManager, OutlineView outlineView,
			List<Integer> matched, boolean select, boolean mark) {

		List<Node> selectedNodes = new ArrayList<Node>();
		for (Integer row : matched) {
			Node node = getNodeFromRow(outlineView, row);
			selectedNodes.add(node);
		}

		selectAndMarkNodes(explorerManager, outlineView, selectedNodes, select, mark);
	}

	public static void selectAndMarkNodes(ExplorerManager explorerManager, OutlineView outlineView,
			List<Node> selectedNodes, boolean select, boolean mark) {
		for (Node node : selectedNodes) {

			EntityNode entityNode = (EntityNode) node;
			//do not change its marked property
			if (!entityNode.isMarked() && mark) {
				entityNode.setMarked(mark);
			}

			if (node instanceof OperationFlowNode) {
				ActionRecorder.markNode((OperationFlowNode) node);
			}

		}

		if (select && selectedNodes.size() > 0) {
			try {
				explorerManager.setSelectedNodes(selectedNodes.toArray(new Node[] {}));
			} catch (PropertyVetoException e) {
				logger.error("error in selecting nodes", e);
			}
		}

		outlineView.revalidate();
		outlineView.repaint();

	}

	// OutlineView.getNodeFromRow(int rowIndex) is package scope
	public static Node getNodeFromRow(OutlineView outlineView, int row) {
		Map<Class, Object> params = new HashMap<Class, Object>();
		params.put(int.class, row);
		return (Node) ReflectionUtil.invokeMethod(outlineView, getNodeFromRowMethod, params);
	}

}
