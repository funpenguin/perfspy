package com.perfspy.ui.tree.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;

import com.perfspy.ui.EntityNode;
import com.perfspy.ui.OperationFlowNode;

public class MarkOperationAction extends AbstractAction {

	private static OutlineView outlineView;
	private static ExplorerManager exploreManager;
	/**
	 * this is an action that can be invoked from multiple selected nodes, in
	 * other words, it is a common action to multiple nodes, hence this action
	 * should not be created for every individual node, so use a static instance
	 * to make sure it is "shared"
	 */
	public static MarkOperationAction markAction = new MarkOperationAction();

	public static MarkOperationAction config(ExplorerManager exploreManager0,
			OutlineView outlineView0) {
		outlineView = outlineView0;
		exploreManager = exploreManager0;
		return markAction;
	}

	private MarkOperationAction() {
		putValue(AbstractAction.NAME, "(Un)Mark Operation");
		putValue(AbstractAction.SHORT_DESCRIPTION, "(Un)Mark Operation");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Node[] nodes = exploreManager.getSelectedNodes();
		if (nodes.length > 0) {
			for (int i = 0; i < nodes.length; i++) {
				EntityNode node = (EntityNode) nodes[i];
				node.setMarked(!node.isMarked());

			}

			outlineView.revalidate();

			if (nodes[0] instanceof OperationFlowNode) {
				ActionRecorder.markNode((OperationFlowNode) nodes[0]);
			}

		}
	}

}
