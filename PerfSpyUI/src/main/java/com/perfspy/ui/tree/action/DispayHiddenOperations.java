package com.perfspy.ui.tree.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.perfspy.ui.PerfSpyAdmin;
import com.perfspy.ui.panel.HiddenOperationDialog;

public class DispayHiddenOperations extends AbstractAction {

	public DispayHiddenOperations() {
		putValue(AbstractAction.NAME, "Display Hidden Operations");
		putValue(AbstractAction.SHORT_DESCRIPTION, "Display Hidden Operations");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		HiddenOperationDialog.showDialog(PerfSpyAdmin.getPerfSpyAdmin());
	}

}
