package com.perfspy.ui.tree.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.netbeans.swing.etable.QuickFilter;
import org.openide.explorer.view.OutlineView;

import com.perfspy.ui.OperationFlowNode;
import com.perfspy.ui.PerfSpyAdmin;

public class ShowUnhealthyOperationAction extends AbstractAction {

	public ShowUnhealthyOperationAction() {
		putValue(AbstractAction.NAME, "Show Unhealthy Operations");
		putValue(AbstractAction.SHORT_DESCRIPTION, "Show Unhealthy Operations");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		OutlineView outlineView = PerfSpyAdmin.getPerfSpyAdmin().getOutlineView();

		// QuickFilter shows the rows that passed the filter
		QuickFilter filter = new QuickFilter() {
			@Override
			public boolean accept(Object aValue) {
				// the children nodes are created asynchronously, so when first
				// expanded, aValue is not OperationFlowNode, at one point, it
				// is a org.openide.nodes.ChildFactory$WaitFilterNode, and then
				// a AbstractNode (i guess)
				if (!aValue.getClass().equals(OperationFlowNode.class)) {
					return true;
				}

				OperationFlowNode node1 = (OperationFlowNode) aValue;
				if (!node1.getEntity().isHealthy()) {
					return true;
				}

				return false;
			}
		};

		outlineView.getOutline().setQuickFilter(0, filter);

	}

}
