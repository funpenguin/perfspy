package com.perfspy.ui.tree.action;

import java.awt.event.ActionEvent;
import java.util.HashSet;
import java.util.Set;

import javax.swing.AbstractAction;

import org.apache.log4j.Logger;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;

import com.perfspy.ui.EntityNode;

public class ExpandAction extends AbstractAction {
	private EntityNode node;
	private static int expandToLevel = 5;
	private boolean expandAll;
	private OutlineView outlineView;
	private static final Logger logger = Logger.getLogger(ExpandAction.class);

	public ExpandAction(EntityNode operationFlowNode, OutlineView outlineView) {
		this(operationFlowNode, false, outlineView);
	}

	public ExpandAction(EntityNode operationFlowNode, boolean expandAll, OutlineView outlineView) {
		this.node = operationFlowNode;
		if (!expandAll) {
			putValue(AbstractAction.NAME, "Expand " + expandToLevel + " levels");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Expand " + expandToLevel + " levels");
		} else {
			putValue(AbstractAction.NAME, "Expand ALL");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Expand ALL");
		}
		this.expandAll = expandAll;

		this.outlineView = outlineView;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (this.expandAll) {
			expandAll(outlineView, node);
		} else {
			expandToLevel(outlineView, node);
		}
	}

	private void expandToLevel(OutlineView outlineView, EntityNode node) {
		if (node == null) {
			return;
		}

		// true: child nodes are created asynchronously, wait for the child
		// nodes are created
		Node[] childNodes = node.getChildren().getNodes(true);

		for (Node child : childNodes) {
			expandToNextLevel(outlineView, child, 2);
		}
	}

	private void expandToNextLevel(OutlineView outlineView, Node child, int level) {
		level++;
		Node[] childrenAtNextLevel = child.getChildren().getNodes(true);
		if (childrenAtNextLevel.length == 0) {
			outlineView.expandNode(child);
		} else {
			for (Node child2 : childrenAtNextLevel) {
				if (level >= expandToLevel) {
					outlineView.expandNode(child2);
				} else {
					expandToNextLevel(outlineView, child2, level);
				}
			}
		}
	}

	public static void expandAll(OutlineView outlineView, EntityNode node) {
		if (node == null) {
			return;
		}

		// prevent from infinite loop, which can happen if an tree node points
		// to another tree node, which again point the former node, 
		//e.g. ObjectA contains ObjectB, ObjectB contains ObjectA
		Set<Object> expanded = new HashSet<Object>();

		doExpandAll(outlineView, node, expanded);

	}

	private static void doExpandAll(OutlineView outlineView, EntityNode node, Set<Object> expanded) {
		if (node.getEntity() == null) {
			return;
		}

		if (expanded.contains(node.getEntity())) {
			if (logger.isDebugEnabled()) {
				logger.debug("already expanded:" + node.getEntityDiscrption());
			}
			return;
		}

		expanded.add(node.getEntity());

		Node[] childNodes = node.getChildren().getNodes(true);
		if (childNodes.length > 0) {
			outlineView.expandNode(node);

			for (Node child : childNodes) {
				doExpandAll(outlineView, (EntityNode) child, expanded);
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Got and expanded " + childNodes.length + " kids:"
					+ node.getEntityDiscrption());
		}

	}

}
