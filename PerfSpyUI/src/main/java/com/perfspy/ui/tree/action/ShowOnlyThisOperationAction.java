package com.perfspy.ui.tree.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.openide.explorer.view.OutlineView;

import com.perfspy.ui.OperationFlowNode;
import com.perfspy.ui.PerfSpyAdmin;

public class ShowOnlyThisOperationAction extends AbstractAction {

	private OperationFlowNode node;

	public ShowOnlyThisOperationAction(OperationFlowNode operationFlowNode) {
		this.node = operationFlowNode;
		putValue(AbstractAction.NAME, "Show This Operation Only");
		putValue(AbstractAction.SHORT_DESCRIPTION, "Show This Operation Only");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		OutlineView outlineView = PerfSpyAdmin.getPerfSpyAdmin().getOutlineView();

		PerfSpyAdmin.getPerfSpyAdmin().triggerLoad(
				PerfSpyAdmin.getPerfSpyAdmin().getLoadOperationTreePanel().getEventId(), this.node.getEntity().getId());

	}

}
