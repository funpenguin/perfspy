package com.perfspy.ui.tree.action;

import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;

import javax.swing.AbstractAction;

import org.netbeans.swing.etable.QuickFilter;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;

import com.perfspy.ui.EntityNode;
import com.perfspy.ui.OperationFlowNode;

public class ShowMarkedAction extends AbstractAction {
	private EntityNode node;
	private OutlineView outlineView;
	private ExplorerManager exploreManager;

	public ShowMarkedAction(EntityNode operationFlowNode, ExplorerManager exploreManager0,
			OutlineView outlineView0) {
		this.node = operationFlowNode;
		exploreManager = exploreManager0;
		outlineView = outlineView0;

		putValue(AbstractAction.NAME, "Show Only Marked Operations");
		putValue(AbstractAction.SHORT_DESCRIPTION, "Show Marked Operations");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		showOnlyMarked(exploreManager, outlineView, node, node instanceof OperationFlowNode);

	}

	public static void showOnlyMarked(ExplorerManager exploreManager, OutlineView outlineView,
			Node selectedNode, boolean operationTreeOutlineView) {
		outlineView.getOutline().setQuickFilter(0, showOnlyMarkedAction);

		if (operationTreeOutlineView) {
			ActionRecorder.showOnlyMarked(true);
		}

		if (selectedNode != null) {
			try {
				exploreManager.setSelectedNodes(new Node[] { selectedNode });
			} catch (PropertyVetoException e1) {
				e1.printStackTrace();
			}
		}

	}

	public static void reset(OutlineView outlineView, boolean operationTreeOutlineView) {
		outlineView.getOutline().setQuickFilter(0, null);
		if (operationTreeOutlineView) {
			ActionRecorder.showOnlyMarked(false);
		}
	}

	// QuickFilter shows the rows that passed the filter
	static QuickFilter showOnlyMarkedAction = new QuickFilter() {
		@Override
		public boolean accept(Object aValue) {
			// the children nodes are created asynchronously, so when first
			// expanded, aValue is not OperationFlowNode, at one point, it
			// is a org.openide.nodes.ChildFactory$WaitFilterNode, and then
			// a AbstractNode (i guess)
			if (!(aValue instanceof EntityNode)) {
				return true;
			}

			EntityNode node1 = (EntityNode) aValue;
			if (node1.isMarked()) {
				return true;
			}

			return false;
		}
	};

}
