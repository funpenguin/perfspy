package com.perfspy.ui.tree.action;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;

import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;

import com.google.common.collect.Lists;
import com.perfspy.monitor.model.OperationFlow;
import com.perfspy.ui.EntityNode;
import com.perfspy.ui.OperationFlowNode;

public class MarkAncestorsAction extends AbstractAction {

	private static OutlineView outlineView;
	private static ExplorerManager exploreManager;
	/**
	 * this is an action that can be invoked from multiple selected nodes, in
	 * other words, it is a common action to multiple nodes, hence this action
	 * should not be created for every individual node, so use a static instance
	 * to make sure it is "shared"
	 */
	public static MarkAncestorsAction markAction = new MarkAncestorsAction();

	public static MarkAncestorsAction config(ExplorerManager exploreManager0,
			OutlineView outlineView0) {
		outlineView = outlineView0;
		exploreManager = exploreManager0;
		return markAction;
	}

	private MarkAncestorsAction() {
		putValue(AbstractAction.NAME, "Mark Ancestor");
		putValue(AbstractAction.SHORT_DESCRIPTION, "Mark Ancestor");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Node[] nodes = exploreManager.getSelectedNodes();
		if (nodes.length > 0) {
			for (int i = 0; i < nodes.length; i++) {
				EntityNode node = (EntityNode) nodes[i];

				if (node instanceof OperationFlowNode) {
					List<String> ancestorIds = Lists.newArrayList();
					OperationFlowNode opNode = (OperationFlowNode) node;
					OperationFlow callingOp = opNode.getEntity().getCallingOp();
					while (callingOp != null) {
						ancestorIds.add(callingOp.getId() + "");
						callingOp = callingOp.getCallingOp();
					}

					SearchAction.selectByColumn(exploreManager, outlineView,
							ancestorIds.toArray(new String[0]), 6, false, true);
				}
			}

			outlineView.revalidate();

			if (nodes[0] instanceof OperationFlowNode) {
				ActionRecorder.markNode((OperationFlowNode) nodes[0]);
			}

		}
	}

}
