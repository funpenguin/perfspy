package com.perfspy.ui.tree.action;

import java.awt.event.ActionEvent;
import java.util.Map;
import java.util.Set;

import javax.swing.AbstractAction;

import org.netbeans.swing.etable.QuickFilter;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;

import com.google.common.collect.Maps;
import com.perfspy.ui.OperationFlowNode;
import com.perfspy.ui.PerfSpyAdmin;

/**
 * "Hide Operations from this class" is not used, mainly because i think the
 * right click menu is too long, but i leave the code around
 */
public class HideOperationAction extends AbstractAction {
	private static Map<String, IntWrapper> hiddenOperations = Maps.newHashMap();
	private OperationFlowNode node;
	private static Map<String, IntWrapper> hiddenClasses = Maps.newHashMap();
	private boolean hideClass;

	public static class IntWrapper {
		int intVal;

		public IntWrapper() {
			this(0);
		}

		public IntWrapper(int intVal) {
			this.intVal = intVal;
		}

		public void increase() {
			this.intVal++;
		}

		public void reset() {
			this.intVal = 0;
		}

		public int getIntVal() {
			return intVal;
		}

	}

	public HideOperationAction(OperationFlowNode operationFlowNode) {
		this(operationFlowNode, false);
	}

	public HideOperationAction(OperationFlowNode operationFlowNode, boolean hideClass) {
		this.node = operationFlowNode;
		this.hideClass = hideClass;

		if (!hideClass) {
			putValue(AbstractAction.NAME, "Hide This Operation");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Hide This Operation");
		} else {
			putValue(AbstractAction.NAME, "Hide Operations From This Class");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Hide Operations From This Class");

		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		OutlineView outlineView = PerfSpyAdmin.getPerfSpyAdmin().getOutlineView();

		resetHiddenOperationCount();

		if (!hideClass) {
			hiddenOperations.put(this.node.getDisplayName(), new IntWrapper());
		} else {
			hiddenClasses.put(this.node.getEntity().getShortClassName(), new IntWrapper());
		}

		int selectedRowIndex = outlineView.getOutline().getSelectedRow();

		hideNode(outlineView);

		selectTheClosestNode(selectedRowIndex);

	}

	public static void selectTheClosestNode(int selectedRowIndex) {

		PerfSpyAdmin perfSpyAdmin = PerfSpyAdmin.getPerfSpyAdmin();
		OutlineView outlineView = perfSpyAdmin.getOutlineView();
		//		Outline outline = outlineView.getOutline();
		//		int rowCount = outline.getRowCount();
		//		for (int i = selectedRowIndex; i < rowCount; i++) {
		//			Node nextNode = SearchAction.getNodeFromRow(outlineView, i);
		//			if (!isFiltered(nextNode)) {
		//				//						matched.add(i);
		//				break;
		//			}
		//		}

		/*the purpose of this is to select the node that is nearest to the ones that haven been filtered
		 * this code is not 100% correct, but it is close enough to anchor one's eyes*/
		SearchAction.selectAndMark(perfSpyAdmin.getExplorerManager(), outlineView,
				selectedRowIndex, true, false);

	}

	//	private static Node findNearestSibling(Node node, Node[] siblings) {
	//		int pos = 0;
	//		for (int i = 0; i < siblings.length; i++) {
	//			if (siblings[i] == node) {
	//				pos = i;
	//				break;
	//			}
	//		}
	//
	//		int i = pos, j = pos;
	//		while (true) {
	//			if (--i >= 0) {
	//				if (!isFiltered(siblings[i])) {
	//					return siblings[i];
	//				}
	//			}
	//
	//			if (++j < siblings.length) {
	//				if (!isFiltered(siblings[j])) {
	//					return siblings[j];
	//				}
	//			}
	//
	//			if (i < 0 && j >= siblings.length) {
	//				break;
	//			}
	//
	//		}
	//
	//		return null;
	//	}

	public static void hideNode(OutlineView outlineView) {

		// QuickFilter shows the rows that passed the filter
		QuickFilter filter = new QuickFilter() {
			@Override
			public boolean accept(Object aValue) {
				// the children nodes are created asynchronously, so when first
				// expanded, aValue is not OperationFlowNode, at one point, it
				// is a org.openide.nodes.ChildFactory$WaitFilterNode, and then
				// a AbstractNode (i guess)
				if (!aValue.getClass().equals(OperationFlowNode.class)) {
					return true;
				}
				return hideTheNode((OperationFlowNode) aValue);
			}

		};

		QuickFilter chainFilter = filter;
		if (ActionRecorder.isShowOnlyMarked()) {
			chainFilter = new ChainFilters(filter)
					.chainFilter(ShowMarkedAction.showOnlyMarkedAction);
		}
		outlineView.getOutline().setQuickFilter(0, chainFilter);
	}

	private static boolean hideTheNode(OperationFlowNode node1) {
		if (node1.getEntity().isDummy()) {
			return true;
		}

		if (isFiltered(node1)) {
			return false;
		}

		return true;
	}

	private static boolean isFiltered(Node nodeToTest) {
		for (String hiddenCls : hiddenClasses.keySet()) {
			if (hiddenCls.equals(((OperationFlowNode) nodeToTest).getEntity().getShortClassName())) {
				hiddenClasses.get(hiddenCls).increase();
				return true;
			}
		}

		for (String op1 : hiddenOperations.keySet()) {
			if (op1.equals(nodeToTest.getDisplayName())) {
				hiddenOperations.get(op1).increase();
				return true;
			}
		}

		return false;
	}

	private void resetHiddenOperationCount() {
		for (String hiddenCls : hiddenClasses.keySet()) {
			hiddenClasses.get(hiddenCls).reset();
		}

		for (String hiddenOp : hiddenOperations.keySet()) {
			hiddenOperations.get(hiddenOp).reset();
		}
	}

	public static void reset() {
		hiddenOperations.clear();
		hiddenClasses.clear();
	}

	public static void setHiddenOperationNames(String[] hiddenNames) {
		for (String name : hiddenNames) {
			hiddenOperations.put(name, new IntWrapper(0));
		}
	}

	public static Set<String> getHiddenOperationNames() {
		return hiddenOperations.keySet();
	}

	public static Map<String, IntWrapper> getHiddenOperations() {
		return hiddenOperations;
	}

}
