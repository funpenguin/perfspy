package com.perfspy.ui;

import java.util.ArrayList;
import java.util.List;

import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

import com.perfspy.monitor.model.OperationFlow;

public final class OperationFlowBean implements Lookup.Provider {
	private OperationFlow op;
	private Lookup lookup;
	private InstanceContent instanceContent;

	public OperationFlowBean() {
		this(null);
	}

	public OperationFlowBean(OperationFlow op0) {
		this.op = op0;
		this.instanceContent = new InstanceContent();
		this.lookup = new AbstractLookup(instanceContent);
	}

	public List<OperationFlowBean> getChildOps() {
		if (this.op == null) {
			return new ArrayList<OperationFlowBean>();
		}
		List<OperationFlowBean> ops = new ArrayList<OperationFlowBean>();

		for (OperationFlow op : this.op.getOutOps()) {
			OperationFlowBean e = new OperationFlowBean(op);
			ops.add(e);
		}
		return ops;
	}

	public String getDisplay() {
		if (this.op == null) {
			return "";
		}
		return this.op.getSignature();

	}

	public OperationFlow getOperation() {
		return op;
	}

	@Override
	public String toString() {
		if (this.op == null) {
			return "";
		}
		return op.toString();
	}

	public Lookup getLookup() {
		return lookup;
	}

}
