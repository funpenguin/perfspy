package com.perfspy.ui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

import com.cedarsoftware.util.io.JsonConstant;
import com.cedarsoftware.util.io.JsonObject;
import com.cedarsoftware.util.io.JsonReader;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.perfspy.monitor.util.ToStringUtil;
import com.perfspy.monitor.util.TypeUtil;

public final class JsonObjectBean implements Lookup.Provider {

	private Object obj;
	private String objName;

	private Lookup lookup;
	private InstanceContent instanceContent;
	private List<JsonObjectBean> childJsonNodes = null;

	private Map<Long, JsonObject> jsonObjRead = Maps.newHashMap();
	private static final Logger logger = Logger.getLogger(JsonObjectBean.class);

	public static JsonObjectBean fromJsonString(String jsonName, String jsonString)
			throws IOException {

		JsonObjectBean bean = new JsonObjectBean();
		bean.objName = jsonName;

		if (jsonString == null) {
			jsonString = "null";
		}

		ByteArrayInputStream ba = new ByteArrayInputStream(jsonString.getBytes("UTF-8"));
		JsonReader reader = new JsonReader(ba, false);
		bean.obj = reader.readJsonObject();
		bean.jsonObjRead = reader.getObjectsRead();
		reader.close();
		reader = null;

		bean.init();

		return bean;
	}

	private JsonObjectBean() {

	}

	private static void fillInType(JsonObject jsonObj) throws Exception {
		// if component type is the same as array type, "@t" is not logged so
		// save space and performance
		// see JSonWrite.logFieldType
		if (jsonObj.isArray()) {
			fillInArrayType(jsonObj, jsonObj.getType());
		}

		Set<String> fieldNames = jsonObj.keySet();
		for (String fieldName : fieldNames) {
			Object fieldValue = jsonObj.get(fieldName);
			if (fieldValue instanceof JsonObject) {
				JsonObject fieldJson = (JsonObject) fieldValue;

				if (fieldJson.isArray()) {
					fillInArrayType(fieldJson, fieldJson.getType());
				}

			}
		}

	}

	private static void fillInArrayType(JsonObject arrayJson, String arrayType) {
		Object[] items = arrayJson.getArray();
		for (Object item : items) {
			if (item instanceof JsonObject) {
				JsonObject itemJson = (JsonObject) item;
				if (!itemJson.containsKey(JsonConstant.JSON_VALUE) && itemJson.getType() == null) {
					itemJson.setType(getComponetClassName(arrayType));
				}
			}
		}
	}

	private static String getComponetClassName(String arrayTypeClass) {
		return arrayTypeClass.substring("[L".length(), arrayTypeClass.length() - 1);
	}

	public JsonObjectBean(String objName, Object obj, Map<Long, JsonObject> jsonObjRead) {
		this.objName = objName;
		this.obj = obj;
		this.jsonObjRead = jsonObjRead;

		init();
	}

	private void init() {
		this.instanceContent = new InstanceContent();
		this.lookup = new AbstractLookup(instanceContent);

		if (this.obj instanceof JsonObject) {
			JsonObject jsonObj = (JsonObject) this.obj;
			try {
				fillInType(jsonObj);
				if (logger.isDebugEnabled()) {
					logger.debug("inited:" + jsonObj.getType() + "@" + jsonObj.getSystemHashcode());
				}
			} catch (Exception e) {
				logger.debug("fail to fill in type information for:" + jsonObj, e);
			}
		}
	}

	public List<JsonObjectBean> getChildJsonNodes() {
		if (childJsonNodes != null) {
			return childJsonNodes;
		}

		if (this.obj == null || !(this.obj instanceof JsonObject)) {
			childJsonNodes = new ArrayList<JsonObjectBean>();
			return childJsonNodes;
		}

		childJsonNodes = new ArrayList<JsonObjectBean>();

		JsonObject<String, Object> jsonObj = (JsonObject<String, Object>) obj;

		if (jsonObj.containsKey(JsonConstant.JSON_VALUE)) {
			// a simple value
			return childJsonNodes;
		}

		if (jsonObj.getType().equals(Timestamp.class.getName())) {
			return childJsonNodes;
		}

		if (TypeUtil.isArray(this.obj)) {
			int length = Array.getLength(this.obj);
			for (int i = 0; i < length; i++) {
				Object eleValue = Array.get(this.obj, i);
				childJsonNodes.add(new JsonObjectBean("[" + i + "]", eleValue, this.jsonObjRead));
			}
			return childJsonNodes;
		}

		if (jsonObj.isArray() || jsonObj.isCollectionAssumption()) {
			Object[] eles = jsonObj.getArray();
			if (eles == null || eles.length == 0) {
				return Lists.newArrayList();
			}
			for (int i = 0; i < eles.length; i++) {
				childJsonNodes.add(createJsonObjectBean("[" + i + "]", eles[i]));
			}
			return childJsonNodes;
		}

		if (jsonObj.isMapAssumption()) {
			Object[] keys = (Object[]) jsonObj.get(JsonConstant.JSON_KEYS);
			Object[] items = jsonObj.getArray();

			if (keys == null || keys.length == 0) {
				return Lists.newArrayList();
			}

			for (int i = 0; i < keys.length; i++) {
				childJsonNodes.add(createJsonObjectBean("entry-key[" + i + "]", keys[i]));
				childJsonNodes.add(createJsonObjectBean("entry-value[" + i + "]", items[i]));
			}
			return childJsonNodes;
		}

		Set<String> keys = jsonObj.keySet();
		for (String key : keys) {
			Object value = jsonObj.get(key);
			JsonObjectBean jsonObjectBean = createJsonObjectBean(key, value);
			childJsonNodes.add(jsonObjectBean);
		}

		return childJsonNodes;
	}

	private JsonObjectBean createJsonObjectBean(String name, Object value) {
		if (value instanceof JsonObject) {
			JsonObject<String, Object> jsonObjValue = (JsonObject<String, Object>) value;
			//getJsonObjectDisplay will take care of properly describing the jsonObjValue, so need to get a simple object here
			//			if (jsonObjValue.containsKey(JsonConstant.JSON_VALUE)) {
			//				return new JsonObjectBean(name, jsonObjValue.get(JsonConstant.JSON_VALUE),
			//						jsonObjRead);
			//			} else 

			if (jsonObjValue.containsKey(JsonConstant.JSON_REF)) {
				JsonObject referred = jsonObjRead.get(jsonObjValue.get(JsonConstant.JSON_REF));
				return new JsonObjectBean(name, referred, jsonObjRead);
			} else {
				return new JsonObjectBean(name, jsonObjValue, jsonObjRead);
			}
		}
		return new JsonObjectBean(name, value, jsonObjRead);
	}

	public String getObjName() {
		return this.objName;

	}

	// public FieldNameValue getFieldValue() {
	// return fnv;
	// }

	@Override
	public String toString() {
		return this.obj.toString();
	}

	public String getJsonObjectDisplay() {
		if (this.obj instanceof JsonObject) {

			return ToStringUtil.getJsonObjectDescription((JsonObject) obj);
		}
		return ToStringUtil.getObjectDescription(obj);
	}

	public Object getJsonObject() {
		return obj;
	}

	public Lookup getLookup() {
		return lookup;
	}

}
