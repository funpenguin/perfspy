package com.perfspy.ui;

import java.util.Stack;

import org.apache.commons.lang3.StringUtils;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.Lookup;

public abstract class EntityNode<T> extends AbstractNode {

	private boolean marked = false;

	public EntityNode(Children children) {
		super(children);
	}

	public EntityNode(Children children, Lookup lookup) {
		super(children, lookup);
	}

	public abstract T getEntity();

	public abstract String getEntityDiscrption();

	public String getEntityHierarchy() {
		Stack<EntityNode> ch = new Stack<EntityNode>();
		ch.push(this);
		EntityNode parent = (EntityNode) this.getParentNode();

		while (parent != null) {
			ch.push(parent);
			parent = (EntityNode) parent.getParentNode();
		}

		StringBuffer sb = new StringBuffer();
		String ws = "--";
		int level = 0;
		while (!ch.empty()) {
			EntityNode op = ch.pop();
			sb.append(StringUtils.repeat(ws, level++) + op.getEntityDiscrption() + "\n");
		}

		return sb.toString();
	}

	public void setMarked(boolean marked) {
		if (this.marked != marked) {
			this.marked = marked;
		}

	}

	public void forceMark(boolean marked) {
		this.marked = marked;

	}

	public boolean isMarked() {
		return marked;
	}

}
