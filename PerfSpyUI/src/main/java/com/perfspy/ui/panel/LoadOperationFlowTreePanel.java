package com.perfspy.ui.panel;

import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.perfspy.monitor.dao.OperationFlowDAO;
import com.perfspy.monitor.model.OperationFlow;
import com.perfspy.ui.OperationFlowBean;
import com.perfspy.ui.OperationFlowNode;
import com.perfspy.ui.PerfSpyAdmin;
import com.perfspy.ui.tree.action.ActionRecorder;
import com.perfspy.ui.tree.action.HideOperationAction;
import com.perfspy.ui.tree.action.SearchAction;
import com.perfspy.ui.tree.action.ShowMarkedAction;

public class LoadOperationFlowTreePanel extends javax.swing.JPanel {
	private static final Logger logger = Logger.getLogger(LoadOperationFlowTreePanel.class);

	public LoadOperationFlowTreePanel() {
		initComponents();
	}

	class LoadPanel extends javax.swing.JPanel {
		LoadPanel() {
			lblSpyEventId = new javax.swing.JLabel();
			lblSpyEventId.setText("Spy Event ID");
			txtSpyEventId = new javax.swing.JTextField();
			lblSpyOperationId = new javax.swing.JLabel();
			lblSpyOperationId.setText("Spy Operation");
			txtSpyOp = new javax.swing.JTextField();
			btnLoadFromDB = new javax.swing.JButton();
			btnLoadFromDB.setText("Load");
			btnLoadFromDB.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					loadFromDb(evt);
				}
			});

			this.setLayout(new java.awt.GridBagLayout());

			java.awt.GridBagConstraints gridBagConstraints;
			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE_LEADING;
			gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
			this.add(lblSpyEventId, gridBagConstraints);

			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
			gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
			gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
			gridBagConstraints.weightx = 1;
			gridBagConstraints.weighty = 1;
			gridBagConstraints.gridwidth = 3;
			this.add(txtSpyEventId, gridBagConstraints);

			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.gridy = 2;
			gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
			this.add(lblSpyOperationId, gridBagConstraints);

			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.gridy = 2;
			gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
			gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
			gridBagConstraints.weightx = 1;
			gridBagConstraints.weighty = 1;
			gridBagConstraints.gridwidth = 3;
			this.add(txtSpyOp, gridBagConstraints);

			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.gridx = 1;
			gridBagConstraints.gridy = 3;
			gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE_LEADING;
			gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
			this.add(btnLoadFromDB, gridBagConstraints);
		}
	}

	class SearchOpPanel extends SearchPanel {

		private JTextField txtGoto;

		public SearchOpPanel() {
			super();

			GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();

			JButton btnGoto = new JButton();
			btnGoto.setText("GOTO");
			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.gridx = 2;
			gridBagConstraints.gridy = ++currentGridy;
			gridBagConstraints.anchor = GridBagConstraints.LINE_END;
			gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 5);
			add(btnGoto, gridBagConstraints);
			btnGoto.addActionListener(new java.awt.event.ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Long opId = parseLongFromTextField(txtGoto, "GOTO field");
					gotoOp(opId);
				}
			});

			txtGoto = new JTextField();
			txtGoto.setColumns(8);
			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.gridy = currentGridy;
			gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 5);
			add(txtGoto, gridBagConstraints);

		}

	}

	class SavePanel extends javax.swing.JPanel {
		private JLabel lblSaveStatus;
		private JRadioButton rbBySig;
		private JRadioButton rbById;

		public SavePanel() {
			java.awt.GridBagConstraints gridBagConstraints;

			setLayout(new java.awt.GridBagLayout());

			ButtonGroup bg = new ButtonGroup();

			setLayout(new java.awt.GridBagLayout());

			rbById = new JRadioButton("By Id");
			rbById.setSelected(true);
			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
			this.add(rbById, gridBagConstraints);

			rbBySig = new JRadioButton("By Signature");
			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
			this.add(rbBySig, gridBagConstraints);

			bg.add(rbById);
			bg.add(rbBySig);

			JButton btnSave = new JButton("Save");
			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.gridy = 1;
			gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 5);
			this.add(btnSave, gridBagConstraints);
			btnSave.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					save();
				}
			});

			JButton btnSaveAs = new JButton("Save As...");
			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.gridy = 1;
			gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 5);
			this.add(btnSaveAs, gridBagConstraints);
			btnSaveAs.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					saveAs();
				}

			});

			JButton btnLoadFromFile = new JButton("Load");
			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.gridy = 1;
			gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 5);
			this.add(btnLoadFromFile, gridBagConstraints);
			btnLoadFromFile.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					loadFile(false);
				}

			});

			JButton btnApplyFromFile = new JButton("Apply");
			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.gridy = 1;
			gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 5);
			this.add(btnApplyFromFile, gridBagConstraints);
			btnApplyFromFile.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					loadFile(true);
				}

			});

			lblSaveStatus = new JLabel();
			lblSaveStatus.setText("unsaved");
			gridBagConstraints = new java.awt.GridBagConstraints();
			gridBagConstraints.gridy = 2;
			gridBagConstraints.gridwidth = 4;
			gridBagConstraints.anchor = GridBagConstraints.LINE_START;
			gridBagConstraints.fill = GridBagConstraints.BOTH;
			gridBagConstraints.insets = new java.awt.Insets(15, 0, 0, 0);
			this.add(lblSaveStatus, gridBagConstraints);

		}
	}

	private void saveAs() {
		this.settingFile = null;

		save();
	}

	private void save() {
		if (this.settingFile == null) {
			JFileChooser fc = initFileChooser();

			if (fc.showSaveDialog(PerfSpyAdmin.getPerfSpyAdmin()) != JFileChooser.APPROVE_OPTION) {
				return;
			}

			File file = fc.getSelectedFile();
			if (file == null) {
				return;
			}

			String fileName = file.getAbsolutePath();
			if (!fileName.endsWith(".properties")) {
				fileName = fileName + ".properties";
			}

			this.settingFile = new File(fileName);

			this.lastSettingFileDir = file.getParentFile().getAbsolutePath();
		}

		boolean byId = this.savePanel.rbById.isSelected();
		try {
			ActionRecorder.saveActions(this.settingFile, byId);
			this.savePanel.lblSaveStatus.setText("saved to " + this.settingFile.getName());
		} catch (Exception e) {
			logger.error("error in saving to " + this.settingFile, e);
			MessagePanel.showQuickErrorDialog(PerfSpyAdmin.getPerfSpyAdmin(), "Error in Saving", e);
		}

	}

	private JFileChooser initFileChooser() {
		JFileChooser fc;
		if (this.lastSettingFileDir == null) {
			fc = new JFileChooser();
		} else {
			fc = new JFileChooser(this.lastSettingFileDir);
		}

		fc.setFileFilter(new FileNameExtensionFilter("PropertyFiles (*.properties)", "properties"));
		return fc;
	}

	private void loadFile(boolean apply) {
		JFileChooser fc = initFileChooser();

		if (fc.showOpenDialog(PerfSpyAdmin.getPerfSpyAdmin()) != JFileChooser.APPROVE_OPTION) {
			return;
		}

		File file = fc.getSelectedFile();
		if (file == null) {
			return;
		}

		this.settingFile = file;
		this.lastSettingFileDir = file.getParentFile().getAbsolutePath();

		try {
			String mode = ActionRecorder.replayActions(file, this.savePanel.lblSaveStatus, apply);
			if (mode.equals(ActionRecorder.SETTING_BY_ID)) {
				savePanel.rbById.setSelected(true);
			} else {
				savePanel.rbBySig.setSelected(true);
			}
		} catch (Exception e) {
			logger.error("error in loading from " + file, e);
			MessagePanel
					.showQuickErrorDialog(PerfSpyAdmin.getPerfSpyAdmin(), "Error in Loading", e);
		}

	}

	protected void gotoOp(long opId) {
		SearchAction.selectOp(PerfSpyAdmin.getPerfSpyAdmin().getExplorerManager(), PerfSpyAdmin
				.getPerfSpyAdmin().getOutlineView(), opId, this);
	}

	private void initComponents() {
		this.setLayout(new GridBagLayout());
		java.awt.GridBagConstraints gridBagConstraints;

		LoadPanel loadPanel = new LoadPanel();
		Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		loadPanel.setBorder(new TitledBorder(loweredetched, "Load"));
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 0.5;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		this.add(loadPanel, gridBagConstraints);

		savePanel = new SavePanel();
		savePanel.setBorder(new TitledBorder(loweredetched, "Save"));
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.weightx = 0.5;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		this.add(savePanel, gridBagConstraints);

		searchPanel = new SearchOpPanel();
		searchPanel.setOutlineView(PerfSpyAdmin.getPerfSpyAdmin().getOutlineView());
		searchPanel.setExploreManager(PerfSpyAdmin.getPerfSpyAdmin().getExplorerManager());

		searchPanel.setBorder(new TitledBorder(loweredetched, "Search"));
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.weightx = 0.5;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		this.add(searchPanel, gridBagConstraints);
	}

	private void loadFromDb(java.awt.event.ActionEvent evt) {
		OperationFlow op = null;
		List<OperationFlow> spyEventOverview = null;
		PerfSpyAdmin admin = PerfSpyAdmin.getPerfSpyAdmin();
		admin.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		try {
			long spyEventId = parseLongFromTextField(txtSpyEventId, "spy event id");
			if (spyEventId != -1) {
				this.eventId = spyEventId;
				Long spyOpId = parseLongFromTextField(txtSpyOp, "operation id");
				this.operatoinId = spyOpId;
				OperationFlowDAO dao = admin.getOperationFlowDao();
				spyEventOverview = dao.getSpyEventOverview(spyEventId);
				op = dao.getOperationTreeByEventId(spyEventId, spyOpId == -1 ? 1 : spyOpId);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			logger.error("Error in loading an operation tree from DB", e);
			admin.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			return;
		}

		if (spyEventOverview != null) {
			admin.setSpyEventOverview(spyEventOverview);
		}

		if (op != null) {
			// clear quick filters
			admin.getOutlineView().getOutline().unsetQuickFilter();
			HideOperationAction.reset();
			ShowMarkedAction.reset(admin.getOutlineView(), true);
			HiddenOperationDialog.clearTable();

			OperationFlowBean query = new OperationFlowBean(op);
			OperationFlowNode node = new OperationFlowNode(query);
			admin.getExplorerManager().setRootContext(node);

			admin.setAdminTitle("Now you are viewing event " + eventId + ":" + op.getFirstOutOp());
		} else {
			JOptionPane.showMessageDialog(this, "Can't load the operation tree from DB", "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		admin.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

	}

	public long getEventId() {
		return eventId;
	}

	public long getOperatoinId() {
		return operatoinId;
	}

	public void triggerLoad(long eventId, long... operationId) {
		txtSpyEventId.setText(eventId + "");
		if (operationId.length > 0) {
			txtSpyOp.setText(operationId[0] + "");
		}
		loadFromDb(null);
	}

	public void setGotoOp(long opId) {
		this.searchPanel.txtGoto.setText(opId + "");
	}

	private long parseLongFromTextField(JTextField txtField, String txtName) {
		String value = txtField.getText();
		long longValue = -1;
		if (StringUtils.isNotBlank(value)) {
			try {
				longValue = Long.parseLong(value);
			} catch (Exception e) {
				String errorMsg = txtName + " is not a valid number";
				logger.debug(errorMsg, e);
				JOptionPane.showMessageDialog(this, errorMsg);
			}
		}

		return longValue;

	}

	private javax.swing.JButton btnLoadFromDB;
	private javax.swing.JLabel lblSpyEventId;

	private javax.swing.JLabel lblSpyOperationId;

	private javax.swing.JTextField txtSpyEventId;
	//	private javax.swing.JButton btnSearchAll;
	//	private javax.swing.JButton btnSearchNext;
	//	private javax.swing.JLabel lblSearch;
	//	private javax.swing.JTextField txtSearch;

	private javax.swing.JTextField txtSpyOp;

	private long eventId = -1;
	private long operatoinId = -1;
	private File settingFile;
	private String lastSettingFileDir;

	private SearchOpPanel searchPanel;
	private SavePanel savePanel;

}
