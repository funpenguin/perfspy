package com.perfspy.ui.panel;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class MessagePanel {
	public static boolean showConfirmDialog(Component parent, String title, String msg) {
		JScrollPane scrollPane = stuffInformation(msg);
		int confirm = JOptionPane.showConfirmDialog(parent, scrollPane, title, JOptionPane.YES_NO_OPTION);
		if (confirm == JOptionPane.YES_OPTION) {
			// this popup window clears the parent's cursor, so have to set the
			// busy cursor again here
			Cursor cursor = parent.getCursor();
			if (cursor.getType() == Cursor.WAIT_CURSOR) {
				parent.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			}
			return true;
		}
		parent.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		return false;
	}

	public static void showQuickErrorDialog(Component parent, String errorMsg, Exception e) {

		StringWriter writer = new StringWriter();
		e.printStackTrace(new PrintWriter(writer));
		JScrollPane scrollPane = stuffInformation(writer.toString());
		Cursor cursor = parent.getCursor();
		if (cursor.getType() == Cursor.WAIT_CURSOR) {
			parent.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}

		JOptionPane.showMessageDialog(parent, scrollPane, errorMsg, JOptionPane.ERROR_MESSAGE);
	}

	private static JScrollPane stuffInformation(String info) {
		final JTextArea textArea = new JTextArea();
		textArea.setEditable(false);

		textArea.setText(info);
		textArea.setCaretPosition(0);

		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setPreferredSize(new Dimension(500, 300));
		return scrollPane;
	}
}
