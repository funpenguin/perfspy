package com.perfspy.ui.panel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

import com.perfspy.monitor.model.OperationFlow;

public class SpyEventOverViewTableModel extends DefaultTableModel {

	public void setOverviews(List<OperationFlow> ops) {
		if (this.ops.equals(ops)) {
			return;
		}
		this.ops = ops;

		// ArrayList<SpyEvent>(events);
		if (SwingUtilities.isEventDispatchThread()) {
			fireTableDataChanged();
			return;
		}

		try {
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					fireTableDataChanged();
				}
			});
		} catch (Exception e) {// ignore
		}

	}

	@Override
	public Object getValueAt(int row, int column) {
		OperationFlow op = this.ops.get(row);
		switch (column) {
		case 0:
			return op.getLongSignature();
		case 1:
			return op.getId();
		case 2:
			return op.getExecutionTime();
		case 3:
			return op.getAnalyzeReport();
		case 4:
			return op.getCallingOpId();
		default:
			return "";
		}

	}

	@Override
	public int getRowCount() {
		// this method is called during the <init> of the superclass
		// DefaultTableModel, upon that time, events hasn't got a chance to be
		// initialized
		if (ops == null) {
			return 0;
		}
		return ops.size();
	}

	@Override
	public int getColumnCount() {
		return COLS.length;
	}

	@Override
	public String getColumnName(int column) {
		return COLS[column];
	}

	private List<OperationFlow> ops = new ArrayList<OperationFlow>();

	private static final String[] COLS = new String[] { "Long Signature", "ID",
			"Execution Time", "Analyze Report", "Calling Operation" };
}
