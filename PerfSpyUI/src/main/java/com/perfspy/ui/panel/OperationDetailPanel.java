/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perfspy.ui.panel;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.perfspy.monitor.model.OperationFlow;

public class OperationDetailPanel extends javax.swing.JPanel {

	public OperationDetailPanel() {
		initComponents();
	}

	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		jLabel1 = new javax.swing.JLabel();
		txtSignature = new javax.swing.JTextField();
		jLabel2 = new javax.swing.JLabel();
		lblExecutionTime = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		txtAnalyzeResult = new javax.swing.JTextField();
		jLabel4 = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		jScrollPane1 = new javax.swing.JScrollPane();
		jScrollPane2 = new javax.swing.JScrollPane();
		jScrollPane3 = new javax.swing.JScrollPane();
		txtParameters = new javax.swing.JTextArea();
		txtReturn = new javax.swing.JTextArea();
		jLabel6 = new javax.swing.JLabel();
		txtLongSig = new javax.swing.JTextArea();
		java.awt.Color fgColor = new java.awt.Color(0, 0, 255);

		setLayout(new java.awt.GridBagLayout());
		gridBagConstraints = new java.awt.GridBagConstraints();

		jLabel1.setText("Signature:");
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE_LEADING;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		gridBagConstraints.weighty = 1.0;
		add(jLabel1, gridBagConstraints);

		txtSignature.setEditable(false);
		txtSignature.setForeground(fgColor);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(txtSignature, gridBagConstraints);

		JLabel lblCallingHierarchy = new JLabel();
		lblCallingHierarchy.setText("Calling Hierarchy");
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE_LEADING;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.gridy = 1;
		add(lblCallingHierarchy, gridBagConstraints);

		txtCallingHierarchy = new JTextArea();
		txtCallingHierarchy.setEditable(false);
		txtCallingHierarchy.setForeground(fgColor);
		txtCallingHierarchy.setColumns(20);
		txtCallingHierarchy.setRows(5);
		JScrollPane spCh = new JScrollPane(txtCallingHierarchy);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(spCh, gridBagConstraints);

		jLabel6.setText("Long Signature:");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 2;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE_LEADING;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(jLabel6, gridBagConstraints);

		txtLongSig.setColumns(20);
		txtLongSig.setRows(5);
		txtLongSig.setForeground(fgColor);
		txtLongSig.setEditable(false);
		txtLongSig.setLineWrap(true);
		jScrollPane2.setViewportView(txtLongSig);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(jScrollPane2, gridBagConstraints);

		jLabel2.setText("Execution Time:");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 3;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE_LEADING;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(jLabel2, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 3;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(lblExecutionTime, gridBagConstraints);
		lblExecutionTime.setForeground(fgColor);

		jLabel3.setText("Analyze Result:");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 4;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE_LEADING;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(jLabel3, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 4;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(txtAnalyzeResult, gridBagConstraints);
		txtAnalyzeResult.setEditable(false);
		txtAnalyzeResult.setForeground(fgColor);

		jLabel4.setText("Parameters:");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 5;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE_LEADING;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(jLabel4, gridBagConstraints);

		txtParameters.setColumns(20);
		txtParameters.setRows(5);
		txtParameters.setForeground(fgColor);
		txtParameters.setLineWrap(true);
		jScrollPane1.setViewportView(txtParameters);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 5;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
		add(jScrollPane1, gridBagConstraints);

		jLabel5.setText("Return value:");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 6;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE_LEADING;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(jLabel5, gridBagConstraints);

		txtReturn.setColumns(20);
		txtReturn.setRows(5);
		txtReturn.setForeground(fgColor);
		txtReturn.setLineWrap(true);
		txtReturn.setEditable(false);
		jScrollPane3.setViewportView(txtReturn);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 6;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.CENTER;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(jScrollPane3, gridBagConstraints);

		this.setPreferredSize(new Dimension(800, 700));
	}

	public void setOperation(OperationFlow op) {
		this.txtCallingHierarchy.setText(op.getCallingHierarchy());
		this.txtSignature.setText(op.getSignature());
		this.txtLongSig.setText(op.getLongSignature());
		this.lblExecutionTime.setText(op.getExecutionTime() + "");
		this.txtAnalyzeResult.setText(op.getAnalyzeReport());
		this.txtParameters.setText(op.getParametersToString());
		this.txtReturn.setText(op.getReturnValueToString());
	}

	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JScrollPane jScrollPane3;
	private javax.swing.JLabel lblExecutionTime;
	private javax.swing.JTextField txtAnalyzeResult;
	private javax.swing.JTextArea txtLongSig;
	private javax.swing.JTextArea txtParameters;
	private javax.swing.JTextArea txtReturn;
	private javax.swing.JTextField txtSignature;
	private JTextArea txtCallingHierarchy;
}
