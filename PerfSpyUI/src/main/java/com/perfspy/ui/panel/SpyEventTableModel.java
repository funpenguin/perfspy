package com.perfspy.ui.panel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

import org.netbeans.swing.etable.ETable;

import com.perfspy.monitor.model.SpyEvent;

public class SpyEventTableModel extends DefaultTableModel {

	public void setEvents(final ETable table, final long[] selectedEvents, Collection<SpyEvent> events) {

		this.events = new ArrayList(events);
		if (SwingUtilities.isEventDispatchThread()) {
			fireTableDataChanged();
			preserveSelection(table, selectedEvents);
			return;
		}

		try {
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					fireTableDataChanged();
					preserveSelection(table, selectedEvents);
				}
			});
		} catch (Exception e) {// ignore
		}

	}

	/*try to select preserve the selection, probably make more sense to do it on SpyEventMonitorPanel, 
	 * but here i already have SwingUtilities.invokeAndWait
	*/
	private void preserveSelection(ETable table, long[] selectedEvents) {
		if (selectedEvents.length >= 0) {
			for (long selectedEvent : selectedEvents) {
				int i = 0;
				for (SpyEvent spyEvent : events) {
					if (spyEvent.getId() == selectedEvent) {
						table.addRowSelectionInterval(i, i);
					}
					i++;
				}
			}

		}
	}

	@Override
	public Object getValueAt(int row, int column) {
		SpyEvent event = this.events.get(row);
		switch (column) {
		case 0:
			return event.getId();
		case 1:
			return event.getName();
		case 2:
			return event.getSpyTime();
		case 3:
			return event.getOperationCount();
		case 4:
			return event.isFinished();
		default:
			return "";
		}

	}

	@Override
	public int getRowCount() {
		// this method is called during the <init> of the superclass
		// DefaultTableModel, upon that time, events hasn't got a chance to be
		// initialized
		if (events == null) {
			return 0;
		}
		return events.size();
	}

	@Override
	public int getColumnCount() {
		return COLS.length;
	}

	@Override
	public String getColumnName(int column) {
		return COLS[column];
	}

	private List<SpyEvent> events = new ArrayList<SpyEvent>();
	private ETable table;
	private static final String[] COLS = new String[] { "ID", "Name", "Spy Time", "Operation Count", "Finished" };
}
