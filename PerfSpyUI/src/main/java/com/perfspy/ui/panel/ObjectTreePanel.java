package com.perfspy.ui.panel;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.NodePopupFactory;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;

import com.perfspy.ui.JsonObjectNode;
import com.perfspy.ui.tree.action.ShowMarkedAction;

public class ObjectTreePanel extends JPanel implements ExplorerManager.Provider {
	private ExplorerManager explorerManager;
	private OutlineView outlineView;
	private static final Logger logger = Logger.getLogger(ObjectTreePanel.class);
	private SearchPanel searchPanel;

	public ObjectTreePanel() {
		this.setLayout(new BorderLayout());

		explorerManager = new ExplorerManager();
		outlineView = new OutlineView();
		initOutlineView();

		this.searchPanel = new SearchPanel();
		this.searchPanel.setOutlineView(outlineView);
		this.searchPanel.setExploreManager(explorerManager);
		this.add(searchPanel, BorderLayout.NORTH);

		this.add(outlineView, BorderLayout.CENTER);

		this.setPreferredSize(new Dimension(1200, 800));
	}

	public void setObject(String name, String jsonString) {
		try {
			ShowMarkedAction.reset(outlineView, false);

			JsonObjectNode node = new JsonObjectNode(name, jsonString);
			explorerManager.setRootContext(node);
		} catch (IOException e) {
			logger.error("error in creating a node from json:" + jsonString + " for " + name, e);
			try {
				explorerManager.setRootContext(new JsonObjectNode(name, null));
			} catch (IOException ignore) {
				logger.error("fail again  in creating a empty node:", e);
			}
		}

	}

	public OutlineView getOutlineView() {
		return outlineView;
	}

	@Override
	public ExplorerManager getExplorerManager() {
		return explorerManager;
	}

	private void initOutlineView() {

		final Outline outline = outlineView.getOutline();
		outline.setRowSorter(null);
		outline.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		outlineView.setPropertyColumns(JsonObjectNode.VALUE_PROP, JsonObjectNode.VALUE_PROP);
		TableColumn col1 = outline.getColumnModel().getColumn(1);
		col1.setPreferredWidth(500);

		// disable showing row filter menu, see
		// http://netbeans.org/bugzilla/show_bug.cgi?id=185196
		outlineView.setNodePopupFactory(new NodePopupFactory() {
			@Override
			public JPopupMenu createPopupMenu(int row, int column, Node[] selectedNodes,
					Component component) {
				return super.createPopupMenu(row, column, selectedNodes, component);
			}
		});
		outlineView.getNodePopupFactory().setShowQuickFilter(false);
		outline.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

	}
}
