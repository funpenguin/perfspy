/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perfspy.ui.panel;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.openide.nodes.Node;

import com.google.common.collect.Lists;
import com.perfspy.ui.EntityNode;
import com.perfspy.ui.PerfSpyAdmin;
import com.perfspy.ui.tree.action.SearchAction;

public class JsonObjectDetailPanel extends javax.swing.JPanel {

	public JsonObjectDetailPanel() {
		initComponents();
	}

	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		java.awt.Color fgColor = new java.awt.Color(0, 0, 255);

		setLayout(new java.awt.GridBagLayout());

		JLabel lblCallingHierarchy = new JLabel();
		lblCallingHierarchy.setText("Hierarchy");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE_LEADING;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		gridBagConstraints.weighty = 1.0;
		add(lblCallingHierarchy, gridBagConstraints);

		txtCallingHierarchy = new JTextArea();
		txtCallingHierarchy.setEditable(false);
		txtCallingHierarchy.setForeground(fgColor);
		txtCallingHierarchy.setColumns(20);
		txtCallingHierarchy.setRows(5);
		JScrollPane spCh = new JScrollPane(txtCallingHierarchy);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(spCh, gridBagConstraints);

		JButton btnMark = new JButton("Mark");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		add(btnMark, gridBagConstraints);
		btnMark.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				markHierachy();
			}
		});

		this.setPreferredSize(new Dimension(500, 300));
	}

	private void markHierachy() {
		List<Node> ch = Lists.newArrayList();
		ch.add(node);

		EntityNode parent = (EntityNode) node.getParentNode();

		while (parent != null) {
			ch.add(parent);
			parent = (EntityNode) parent.getParentNode();
		}

		ObjectTreePanel objectTreePanel = PerfSpyAdmin.getPerfSpyAdmin().getObjectTreePanel();
		SearchAction.selectAndMarkNodes(objectTreePanel.getExplorerManager(),
				objectTreePanel.getOutlineView(), ch, true, true);
	}

	public void setNode(EntityNode node) {
		this.node = node;
		txtCallingHierarchy.setText(node.getEntityHierarchy());
	}

	private JTextArea txtCallingHierarchy;
	private EntityNode node;

}
