package com.perfspy.ui.panel;

import java.awt.event.ActionEvent;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.apache.commons.lang3.StringUtils;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;

import com.google.common.collect.Maps;
import com.perfspy.ui.tree.action.SearchAction;

public class SearchPanel extends javax.swing.JPanel {

	protected javax.swing.JButton btnSearchAll;
	protected javax.swing.JButton btnSearchNext;
	protected javax.swing.JLabel lblSearch;
	protected javax.swing.JTextField txtSearch;
	protected JButton btnMarkAll;
	protected JButton btnSearchMarkNext;
	protected int currentGridy;
	protected int currentGridx;

	private OutlineView outlineView;
	private ExplorerManager exploreManager;
	private JComboBox cmbColumnIndex;
	private Map<String, Integer> columns = Maps.newLinkedHashMap();

	public SearchPanel() {
		java.awt.GridBagConstraints gridBagConstraints;

		setLayout(new java.awt.GridBagLayout());

		lblSearch = new javax.swing.JLabel();
		lblSearch.setText("Search");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(lblSearch, gridBagConstraints);

		txtSearch = new javax.swing.JTextField();
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridwidth = 8;
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1.0;
		add(txtSearch, gridBagConstraints);

		JLabel lblColumnIndex = new JLabel("BY");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		add(lblColumnIndex, gridBagConstraints);

		cmbColumnIndex = new javax.swing.JComboBox();

		cmbColumnIndex.setSelectedItem(0);
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		add(cmbColumnIndex, gridBagConstraints);

		btnSearchNext = new javax.swing.JButton();
		btnSearchNext.setText("Search Next");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 5);
		add(btnSearchNext, gridBagConstraints);

		btnSearchMarkNext = new javax.swing.JButton();
		btnSearchMarkNext.setText("Search&Mark Next");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 2;
		gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 5);
		add(btnSearchMarkNext, gridBagConstraints);

		btnSearchAll = new javax.swing.JButton();
		btnSearchAll.setText("Search All");
		gridBagConstraints = new java.awt.GridBagConstraints();
		//		gridBagConstraints.gridx = 6;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.insets = new java.awt.Insets(15, 200, 5, 5);
		add(btnSearchAll, gridBagConstraints);

		btnMarkAll = new javax.swing.JButton();
		btnMarkAll.setText("Search&Mark All");
		gridBagConstraints = new java.awt.GridBagConstraints();
		//		gridBagConstraints.gridx = 7;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 5);
		add(btnMarkAll, gridBagConstraints);

		currentGridy = gridBagConstraints.gridy;
		currentGridx = gridBagConstraints.gridx;

		btnSearchNext.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				search(false, false);
			}
		});

		btnSearchMarkNext.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				search(false, true);
			}
		});

		btnSearchAll.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				search(true, false);
			}
		});

		btnMarkAll.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				search(true, true);
			}
		});

	}

	public void setOutlineView(OutlineView outlineView) {
		this.outlineView = outlineView;

		Outline outline = this.outlineView.getOutline();
		int colCount = outline.getColumnCount();
		for (int i = 0; i < colCount; i++) {
			String colName = outline.getColumnName(i);
			this.columns.put(colName, i);
		}
		cmbColumnIndex.setModel(new javax.swing.DefaultComboBoxModel(this.columns.keySet().toArray(
				new String[0])));
	}

	public void setExploreManager(ExplorerManager exploreManager) {
		this.exploreManager = exploreManager;
	}

	protected void search(boolean all, boolean mark) {
		String searchText = this.getSearchText();
		if (StringUtils.isBlank(searchText)) {
			return;
		}

		String searchColumnName = (String) this.cmbColumnIndex.getSelectedItem();
		Integer searchColumIndex = this.columns.get(searchColumnName);
		search(searchColumIndex, searchText.toLowerCase(), all, mark);
	}

	protected void search(int searchByColumnIndex, String toLookFor, boolean all, boolean mark) {
		SearchAction.search(this.exploreManager, this.outlineView, searchByColumnIndex, toLookFor,
				this, all, mark);
	}

	public String getSearchText() {
		return this.txtSearch.getText();
	}

}