package com.perfspy.ui.panel;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellRenderer;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.netbeans.swing.etable.ETable;

import com.perfspy.monitor.dao.SpyEventDAO;
import com.perfspy.monitor.model.SpyEvent;
import com.perfspy.ui.PerfSpyAdmin;

public class SpyEventMonitorPanel extends javax.swing.JPanel {
	private static final Logger logger = Logger.getLogger(SpyEventMonitorPanel.class);

	public SpyEventMonitorPanel() {
		initComponents();
	}

	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		lblSpyEvent = new javax.swing.JLabel();
		cmbEventNums = new javax.swing.JComboBox();
		btnStart = new javax.swing.JButton();
		btnStop = new javax.swing.JButton();
		jScrollPane1 = new javax.swing.JScrollPane();
		tableSpyEvent = new ETable(new SpyEventTableModel()) {
			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {

				Component comp = super.prepareRenderer(renderer, row, col);
				if (col == 5) {
					return comp;
				}
				if (!isCellSelected(row, col)) {
					if (row % 2 == 0) {// && !isCellSelected(row, col)) {
						comp.setBackground(PerfSpyAdmin.LIGHT_GREEN);
					} else {
						comp.setBackground(Color.white);
					}
				} else {
					comp.setBackground(Color.lightGray);
				}
				return comp;
			}

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return false;
			}
		};

		//		tableSpyEvent.addMouseListener()
		tableSpyEvent.setRowHeight(25);
		tableSpyEvent.setRowSorter(null);
		tableSpyEvent.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tableSpyEvent.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					loadEvent();
				}
			}

		});

		setLayout(new java.awt.GridBagLayout());

		lblSpyEvent.setText("mointoring the latest spy event:");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 0;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BELOW_BASELINE_LEADING;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(lblSpyEvent, gridBagConstraints);

		cmbEventNums.setModel(new javax.swing.DefaultComboBoxModel(new Integer[] { 10, 15, 20, 25,
				30, 50, 100, 150, 200 }));
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		gridBagConstraints.ipadx = 50;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		add(cmbEventNums, gridBagConstraints);

		btnStart.setText("start");
		btnStart.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnStartActionPerformed(evt);
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		add(btnStart, gridBagConstraints);

		btnStop.setText("stop");
		btnStop.setEnabled(false);
		btnStop.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnStopActionPerformed(evt);
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		add(btnStop, gridBagConstraints);

		btnDeleteThisEvent = new JButton();
		btnDeleteThisEvent.setText("Delete");
		btnDeleteThisEvent.setToolTipText("Delete This Event");
		gridBagConstraints = new java.awt.GridBagConstraints();
		//		gridBagConstraints.gridx = 13;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
		gridBagConstraints.insets = new java.awt.Insets(5, 200, 5, 5);
		this.add(btnDeleteThisEvent, gridBagConstraints);
		btnDeleteThisEvent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteEvent(false);
			}
		});

		btnDeleteAllEvents = new JButton();
		btnDeleteAllEvents.setText("Delete All");
		btnDeleteAllEvents.setToolTipText("Delete All Events");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		this.add(btnDeleteAllEvents, gridBagConstraints);
		btnDeleteAllEvents.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				deleteEvent(true);
			}
		});

		jScrollPane1.setViewportView(tableSpyEvent);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridwidth = 15;
		gridBagConstraints.gridheight = java.awt.GridBagConstraints.RELATIVE;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.weighty = 1.0;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		add(jScrollPane1, gridBagConstraints);
	}

	private void deleteEvent(boolean deleteAll) {
		long[] eventIdsToDelete = new long[0];

		if (!deleteAll) {
			//			eventIdToDelete = getSelectedEvent();
			eventIdsToDelete = getSelectedEvents();
			if (eventIdsToDelete.length == 0) {
				JOptionPane.showMessageDialog(PerfSpyAdmin.getPerfSpyAdmin(),
						"Please select an event to delete!");
				return;
			}
		}

		if (JOptionPane.showConfirmDialog(
				PerfSpyAdmin.getPerfSpyAdmin(),
				"You are about to delete "
						+ (deleteAll ? "all events" : "event "
								+ ArrayUtils.toString(eventIdsToDelete)) + ",please confirm",
				"Confirm", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
			return;
		}

		try {
			int delRows = SpyEventDAO.deleteEvent(PerfSpyAdmin.getPerfSpyAdmin().getConnection(),
					deleteAll, eventIdsToDelete);
			JOptionPane.showMessageDialog(PerfSpyAdmin.getPerfSpyAdmin(), "Deleted " + delRows
					+ " rows");
		} catch (Exception e) {
			logger.error("Error in deleting event:" + eventIdsToDelete, e);
			MessagePanel.showQuickErrorDialog(PerfSpyAdmin.getPerfSpyAdmin(),
					"Error in deleting event:" + eventIdsToDelete, e);
		}

	}

	private void loadEvent() {
		PerfSpyAdmin.getPerfSpyAdmin().triggerLoad(getSelectedEvent());
	}

	private long[] getSelectedEvents() {
		int[] rows = tableSpyEvent.getSelectedRows();
		if (rows.length == 0) {
			return new long[0];
		}

		long[] tableEventIds = new long[rows.length];
		for (int i = 0; i < rows.length; i++) {
			long eventId = (Long) tableSpyEvent.getValueAt(rows[i], 0);
			tableEventIds[i] = eventId;

		}

		return tableEventIds;
	}

	private long getSelectedEvent() {

		int row = tableSpyEvent.getSelectedRow();
		if (row == -1) {
			return -1;
		}
		long eventId = (Long) tableSpyEvent.getValueAt(row, 0);
		return eventId;
	}

	private void btnStartActionPerformed(java.awt.event.ActionEvent evt) {
		stopMonitor = false;
		btnStart.setEnabled(false);
		btnStop.setEnabled(true);

		startMonitorThread();

	}

	private void getLastNSpyEvents() {
		long[] selectedEvents = this.getSelectedEvents();

		Integer number = (Integer) cmbEventNums.getSelectedItem();
		if (number != null) {
			try {
				Collection<SpyEvent> spyEvents = SpyEventDAO
						.getLastNEvents(getConnection(), number);
				((SpyEventTableModel) tableSpyEvent.getModel()).setEvents(tableSpyEvent,
						selectedEvents, spyEvents);

			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
				logger.error("Error in monitoring the spy events", e);
			}
		}
	}

	private void startMonitorThread() {
		if (this.monitorThread == null) {
			monitorThread = new Thread(new MonitorTask(), "Spy Event Monitoring");
		}

		if (!monitorRunning) {
			monitorThread.start();
		}
	}

	private void btnStopActionPerformed(java.awt.event.ActionEvent evt) {
		stopMonitor = true;
		btnStop.setEnabled(false);
		btnStart.setEnabled(true);
	}

	private Connection getConnection() throws Exception {
		if (con == null) {
			con = PerfSpyAdmin.getPerfSpyAdmin().getPerfSpyConfig().getConnection(null);// "jdbc:oracle:thin:@16.155.207.79:1521:ORCL",		
		}
		return con;
	}

	public void close() {
		stopMonitor = true;
		if (con != null) {
			try {
				if (!con.isClosed()) {
					con.close();
				}
			} catch (SQLException e1) {
				logger.error("error in closing the connection", e1);
				// ignore
			}
		}

	}

	private class MonitorTask implements Runnable {
		@Override
		public void run() {
			monitorRunning = true;

			while (!stopMonitor) {
				getLastNSpyEvents();
				if (!stopMonitor) {
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						break;// ignore
					}
				}

			}// while

			monitorRunning = false;
		}
	}

	private Thread monitorThread;
	private volatile boolean stopMonitor;
	private volatile boolean monitorRunning;

	private Connection con;
	private SpyEventDAO dao;

	private javax.swing.JButton btnStart;
	private javax.swing.JButton btnStop;
	private javax.swing.JButton btnDeleteThisEvent;
	private javax.swing.JButton btnDeleteAllEvents;
	private javax.swing.JComboBox cmbEventNums;
	private javax.swing.JScrollPane jScrollPane1;
	private ETable tableSpyEvent;
	private javax.swing.JLabel lblSpyEvent;

}
