package com.perfspy.ui.panel;

import javax.swing.JPanel;

import com.perfspy.monitor.model.OperationFlow;
import com.perfspy.ui.EntityNode;

public class EntityDetailPanelFacctory {

	public static JPanel setEntity(EntityNode node) {
		Object entity = node.getEntity();
		if (entity instanceof OperationFlow) {
			OperationDetailPanel panel = new OperationDetailPanel();
			panel.setOperation((OperationFlow) entity);
			return panel;
		} else {
			JsonObjectDetailPanel panel = new JsonObjectDetailPanel();
			panel.setNode(node);
			return panel;
		}

	}
}
