package com.perfspy.ui.panel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.EventObject;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.netbeans.swing.etable.ETable;

import com.google.common.collect.Lists;
import com.perfspy.ui.PerfSpyAdmin;
import com.perfspy.ui.tree.action.HideOperationAction;
import com.perfspy.ui.tree.action.HideOperationAction.IntWrapper;

public class HiddenOperationDialog extends JDialog {
	private static final String COL_COUNT = "Count";
	private static final String COL_SIGNATURE = "Signature";
	private static final String COL_HIDE = "Hide";
	private ETable tblHiddens;
	private javax.swing.table.DefaultTableModel hiddenTbModel;

	private static HiddenOperationDialog dlg = new HiddenOperationDialog();
	private static boolean visible = false;

	public static void showDialog(Component parent) {
		if (visible) {
			return;
		}

		Vector<Vector> datas = dlg.getHiddenTbModel().getDataVector();
		Map<String, IntWrapper> hiddenOperations = HideOperationAction.getHiddenOperations();
		Set<String> names = hiddenOperations.keySet();
		for (String name : names) {
			dlg.getHiddenTbModel().addRow(
					new Object[] { true, name, hiddenOperations.get(name).getIntVal() });
		}

		visible = true;
		dlg.setLocationRelativeTo(parent);
		dlg.setVisible(true);

	}

	//	private static void addHiddenOperation(String op, int count) {
	//		Vector<Vector> datas = dlg.getHiddenTbModel().getDataVector();
	//
	//		dlg.getHiddenTbModel().addRow(new Object[] { true, op, count });
	//	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setLayout(new FlowLayout());
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				dlg.dispose();
				System.exit(0);
			}
		});

		frame.setPreferredSize(new Dimension(300, 400));
		frame.pack();

		JButton btnShow = new JButton();
		btnShow.setText("Show");
		btnShow.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				HiddenOperationDialog.showDialog(null);
			}
		});
		frame.add(btnShow);

		JButton btnAdd = new JButton();
		btnAdd.setText("Add");
		btnAdd.addActionListener(new ActionListener() {
			int index = 0;

			//			@Override
			public void actionPerformed(ActionEvent e) {
				index++;
				//				if (index % 2 == 0) {
				//					HiddenOperationDialog.addHiddenOperation("abcde_0", index);
				//				} else {
				//					HiddenOperationDialog.addHiddenOperation("abcde_1", index);
				//				}
			}
		});
		frame.add(btnAdd);

		JButton btnClear = new JButton();
		btnClear.setText("Clear Table");
		btnClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearTable();
			}
		});
		frame.add(btnClear);

		frame.setVisible(true);

	}

	public HiddenOperationDialog() {
		super();
		this.setLayout(new GridBagLayout());
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				hideDialog();
			}
		});
		this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		this.setModal(false);
		this.setAlwaysOnTop(true);

		tblHiddens = new ETable() {
			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {

				Component comp = super.prepareRenderer(renderer, row, col);
				if (col == 5) {
					return comp;
				}
				if (row % 2 == 0) {// && !isCellSelected(row, col)) {
					comp.setBackground(PerfSpyAdmin.LIGHT_GREEN);
				} else {
					comp.setBackground(Color.white);
				}
				return comp;
			}
		};
		tblHiddens.setRowHeight(25);
		tblHiddens.setPopupUsedFromTheCorner(true);
		tblHiddens.setDefaultRenderer(boolean.class, new CheckBoxRenderer());
		tblHiddens.setDefaultEditor(boolean.class, new CheckBoxEditor());
		tblHiddens.setRowSorter(null); // turn off sort

		hiddenTbModel = new javax.swing.table.DefaultTableModel(new String[] { COL_HIDE,
				COL_SIGNATURE, COL_COUNT }, 0) {
			Class[] types = new Class[] { boolean.class, String.class, java.lang.Integer.class };

			public Class getColumnClass(int columnIndex) {
				return types[columnIndex];
			}

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				// if (columnIndex == 0) {
				// return true;
				// }
				return false;
			}

		};
		tblHiddens.setModel(hiddenTbModel);
		tblHiddens.getColumn(COL_HIDE).setPreferredWidth(20);
		tblHiddens.getColumn(COL_SIGNATURE).setPreferredWidth(250);
		tblHiddens.getColumn(COL_COUNT).setPreferredWidth(50);

		JScrollPane sp = new JScrollPane();
		sp.setViewportView(tblHiddens);

		GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.gridwidth = 5;
		this.add(sp, gridBagConstraints);

		// gridBagConstraints = new java.awt.GridBagConstraints();
		// gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
		// gridBagConstraints.insets = new java.awt.Insets(5, 150, 5, 5);
		// gridBagConstraints.gridx = 2;
		// gridBagConstraints.gridy = 1;
		// JButton btnApply = new JButton("Apply");
		// this.add(btnApply, gridBagConstraints);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.anchor = java.awt.GridBagConstraints.CENTER;
		gridBagConstraints.insets = new java.awt.Insets(5, 200, 5, 5);
		gridBagConstraints.gridy = 1;
		gridBagConstraints.gridx = 3;
		JButton btnClose = new JButton("Close");
		this.add(btnClose, gridBagConstraints);
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				hideDialog();
			}

		});

		this.setPreferredSize(new Dimension(500, 400));
		this.pack();

	}

	private void hideDialog() {
		visible = false;
		clearTable();
		dlg.setVisible(false);
	}

	public javax.swing.table.DefaultTableModel getHiddenTbModel() {
		return hiddenTbModel;
	}

	public static void clearTable() {
		dlg.getHiddenTbModel().setDataVector(new Object[0][0],
				new String[] { COL_HIDE, COL_SIGNATURE, COL_COUNT });
	}

	static class CheckBoxEditor extends JCheckBox implements TableCellEditor, ItemListener {
		List<CellEditorListener> editorListeners;

		public CheckBoxEditor() {
			super();
			setHorizontalAlignment(JLabel.CENTER);
			addItemListener(this);
			editorListeners = Lists.newArrayList();
		}

		public void itemStateChanged(ItemEvent e) {
			for (int i = 0; i < editorListeners.size(); ++i) {
				((CellEditorListener) editorListeners.get(i)).editingStopped(new ChangeEvent(this));
			}
		}

		public Component getTableCellEditorComponent(JTable table, Object value,
				boolean isSelected, int row, int column) {
			this.setSelected((Boolean) value);
			return this;
		}

		public Object getCellEditorValue() {
			return this.isSelected();
		}

		public boolean isCellEditable(EventObject anEvent) {
			return true;
		}

		public boolean shouldSelectCell(EventObject anEvent) {
			if (anEvent instanceof MouseEvent) {
				MouseEvent me = (MouseEvent) anEvent;
				return me.getID() != MouseEvent.MOUSE_DRAGGED;
			}
			return true;
		}

		public boolean stopCellEditing() {
			return true;
		}

		public void cancelCellEditing() {
		}

		public void addCellEditorListener(CellEditorListener l) {
			editorListeners.add(l);
		}

		public void removeCellEditorListener(CellEditorListener l) {
			editorListeners.remove(l);
		}

	}

	static class CheckBoxRenderer extends JCheckBox implements TableCellRenderer {

		CheckBoxRenderer() {
			setHorizontalAlignment(JLabel.CENTER);
		}

		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int column) {

			if (isSelected) {
				setForeground(table.getSelectionForeground());
				setBackground(table.getSelectionBackground());
			} else {
				setForeground(table.getForeground());
				setBackground(table.getBackground());
			}
			setSelected((value != null && ((Boolean) value).booleanValue()));
			return this;
		}

	}
}
