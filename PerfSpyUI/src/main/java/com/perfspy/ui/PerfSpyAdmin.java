package com.perfspy.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;
import org.netbeans.swing.etable.ETable;
import org.netbeans.swing.outline.Outline;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.NodePopupFactory;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;

import com.perfspy.monitor.config.PerfSpyConfig;
import com.perfspy.monitor.dao.OperationFlowDAO;
import com.perfspy.monitor.model.OperationFlow;
import com.perfspy.ui.panel.LoadOperationFlowTreePanel;
import com.perfspy.ui.panel.MessagePanel;
import com.perfspy.ui.panel.ObjectTreePanel;
import com.perfspy.ui.panel.SpyEventMonitorPanel;
import com.perfspy.ui.panel.SpyEventOverViewTableModel;
import com.perfspy.ui.tree.action.SearchAction;

public class PerfSpyAdmin extends javax.swing.JFrame implements ExplorerManager.Provider {

	private ExplorerManager explorerManager;

	private OutlineView outlineView;
	private PerfSpyConfig config;

	private static final Logger logger = Logger.getLogger(PerfSpyAdmin.class);

	public PerfSpyAdmin() throws Exception {
		this.setIconImage(ImageUtilities.loadImage("dolphin.png"));

		config = PerfSpyConfig.config("perfspy-ui-config.xml");

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				closeConnection();
				spyEventMonitorPanel.close();
				System.exit(0);
			}
		});

		setUIFont(new javax.swing.plaf.FontUIResource("Arial", Font.PLAIN, 20));

		explorerManager = new ExplorerManager();
		outlineView = new OutlineView();
		initOutlineView();

	}

	private void init() {
		initComponents();

		pack();
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}

	private void setUIFont(javax.swing.plaf.FontUIResource f) {
		java.util.Enumeration keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value instanceof javax.swing.plaf.FontUIResource)
				UIManager.put(key, f);
		}
	}

	@SuppressWarnings("unchecked")
	private void initComponents() {
		setAdminTitle("Performance Spy");
		getContentPane().setLayout(new java.awt.GridBagLayout());
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		java.awt.GridBagConstraints gridBagConstraints;

		upperTabPane = new javax.swing.JTabbedPane();
		upperTabPane.setPreferredSize(new Dimension(300, 250));

		spyEventMonitorPanel = new SpyEventMonitorPanel();
		upperTabPane.addTab("Spy Event Monitor", spyEventMonitorPanel);

		loadOperationTreePanel = new LoadOperationFlowTreePanel();
		upperTabPane.addTab("Load", loadOperationTreePanel);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 0;
		gridBagConstraints.insets = new java.awt.Insets(6, 6, 6, 6);
		getContentPane().add(upperTabPane, gridBagConstraints);

		downTabPane = new javax.swing.JTabbedPane();
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
		gridBagConstraints.weightx = 1;
		gridBagConstraints.weighty = 1;
		gridBagConstraints.insets = new java.awt.Insets(6, 6, 6, 6);
		getContentPane().add(downTabPane, gridBagConstraints);

		pnlOutlineViewContainer = new javax.swing.JPanel();
		pnlOutlineViewContainer.setLayout(new java.awt.BorderLayout());
		downTabPane.addTab("Operation Tree", pnlOutlineViewContainer);

		pnlOutlineViewContainer.add(outlineView, BorderLayout.CENTER);

		JPanel eventOverViewPanel = new javax.swing.JPanel();
		eventOverViewPanel.setLayout(new java.awt.BorderLayout());
		downTabPane.addTab("Event Overview", eventOverViewPanel);

		eventOverViewPanel.add(initSpyEventOverviewTable(), BorderLayout.CENTER);

	}

	public void initJsonObjectlineView() {
		objTreePanel = new ObjectTreePanel();
	}

	public void setAdminTitle(String title) {
		setTitle(title);
	}

	private JScrollPane initSpyEventOverviewTable() {
		JScrollPane jScrollPane1 = new javax.swing.JScrollPane();
		eventOverviewTable = new ETable(new SpyEventOverViewTableModel()) {
			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {

				Component comp = super.prepareRenderer(renderer, row, col);
				if (col == 5) {
					return comp;
				}
				if (row % 2 == 0) {// && !isCellSelected(row, col)) {
					comp.setBackground(LIGHT_GREEN);
				} else {
					comp.setBackground(Color.white);
				}
				return comp;
			}

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return false;
			}
		};
		eventOverviewTable.setRowHeight(25);
		eventOverviewTable.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					JTable table = (JTable) e.getSource();
					int row = table.getSelectedRow();
					Long operationId = (Long) table.getValueAt(row, 1);
					PerfSpyAdmin.getPerfSpyAdmin().triggerLoad(loadOperationTreePanel.getEventId(),
							operationId);
				}
			}
		});
		jScrollPane1.setViewportView(eventOverviewTable);

		return jScrollPane1;
	}

	public void setSpyEventOverview(List<OperationFlow> overview) {
		((SpyEventOverViewTableModel) eventOverviewTable.getModel()).setOverviews(overview);
	}

	private void initOutlineView() {

		final Outline outline = outlineView.getOutline();
		outline.setRowSorter(null);
		outline.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		outlineView
				.setPropertyColumns(OperationFlowNode.ANALYZE_PROP, OperationFlowNode.ANALYZE_PROP,
						OperationFlowNode.EXECUTIONTIME_PROP, OperationFlowNode.EXECUTIONTIME_PROP,
						OperationFlowNode.RETURN_PROP, OperationFlowNode.RETURN_PROP,
						OperationFlowNode.PARAM_PROP, OperationFlowNode.PARAM_PROP,
						OperationFlowNode.Long_SIGNATURE_PROP,
						OperationFlowNode.Long_SIGNATURE_PROP, OperationFlowNode.OP_ID,
						OperationFlowNode.OP_ID, OperationFlowNode.Calling_Op,
						OperationFlowNode.Calling_Op);

		TableColumn firstCol = outline.getColumnModel().getColumn(0);
		firstCol.setMinWidth(300);

		// disable showing row filter menu, see
		// http://netbeans.org/bugzilla/show_bug.cgi?id=185196
		outlineView.setNodePopupFactory(new NodePopupFactory() {
			@Override
			public JPopupMenu createPopupMenu(int row, int column, Node[] selectedNodes,
					Component component) {
				return super.createPopupMenu(row, column, selectedNodes, component);
			}
		});
		outlineView.getNodePopupFactory().setShowQuickFilter(false);
		outline.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		outline.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					int column = outline.getSelectedColumn();
					if (column != 3 && column != 4 && column != 7) {
						return;
					}

					OperationFlowNode node = (OperationFlowNode) explorerManager.getSelectedNodes()[0];
					OperationFlow op = node.getEntity();

					if (column == 3 || column == 4) {
						fleshOpDetails(op, column);
						return;
					}

					if (column == 7) {
						SearchAction.selectParent(PerfSpyAdmin.getPerfSpyAdmin()
								.getExplorerManager(), PerfSpyAdmin.getPerfSpyAdmin()
								.getOutlineView(), op, PerfSpyAdmin.this);
						getLoadOperationTreePanel().setGotoOp(op.getId());
						return;
					}

				}
			}

		});

	}

	private void fleshOpDetails(OperationFlow op, int column) {
		if (!op.isFleshed() && op.isDetailLogged()) {
			try {
				OperationFlow op0 = getOperationFlowDao().getOperationDetails(op.getId(),
						PerfSpyAdmin.getPerfSpyAdmin().getLoadOperationTreePanel().getEventId());
				if (op0 != null) {
					op.setParametersJson(op0.getParametersJson());
					op.setReturnValueJson(op0.getReturnValueJson());

					op.setFleshed(true);
				} else {
					op.setFleshed(false);
				}
			} catch (Exception e1) {
				String errMsg = "Error in loading parameters and the return value for " + op;
				logger.error(errMsg, e1);
				MessagePanel.showQuickErrorDialog(PerfSpyAdmin.admin, errMsg, e1);

			}
		}

		if (op.isDetailLogged() && op.isFleshed()) {
			if (column == 4) {

				DialogDescriptor dlg = new DialogDescriptor(objTreePanel, "Parameters:" + op);
				if (logger.isDebugEnabled()) {
					logger.debug("reading parameter json:" + op.getParametersJson() + " for op:"
							+ op.getSignature());
				}
				objTreePanel.setObject("parameters", op.getParametersJson());
				DialogDisplayer.getDefault().notifyLater(dlg);
			} else {
				DialogDescriptor dlg = new DialogDescriptor(objTreePanel, "Return Value:" + op);
				if (logger.isDebugEnabled()) {
					logger.debug("reading return value json:" + op.getReturnValueJson()
							+ " for op:" + op.getSignature());
				}
				objTreePanel.setObject("return", op.getReturnValueJson());
				DialogDisplayer.getDefault().notifyLater(dlg);
			}
		}
	}

	public static void main(String args[]) throws Exception {
		admin = new PerfSpyAdmin();
		/**
		 * PerfSpyAdmin.getPerfSpyAdmin() is used widely to get various objects,
		 * e.g. exploreManager,outlineView so first construct admin, allow UI
		 * components to be able to access PerfSpyAdmin.getPerfSpyAdmin()
		 * 
		 * initJsonObjectlineView() is extracted as a separate public method to
		 * facilitate testing
		 */
		admin.init();
		admin.initJsonObjectlineView();

		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				admin.setVisible(true);
			}
		});
	}

	public static PerfSpyAdmin getPerfSpyAdmin() {
		return admin;
	}

	/**
	 * just for testing
	 */
	public static void setPerfSpyAdmin(PerfSpyAdmin admin0) {
		admin = admin0;
	}

	@Override
	public ExplorerManager getExplorerManager() {
		return this.explorerManager;
	}

	public PerfSpyConfig getPerfSpyConfig() {
		return this.config;
	}

	public OutlineView getOutlineView() {
		return outlineView;
	}

	public ObjectTreePanel getObjectTreePanel() {
		return this.objTreePanel;
	}

	public LoadOperationFlowTreePanel getLoadOperationTreePanel() {
		return loadOperationTreePanel;
	}

	public void triggerLoad(long eventId, long... operationId) {
		upperTabPane.setSelectedIndex(1);
		downTabPane.setSelectedIndex(0);
		loadOperationTreePanel.triggerLoad(eventId, operationId);
		if (operationId.length > 0) {
			// TODO: can't select the node, expand(node) doesn't work
			// selectNode(operationId[0]);
		}
	}

	private void selectNode(long operationId) {
		OperationFlowNode root = (OperationFlowNode) explorerManager.getRootContext();
		OperationFlowNode match = searchNode(root, operationId);
		if (match != null) {
			try {
				Node expanded = root;
				while (expanded != match) {
					outlineView.expandNode(expanded);
					expanded = expanded.getChildren().getNodeAt(0);
				}
				// outlineView.expandNode(match);
				explorerManager.setSelectedNodes(new Node[] { match });
			} catch (PropertyVetoException e) {
				// ignore
				logger.error("ignore this", e);
			}
		}

	}

	private OperationFlowNode searchNode(OperationFlowNode searchFrom, long operationId) {
		if (searchFrom.getEntity().getId() == operationId) {
			return searchFrom;
		}

		Node[] childNodes = searchFrom.getChildren().getNodes(true);
		for (Node child : childNodes) {
			OperationFlowNode match = searchNode((OperationFlowNode) child, operationId);
			if (match != null) {
				return match;
			}
		}
		return null;

	}

	public OperationFlowDAO getOperationFlowDao() throws Exception {
		if (operationDao != null) {
			return operationDao;
		}

		operationDao = new OperationFlowDAO(getConnection());
		operationDao.setOperationNumberLimit(config.getTreeNodeLimit());
		return operationDao;
	}

	public Connection getConnection() throws Exception {
		if (con == null) {
			con = config.getConnection(null);// "jdbc:oracle:thin:@16.155.207.79:1521:ORCL",
		}
		return con;
	}

	public void closeConnection() {
		if (con != null) {
			try {
				if (!con.isClosed()) {
					con.close();
				}
			} catch (SQLException e1) {
				logger.error("error in closing the connection", e1);
				// ignore
			}
		}

	}

	private ObjectTreePanel objTreePanel;
	private OperationFlowDAO operationDao;
	private Connection con;
	private javax.swing.JPanel pnlOutlineViewContainer;
	private javax.swing.JTabbedPane upperTabPane;
	private javax.swing.JTabbedPane downTabPane;
	private LoadOperationFlowTreePanel loadOperationTreePanel;
	private SpyEventMonitorPanel spyEventMonitorPanel;
	private ETable eventOverviewTable;
	private static PerfSpyAdmin admin;
	public static final Color LIGHT_GREEN = new Color(219, 254, 236);

}
