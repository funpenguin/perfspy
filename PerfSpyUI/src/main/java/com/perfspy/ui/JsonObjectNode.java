package com.perfspy.ui;

import java.awt.Image;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ProxyLookup;

import com.perfspy.ui.panel.ObjectTreePanel;
import com.perfspy.ui.tree.action.ExpandAction;
import com.perfspy.ui.tree.action.MarkOperationAction;
import com.perfspy.ui.tree.action.ShowAllOperations;
import com.perfspy.ui.tree.action.ShowDetailAction;
import com.perfspy.ui.tree.action.ShowMarkedAction;

/**
 * a node that shows an java object, each field of the object acting as a child
 * node
 */
public final class JsonObjectNode extends EntityNode<Object> {

	private JsonObjectBean jsonObjectBean;

	public static final String VALUE_PROP = "value";

	public JsonObjectNode(String name, String jsonString) throws IOException {
		this(JsonObjectBean.fromJsonString(name, jsonString));
	}

	public JsonObjectNode(JsonObjectBean objEntity) {
		this(objEntity, new InstanceContent());
	}

	private JsonObjectNode(JsonObjectBean objEntity, InstanceContent ic) {
		super(Children.create(new JsonObjectNodeChildFactory(objEntity), true), new ProxyLookup(
				objEntity.getLookup(), new AbstractLookup(ic)));

		this.jsonObjectBean = objEntity;

	}

	@Override
	public String getDisplayName() {
		return jsonObjectBean.getObjName();
	}

	@Override
	public String getHtmlDisplayName() {
		if (this.isMarked()) {
			return "<font color='0000FF'>" + getDisplayName() + "</font>";
		}

		return getDisplayName();

	}

	@Override
	public Image getIcon(int type) {
		return ImageUtilities.loadImage("query.png");
	}

	@Override
	public Image getOpenedIcon(int type) {
		return ImageUtilities.loadImage("open.png");
	}

	@Override
	protected Sheet createSheet() {
		Sheet sheet = Sheet.createDefault();
		Sheet.Set set = Sheet.createPropertiesSet();

		PropertySupport valueProp = new PropertySupport.ReadOnly<String>(VALUE_PROP, String.class,
				VALUE_PROP, "The value of the field") {
			@Override
			public String getValue() throws IllegalAccessException, InvocationTargetException {
				if (jsonObjectBean == null) {
					return "";
				}

				return jsonObjectBean.getJsonObjectDisplay();
				//ToStringUtil.getObjectDescription(jsonObjectBean.getJsonObject());

			}
		};
		set.put(valueProp);

		sheet.put(set);
		return sheet;
	}

	@Override
	public Object getEntity() {
		return this.jsonObjectBean.getJsonObject();
	}

	@Override
	public String getEntityDiscrption() {
		return this.jsonObjectBean.getObjName() + ":" + this.jsonObjectBean.getJsonObjectDisplay();
	}

	@Override
	public String toString() {
		return this.getDisplayName();
	}

	@Override
	public javax.swing.Action[] getActions(boolean context) {
		ObjectTreePanel objectTreePanel = PerfSpyAdmin.getPerfSpyAdmin().getObjectTreePanel();
		OutlineView outlineView = objectTreePanel.getOutlineView();
		ExplorerManager explorerManager = objectTreePanel.getExplorerManager();

		return new javax.swing.Action[] { new ExpandAction(this, true, outlineView),
				MarkOperationAction.markAction.config(explorerManager, outlineView),
				new ShowMarkedAction(this, explorerManager, outlineView),
				new ShowAllOperations(this, explorerManager, outlineView),
				new ShowDetailAction(this) };
	}
}
